FROM node:14-alpine
EXPOSE 8080
WORKDIR /app
COPY ./frontend ./
RUN npm install
RUN npm run build
FROM nginx:1.17.0-alpine
COPY --from=0 /app/dist/frontend/ /usr/share/nginx/html
COPY ./frontend/nginx.conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]