import { Injectable } from '@angular/core';
import { InfoUserService } from '../services/info-user.service';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const AUTHORITIES_KEY = 'AuthAuthorities';


@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  imageToShow: any;
  
  private roles: Array<string> = [];

  constructor(private UserServ: InfoUserService) { }

  activo(): boolean {
    if ( sessionStorage.getItem(TOKEN_KEY) ) {
      return true;
      } else {
        return false;
    }
}

  signOut(): void {
    window.localStorage.clear();
    window.sessionStorage.clear();
  }

  public saveToken(token: string): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return sessionStorage.getItem(TOKEN_KEY);
  }

  public saveUser(user: string): void {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  
  public getUser(): any {
    return JSON.parse(sessionStorage.getItem(USER_KEY));
  }

  async saveGeneralInfo(){
      ///await this.GetPhoto();
      await this.GetInfo();
  }

  
  public GetInfo(){
    this.UserServ.get_info().subscribe((response: any) => 
    {
      localStorage.setItem('trabajo', response.jobTitle);
      localStorage.setItem('nombres', response.givenName);
      localStorage.setItem('apellidos', response.surname);
      
    });
  }

  
  /*public GetPhoto(){
    this.UserServ.get_photo().subscribe((response: any) => {
      console.log(response);
      this.createImageFromBlob(response);
    });
  }
  
  async createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    await reader.addEventListener("load", async () => {
       this.imageToShow = reader.result;
       await localStorage.setItem('photo', this.imageToShow);
    }, false);
  
    if (image) {
      await reader.readAsDataURL(image);
    }
  }*/
  

}

