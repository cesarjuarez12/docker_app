import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const httpOptions = {
		  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
		};


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) { }

  getCurrentUser(): Observable<any> {
    return this.http.get('api/user/me', httpOptions);
  }

  getCoordinadores(): Observable<any> {
    return this.http.get('api/user/coordinadores').pipe(
      map(response => response)
    );
  }

  changeNotifications(id: number): Observable<any> {
    return this.http.patch<any>('api/user/coordinadores/activate-notif/' + id, { headers: this.httpHeaders })
  }

}
