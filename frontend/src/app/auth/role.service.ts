import { Injectable } from '@angular/core';
import { TokenStorageService } from './token-storage.service';

@Injectable({
    providedIn: 'root'
})
export class RoleService {

    user: any;
    roles: any;

    constructor(private tokenStorageService: TokenStorageService) { 
        this.getRol();
    }

    getRol() {
        this.user = this.tokenStorageService.getUser();
        this.roles = this.user.roles;
    }

    hasRole(role: string): boolean {
        if (this.user.roles.includes(role)) {
            return true;
        }
        return false;
    }

}
