import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../auth/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionGuardService {

  constructor(private _Router: Router, private _Token: TokenStorageService) { }

canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
  let user = this._Token.getUser();
  let token = this._Token.getToken();
  let valid_role:boolean;
  let length = user.roles.length;

  if(user != null){
    for (let index = 0; index < length; index++) {
      route.data.permission.forEach((permited: any) => {
        if(permited == user.roles[index]){
          valid_role = true;
          index = length;  
        }
     });
    }
  }


  if (!token || !user || !user.displayName) {
    window.location.replace('/login');
    //console.log('Denied Permission');
    return false;
  } else if (!!user && route.data.permission && route.data.permission.length && valid_role != undefined && valid_role == true) {
    //console.log('Welcome Access granted!');
    return true;
  } else {
    window.location.replace('/inicio');
    //console.log('Denied Permission');
  }
  return false; 
  }
}
