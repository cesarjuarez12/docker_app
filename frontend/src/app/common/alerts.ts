import Swal from "sweetalert2";

export class alert {

    public static alertOk(message: string) {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: message,
            showConfirmButton: false,
            timer: 5000,
            toast: true,
            inputAttributes: {
                type: 'hidden'
            }
        })
    }

    public static alertError(message: string) {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: message,
            showConfirmButton: false,
            timer: 5000,
            toast: true,
            timerProgressBar: false,
            inputAttributes: {
                type: 'hidden'
            }
        })
    }

    public static alertErrorWithText(message: string, text: string) {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: message,
            text: text,
            showConfirmButton: false,
            timer: 4000,
            toast: false,
            backdrop: false,
            timerProgressBar: true,
            inputAttributes: {
                type: 'hidden'
            }
        })
    }

}