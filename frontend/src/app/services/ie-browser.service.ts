import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { alert } from "src/app/common/alerts";
import * as dayjs from "dayjs";
@Injectable({
  providedIn: 'root'
})
export class IEBrowserService {

  IE: boolean = false;

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) { }

  async detectionBrowser(){
    let isIEOrEdge = /msie\s|trident\/|Edge\//i.test(window.navigator.userAgent)
    if(isIEOrEdge){
      return true;
    } else {
      return false;
    }
  }
  
  getReportIE(idRequest: number) {
    return this.http.get('api/reports/pdf-final/' + idRequest,  { headers: this.httpHeaders, responseType: 'blob' }).pipe(map(result => {
      const blob = new Blob([result], { type: "application/pdf" });
      return blob;
    }));
  }

  getReportCompleteIE(request: any) {
    let name = "INGENIERÍA-" + request.requestType.description + "-SOLICITUD" + request.id + "-" + request.projectName + "-" + dayjs().format('DDMMYY') + ".pdf";
    this.getReportIE(request.id).subscribe((data) => {
      window.navigator.msSaveBlob(data,name);
    }, error => {
      alert.alertError("Ocurrió un error al descargar la ingeniería")
    })
  }

}
