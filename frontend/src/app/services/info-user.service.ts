import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from "rxjs";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { Page } from "../components/users/users-list/page";
import { UserProfile } from "../pages/template/profile/user-profile";

@Injectable({
  providedIn: 'root'
})
export class InfoUserService {

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) { }

  get_info() {
    return this.http.get('api/user/me/profile', { headers: this.httpHeaders }).pipe(
      map(response => response)
    );
  }

  get_photo() {
    return this.http.get('api/user/photo', { headers: this.httpHeaders, responseType: 'blob' });
    /*return this.http.get('api/user/photo',  { headers: this.httpHeaders, responseType: "blob" }).pipe(map(result => {
      let url = window.URL;
      return this.sanitizer.bypassSecurityTrustUrl(url.createObjectURL(result));
    }));*/
  }

  getUsers(page: Page): Observable<any> {
    const params = new HttpParams()
      .set('page', page.pageNumber.toString())
      .set('size', page.size.toString())
    return this.http.get(`api/user/`, { params });
  }


  getUser(id: number): Observable<UserProfile> {
    const params = new HttpParams()
      .set('idUser', id.toString())
    return this.http.get<UserProfile>(`api/user/profile`, { params });
  }


  getManual() {
    return this.http.get('api/ayuda/manual', { headers: this.httpHeaders, responseType: 'blob' });
  }

  getPDF(): Observable<SafeUrl> {
    return this.http.get('api/ayuda/manual', { responseType: "blob" }).pipe(map(result => {
      /*let url = window.URL;
      let blob = new Blob([result], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      return this.sanitizer.bypassSecurityTrustUrl(url.createObjectURL(blob));*/
      return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(result));
      //return new Blob([result], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
    }));/**/
  }
}
