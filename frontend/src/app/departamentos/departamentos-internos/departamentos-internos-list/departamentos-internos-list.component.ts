import { Component, OnInit } from '@angular/core';
import { faEdit, faEye, faPen, faSearch, faTimes, faTrash } from '@fortawesome/free-solid-svg-icons';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import Swal from 'sweetalert2';
import { DepartamentoInterno } from '../departamento-interno';
import { DepartamentoInternoService } from '../departamento-interno.service';

declare var $ :any;

@Component({
  selector: 'app-departamentos-internos-list',
  templateUrl: './departamentos-internos-list.component.html',
  styleUrls: ['./departamentos-internos-list.component.css']
})
export class DepartamentosInternosListComponent implements OnInit {

  title = "DEPARTAMENTOS INTERNOS (SOLICITUDES)"

  ColumnMode = ColumnMode;
  SelectionType = SelectionType;

  faSearch = faSearch;
  faEdit = faEdit;
  faTimes = faTimes;
  faTrash = faTrash;
  faPen = faPen;
  faEye = faEye;

  departamentosInternos!: DepartamentoInterno[];

  page: number = 1;
  size: number = 5;
  paginador: any;
  name: string = null;

  departamentoInterno: DepartamentoInterno = new DepartamentoInterno();

  constructor(private departamentoInternoService: DepartamentoInternoService, private loadingService: NgxUiLoaderService) {
    this.getDepartamentosInternos();
  }

  ngOnInit(): void {
    
  }


  getDepartamentosInternos() {
    //this.loadingService.start();
    this.departamentoInternoService.getDepartamentosInternos().subscribe(
      (data) => {
        console.log(data)
        this.departamentosInternos = data as DepartamentoInterno[]
        //this.loadingService.stop()
      }, error => {
        alert.alertError("Ocurrió un error!")
        //this.loadingService.stop();
      })
  }

  find(event?: any) {
    const val = event.target.value.toLowerCase();
    this.departamentoInternoService.getDepartamentosInternosByName(val).subscribe(
      (data) => this.departamentosInternos = data as DepartamentoInterno[])
  }

  delete(id: number): void {
    Swal.fire({
      title: '¿Seguro que desea eliminar el departamento?',
      text: 'No podrá deshacer esta acción',
      icon: 'warning',
      customClass: {
        title: 'text-primary-1 font-weight-bold',
        confirmButton: 'btn btn-primary btn-sm mr-2',
        cancelButton: 'btn btn-secondary btn-sm'
      },
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      backdrop: false,
      allowOutsideClick: false,
      input: null,
      inputAttributes: {
        type: 'hidden'
      },
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.loadingService.start();
        this.departamentoInternoService.delete(id).subscribe(
          data => {
            this.loadingService.stop()
            alert.alertOk("Departamento eliminado correctamente!")
            this.getDepartamentosInternos();
          }, error => {
            this.loadingService.stop()
            alert.alertError("Ocurrió un error, asegúrese que el departamento no este siendo usado en ninguna solicitud")
          }
        )
      }
    });
  }

  show(id: number){
    // para mostrarlo:
    this.departamentoInternoService.getDepartamentoInterno(id).subscribe((departamentoInterno) => this.departamentoInterno = departamentoInterno)
    $('#ShowModalCenter').modal('show');
  }
}
