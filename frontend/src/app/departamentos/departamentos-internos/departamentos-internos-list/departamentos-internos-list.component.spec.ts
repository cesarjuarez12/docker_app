import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartamentosInternosListComponent } from './departamentos-internos-list.component';

describe('DepartamentosInternosListComponent', () => {
  let component: DepartamentosInternosListComponent;
  let fixture: ComponentFixture<DepartamentosInternosListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartamentosInternosListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartamentosInternosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
