import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartamentosInternosFormComponent } from './departamentos-internos-form.component';

describe('DepartamentosInternosFormComponent', () => {
  let component: DepartamentosInternosFormComponent;
  let fixture: ComponentFixture<DepartamentosInternosFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartamentosInternosFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartamentosInternosFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
