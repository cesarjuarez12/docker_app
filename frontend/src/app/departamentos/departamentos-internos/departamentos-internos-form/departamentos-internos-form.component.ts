import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import { DepartamentoInterno } from '../departamento-interno';
import { DepartamentoInternoService } from '../departamento-interno.service';

@Component({
  selector: 'app-departamentos-internos-form',
  templateUrl: './departamentos-internos-form.component.html',
  styleUrls: ['./departamentos-internos-form.component.css']
})
export class DepartamentosInternosFormComponent implements OnInit {

  title = "NUEVO DEPARTAMENTO INTERNO (SOLICITUDES)"
  departamentoInterno: DepartamentoInterno = new DepartamentoInterno();

  errors!: string[];

  Path: string = '';
  ShowMode: boolean;
  faExclamationTriangle = faExclamationTriangle;
  isNumberMessage = '';

  //Validacion
  NombreValido: boolean;


  constructor(private departamentoInternoService: DepartamentoInternoService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private loadingService: NgxUiLoaderService) { }

  ngOnInit() {
    this.cargarDepartamentoInterno()
  }

  cargarDepartamentoInterno(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.departamentoInternoService.getDepartamentoInterno(id).subscribe((departamentoInterno) => this.departamentoInterno = departamentoInterno)
        this.title = "EDITAR DEPARTAMENTO INTERNO (SOLICITUDES)";
      }
    })
  }

  save(): void {
    if (!/^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.]{2,254})+$/i.test(this.departamentoInterno.name)){
      this.NombreValido = true;
    } else {
      this.NombreValido = false;
     
    
    this.loadingService.start();
    this.departamentoInternoService.create(this.departamentoInterno)
      .subscribe(departamentoInterno => {
        alert.alertOk("Departamento guardado correctamente")
        this.loadingService.stop();
        this.router.navigate(['/departamentos-internos'])
      },
        err => {
          this.errors = err.error.errors as string[];
          alert.alertError("Ocurrió un error!")
          this.loadingService.stop();
          console.log(this.errors);
        }
      );

    }



  }

  update(): void {
    if (!/^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.]{2,254})+$/i.test(this.departamentoInterno.name)){
      this.NombreValido = true;
    } else {
      this.NombreValido = false;

    this.loadingService.start();
    this.departamentoInternoService.update(this.departamentoInterno)
      .subscribe(departamentoInterno => {
        alert.alertOk("Departamento guardado correctamente")
        this.loadingService.stop();
        this.router.navigate(['/departamentos-internos'])
      }, error => {
        alert.alertError("Ocurrió un error!")
        this.loadingService.stop();
      }
      )
    }
  }

  keyupName(x: any){
    this.NombreValido = false;
    if (!/^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.]{2,254})\2(\d{4})$/i.test(x.target.value)) {
      x.target.value = x.target.value.replace(/[^a-zA-ZÀ-ÿ \: \( \) \- \_ \#0-9,.]+/ig,"");
    }
  }

}
