import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from "rxjs";
import { DepartamentoInterno } from "./departamento-interno";

@Injectable()
export class DepartamentoInternoService {

  private urlEndPoint: string =  '/api/departamentos-internos/';

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) { }

  getDepartamentosInternos(): Observable<any> {
    return this.http.get(this.urlEndPoint).pipe(
      map(response => response)
    );
  }

  getDepartamentosInternosByName(name: string): Observable<any> {
    return this.http.get(this.urlEndPoint + 'search/find-by-name?name=' + name).pipe(
      map(response => response)
    );
  }

  create(departamentoInterno: DepartamentoInterno): Observable<DepartamentoInterno> {
    return this.http.post<DepartamentoInterno>(this.urlEndPoint, departamentoInterno, { headers: this.httpHeaders })
  }

  getDepartamentoInterno(id: number): Observable<DepartamentoInterno> {
    return this.http.get<DepartamentoInterno>(`${this.urlEndPoint}${id}`)
  }

  update(departamentoInterno: DepartamentoInterno): Observable<DepartamentoInterno> {
    return this.http.patch<DepartamentoInterno>(`${this.urlEndPoint}${departamentoInterno.id}`, departamentoInterno, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<DepartamentoInterno> {
    return this.http.delete<DepartamentoInterno>(`${this.urlEndPoint}${id}`, { headers: this.httpHeaders })
  }

}