import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartamentosInternosOmListComponent } from './departamentos-internos-om-list.component';

describe('DepartamentosInternosOmListComponent', () => {
  let component: DepartamentosInternosOmListComponent;
  let fixture: ComponentFixture<DepartamentosInternosOmListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartamentosInternosOmListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartamentosInternosOmListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
