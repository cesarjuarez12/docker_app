import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from "rxjs";
import { DepartamentoInternoOm } from "./departamento-interno-om";
@Injectable()
export class DepartamentoInternoOmService {

    private urlEndPoint: string = 'api/departamentos-internos-om/';

    private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

    constructor(private http: HttpClient) { }

    getDepartamentosInternos(): Observable<any> {
        return this.http.get(this.urlEndPoint).pipe(
            map(response => response)
        );
    }

    getDepartamentosInternosByName(name: string): Observable<any> {
        return this.http.get(this.urlEndPoint + 'search/find-by-name?name=' + name).pipe(
            map(response => response)
        );
    }

    create(departamentoInterno: DepartamentoInternoOm): Observable<DepartamentoInternoOm> {
        return this.http.post<DepartamentoInternoOm>(this.urlEndPoint, departamentoInterno, { headers: this.httpHeaders })
    }

    getDepartamentoInterno(id: number): Observable<DepartamentoInternoOm> {
        return this.http.get<DepartamentoInternoOm>(`${this.urlEndPoint}${id}`, { headers: this.httpHeaders })
    }

    update(departamentoInterno: DepartamentoInternoOm): Observable<DepartamentoInternoOm> {
        return this.http.patch<DepartamentoInternoOm>(`${this.urlEndPoint}${departamentoInterno.id}`, departamentoInterno, { headers: this.httpHeaders })
    }

    delete(id: number): Observable<DepartamentoInternoOm> {
        return this.http.delete<DepartamentoInternoOm>(`${this.urlEndPoint}${id}`, { headers: this.httpHeaders })
    }


}