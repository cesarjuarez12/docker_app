import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import Swal from 'sweetalert2';
import { DepartamentoInternoOm } from '../departamento-interno-om';
import { DepartamentoInternoOmService } from '../departamento-interno-om.service';

@Component({
  selector: 'app-departamentos-internos-om-form',
  templateUrl: './departamentos-internos-om-form.component.html',
  styleUrls: ['./departamentos-internos-om-form.component.css']
})
export class DepartamentosInternosOmFormComponent implements OnInit {


  title = "NUEVO DEPARTAMENTO INTERNO (SOPORTE)"

  departamentoInterno: DepartamentoInternoOm = new DepartamentoInternoOm();

  errors!: string[];

  faExclamationTriangle = faExclamationTriangle;

  //Validacion
  NombreValido: boolean;

  constructor(private departamentoInternoService: DepartamentoInternoOmService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private loadingService: NgxUiLoaderService) { }

  ngOnInit() {
    this.cargarDepartamentoInterno()
  }

  cargarDepartamentoInterno(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.departamentoInternoService.getDepartamentoInterno(id).subscribe((departamentoInterno) => this.departamentoInterno = departamentoInterno)
        this.title = "EDITAR DEPARTAMENTO INTERNO (SOPORTE)";
      }
    })
  }

  save(): void {
    if (!/^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.]{2,254})+$/i.test(this.departamentoInterno.name)){
      this.NombreValido = true;
    } else {
      this.NombreValido = false;
     
    
    this.loadingService.start();
    this.departamentoInternoService.create(this.departamentoInterno)
      .subscribe(departamentoInterno => {
        alert.alertOk("Departamento guardado correctamente")
        this.loadingService.stop();
        this.router.navigate(['/departamentos-internos-om'])
      },
        err => {
          this.errors = err.error.errors as string[];
          alert.alertError("Ocurrió un error!")
          this.loadingService.stop();
          console.log(this.errors);
        }
      );
    }
  }

  update(): void {
    this.loadingService.start();
    this.departamentoInternoService.update(this.departamentoInterno)
      .subscribe(departamentoInterno => {
        alert.alertOk("Departamento guardado correctamente")
        this.loadingService.stop();
        this.router.navigate(['/departamentos-internos-om'])
      }, error => {
        alert.alertError("Ocurrió un error!")
        this.loadingService.stop();
      }
      )
  }

  keyupName(x: any){
    this.NombreValido = false;
    if (!/^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.]{2,254})\2(\d{4})$/i.test(x.target.value)) {
      x.target.value = x.target.value.replace(/[^a-zA-ZÀ-ÿ \: \( \) \- \_ \#0-9,.]+/ig,"");
    }
  }

}
