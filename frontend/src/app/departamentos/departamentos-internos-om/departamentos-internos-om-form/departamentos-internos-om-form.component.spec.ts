import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartamentosInternosOmFormComponent } from './departamentos-internos-om-form.component';

describe('DepartamentosInternosOmFormComponent', () => {
  let component: DepartamentosInternosOmFormComponent;
  let fixture: ComponentFixture<DepartamentosInternosOmFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartamentosInternosOmFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartamentosInternosOmFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
