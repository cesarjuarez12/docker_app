import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { RoleService } from 'src/app/auth/role.service';
import { TokenStorageService } from 'src/app/auth/token-storage.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  /*currentUser: any;
  rol: String;
  isImageLoading: boolean;
  imageToShow: string | ArrayBuffer;
  imgb64: string;

  fileUrl: any;
  pdfSrc = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf"
  constructor(private _Token: TokenStorageService, private UserServ: InfoUserService, private ayudaService: AyudaService) { }
*/
  isLoggedIn: boolean = false;

  constructor(private localeService: BsLocaleService, private tokenStorage: TokenStorageService, private roleService: RoleService, private router: Router) {
    this.localeService.use('es');
  }



  ngOnInit(): void {
    this.redirect();
    //this.getUrl();
    //this.getPhoto();
    /* if (this.tokenStorage.getToken()) {
       this.isLoggedIn = true;
       console.log('Logueado');
     } else {
       console.log('No logueado');
     }*/
  }


  redirect() {
    if (this.roleService.hasRole("ROLE_SOLICITANTE")) {

      this.router.navigateByUrl("/historial-solicitudes")

    } else if (this.roleService.hasRole("ROLE_COORDINADOR")) {

      this.router.navigateByUrl("/historial-all-solicitudes")

    } else if (this.roleService.hasRole("ROLE_INGENIERO")) {

      this.router.navigateByUrl("/historial-solicitudes-ing")

    } else if (this.roleService.hasRole("ROLE_ADMINFUNCIONAL")) {

      this.router.navigateByUrl("/solicitudes")

    } else if (this.roleService.hasRole("ROLE_ADMIN")) {

      this.router.navigateByUrl("/ayuda")

    }
  }


  /*
    getUrl(){
  
      this.UserServ.getPDF().subscribe((response) => {
        // console.log(response);
         this.fileUrl  = response;
        }, err => {
         console.error(err);
       });
    }
  /*
    getPhoto(){
      this.isImageLoading = true;
      this.UserServ.get_photo().subscribe(data => {
        this.createImageFromBlob(data);
        //localStorage.setItem("photo", data);
        this.isImageLoading = false;
      }, error => {
        this.isImageLoading = false;
        console.log(error);
      });
    }
  
  
  createImageFromBlob(image: any) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
       this.imageToShow = reader.result;
      // localStorage.setItem('photo', this.imageToShow);
       console.log(reader.result);
    }, false);
  
    if (image) {
       reader.readAsDataURL(image);
    }
  }
  /** */
  /*
  getRol(){
    this.currentUser = this._Token.getUser();
    this.rol = this.currentUser.roles[0];
    console.log(this.rol);
  }*/


}
