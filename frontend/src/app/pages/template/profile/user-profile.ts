export class UserProfile {
    displayName: string;
    email: string;
    enabled: true
    givenName: string;
    id: number;
    jobTitle: string;
    mobilePhone: string;
    modifiedDate: Date;
    notifications: boolean;
    provider: string;
    providerUserId: string;
    surname: string;
    createdDate: Date;
    roles: any;
}