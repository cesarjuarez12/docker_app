import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { TokenStorageService } from '../../../auth/token-storage.service';
import { InfoUserService } from '../../../services/info-user.service';
import { UserProfile } from './user-profile';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  currentUser: any;
  userProfile: UserProfile = new UserProfile();
  groups: Array<string> = new Array();

  src: SafeResourceUrl = undefined;
  img = '';

  constructor(
    private token: TokenStorageService, 
    private UserServ: InfoUserService, 
    private sanitizer: DomSanitizer, 
    private loadingService: NgxUiLoaderService) { 
    
    

    this.UserServ.get_photo().subscribe((response: any) => 
      {
        this.src = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(response));
      }  
    )
  }

  ngOnInit(): void {
    this.currentUser = this.token.getUser();
    this.get_Info();
    this.getGroups();
  }

  cargar_info(){
    this.token.saveGeneralInfo();
  }

  get_Info() {
    this.UserServ.get_info().subscribe((response: any) => 
      {
       this.userProfile = response;
      }  
    )
    this.img =localStorage.getItem('photo');
  }

  getGroups(){
    if(this.currentUser.roles.includes("ROLE_ADMIN")){
      this.groups.push("Administradores")
    }
    if(this.currentUser.roles.includes("ROLE_ADMINFUNCIONAL")){
      this.groups.push("Administradores Funcionales")
    }
    if(this.currentUser.roles.includes("ROLE_COORDINADOR")){
      this.groups.push("Coordinadores")
    }
    if(this.currentUser.roles.includes("ROLE_INGENIERO")){
      this.groups.push("Ingenieros")
    }
    if(this.currentUser.roles.includes("ROLE_SOLICITANTE")){
      this.groups.push("Solicitantes")
    }
  }

 
}
