import { Component, OnInit } from '@angular/core';
import { UserService } from '../auth/user.service';
import { TokenStorageService } from '../auth/token-storage.service';
import { ActivatedRoute } from '@angular/router';
import { AppConstants } from '../common/app.constants';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DepartamentoInternoService } from '../departamentos/departamentos-internos/departamento-interno.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  currentUser: any;
  azureURL = AppConstants.AZURE_AUTH_URL;

  constructor(private dpService: DepartamentoInternoService, private tokenStorage: TokenStorageService, private route: ActivatedRoute, private userService: UserService, private loadingService: NgxUiLoaderService) { }

  ngOnInit(): void {

    this.dpService.getDepartamentosInternos().subscribe(data => console.log(data), error => console.log(error))

    const error: string = this.route.snapshot.queryParamMap.get('error');
    const token: string = this.route.snapshot.queryParamMap.get('token');


    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.currentUser = this.tokenStorage.getUser();
      window.location.replace('/inicio');
    }
    else if (token) {
      this.tokenStorage.saveToken(token);
      this.userService.getCurrentUser().subscribe(
        data => {
          this.loadingService.start();
          this.login(data);
        },
        err => {
          console.log(err)
          this.tokenStorage.signOut();
          if (token == 'group_error') {
            this.errorMessage = 'No pertenece a un grupo válido de Azure Active Directory para utilizar esta aplicación. Contacte al administrador para más información.';
          } else {
            this.errorMessage = err.error.message
          }
          this.isLoginFailed = true;
        }
      );
    }
    else if (error) {
      this.errorMessage = error;
      console.log(error);
      this.isLoginFailed = true;
    }
  }

  /*onSubmit(): void {
    this.authService.login(this.form).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.login(data.user);
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }*/

  async login(user: string): Promise<void> {
    await this.tokenStorage.saveGeneralInfo();
    await this.tokenStorage.saveUser(user);
    this.isLoginFailed = false;
    this.isLoggedIn = true;
    this.currentUser = this.tokenStorage.getUser();
    setTimeout(this.activo, 2000);
  }

  activo() {
    let x = JSON.parse(sessionStorage.getItem('ID_SOLICITUD'));
    if (x) {
      window.location.replace('/solicitud/' + x);
    } else {
      window.location.replace('/inicio');
    }
  }

  link(): void {
    console.log( this.azureURL)
    window.location.href = this.azureURL;
  }

}
