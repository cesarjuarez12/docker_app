import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigRequestsComponent } from './config-requests.component';

describe('ConfigRequestsComponent', () => {
  let component: ConfigRequestsComponent;
  let fixture: ComponentFixture<ConfigRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigRequestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
