import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { alert } from 'src/app/common/alerts';
import { ConfiguracionGlobal } from '../configuracion-global/configuracion-global';
import { ConfiguracionGlobalService } from '../configuracion-global/configuracion-global.service';

@Component({
  selector: 'app-config-requests',
  templateUrl: './config-requests.component.html',
  styleUrls: ['./config-requests.component.css']
})
export class ConfigRequestsComponent implements OnInit {


  faExclamationTriangle = faExclamationTriangle

  conForm: FormGroup
  conf: ConfiguracionGlobal = new ConfiguracionGlobal()
  loading = false

  constructor(private configuracionService: ConfiguracionGlobalService) { }

  getConfig() {
    this.configuracionService.getConfiguracionGlobal().subscribe((conf) => {
      this.conf = conf
      this.conForm.controls['maxServers'].setValue(this.conf.maxServers)
    }, (error) => {
      alert.alertError("¡Tenemos problemas para obtener la configuración!")
    })
  }

  createForm() {
    this.conForm = new FormGroup({
      maxServers: new FormControl('', [Validators.required, Validators.min(1)])
    })
  }

  submit() {
    this.loading = true
    this.conf.maxServers = this.conForm.value['maxServers']
    this.configuracionService.patch(this.conf).subscribe((data) => {
      this.loading = false
      this.conf = data.conf;
      alert.alertOk("Configuración guardada correctamente!")
    },
      error => {
        this.loading = false
        alert.alertError("Ocurrió un error al guardar la configuración")
      })
  }

  ngOnInit(): void {
    this.createForm();
    this.getConfig();
  }

}
