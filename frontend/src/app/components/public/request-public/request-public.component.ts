import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-request-public',
  templateUrl: './request-public.component.html',
  styleUrls: ['./request-public.component.css']
})
export class RequestPublicComponent implements OnInit {

  paramID: any;

  constructor(private rutaActiva: ActivatedRoute, private ngxLoadingService: NgxUiLoaderService) { }

  ngOnInit(): void {
    this.ngxLoadingService.start();
    this.paramID = this.rutaActiva.snapshot.params['id'];
   // console.log(this.paramID);
    window.sessionStorage.setItem('ID_SOLICITUD', this.paramID);
    //window.localStorage.setItem('ID_SOLICITUD', this.paramID);
    
    let x = JSON.parse(sessionStorage.getItem('ID_SOLICITUD'));
    if(x) { 
      window.location.replace('/login');
     }
  }

}
