import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestPublicComponent } from './request-public.component';

describe('RequestPublicComponent', () => {
  let component: RequestPublicComponent;
  let fixture: ComponentFixture<RequestPublicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestPublicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestPublicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
