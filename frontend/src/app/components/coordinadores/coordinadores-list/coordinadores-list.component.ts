import { Component, OnInit, ViewChild } from '@angular/core';
import { faFlag } from '@fortawesome/free-solid-svg-icons';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { data } from 'jquery';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/auth/user.service';
import { alert } from 'src/app/common/alerts';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-coordinadores-list',
  templateUrl: './coordinadores-list.component.html',
  styleUrls: ['./coordinadores-list.component.css']
})
export class CoordinadoresListComponent implements OnInit {

  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  faFlag = faFlag;



  coordinadores!: any[];

  temp!: any[];

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private userService: UserService, private loadingService: NgxUiLoaderService) { 
  }


  getCoordinadores(){
    //this.loadingService.start()
    this.userService.getCoordinadores().subscribe( (data) => {
      this.temp = [...data]
      this.coordinadores = data
      //this.loadingService.stop()
    }, error => {
      //this.loadingService.stop()
      alert.alertError("Ocurrió un error al intentar obtener los coordinadores")
    });
  }


  find(event?: any){
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.displayName.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.coordinadores = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;

  }

  notif(id: number){
    this.userService.changeNotifications(id)
      .subscribe((data) => {
       alert.alertOk("Usuario actualizado")
        this.userService.getCoordinadores().subscribe( (data) => this.coordinadores = data);
      }, 
      err => {
        alert.alertError("Ocurrió un error")
        console.log(err);
      })
  }

  ngOnInit(): void {
    this.getCoordinadores()
  }

}
