import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoordinadoresListComponent } from './coordinadores-list.component';

describe('CoordinadoresListComponent', () => {
  let component: CoordinadoresListComponent;
  let fixture: ComponentFixture<CoordinadoresListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoordinadoresListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoordinadoresListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
