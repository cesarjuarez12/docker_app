import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, map, tap } from 'rxjs/operators';
import { Ayuda } from "./ayuda";
import { VideoAyuda } from "./video-ayuda";

@Injectable()
export class AyudaService {

  
  private urlEndPoint: string = 'api/ayuda/';

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) { }

  getAyuda(): Observable<Ayuda> {
    return this.http.get<Ayuda>(`${this.urlEndPoint}`, { headers: this.httpHeaders })
  }

  getManual() {
    return this.http.get('api/ayuda/manual', { headers: this.httpHeaders, responseType: 'blob' });
  }
  
  getManualIE() {
      return this.http.get('api/ayuda/manual' , { responseType: "blob" }).pipe(map(result => {
        const blob = new Blob([result], { type: 'application/pdf' });
        //return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
        return blob;
      }));
    }

  getVideo(id: number): Observable<VideoAyuda> {
    return this.http.get<VideoAyuda>(`${this.urlEndPoint}videos/` + id, { headers: this.httpHeaders })
  }

  getVideoFile(idVideoDescription: number) {
    return this.http.get('api/ayuda/video/' + idVideoDescription, { headers: this.httpHeaders, responseType: 'blob' });
  }



  getVideosAyuda(): Observable<any> {
    return this.http.get<any>(this.urlEndPoint + 'videos', { headers: this.httpHeaders })
  }

  subirManual(archivo: File): Observable<HttpEvent<{}>> {
    let formData = new FormData();
    formData.append("archivo", archivo);
    const req = new HttpRequest('POST', `${this.urlEndPoint}manual/upload`, formData, {
      reportProgress: true,
    });
    return this.http.request(req).pipe(
      catchError(e => {
        console.log(e);
        return throwError(e);
      })
    );
  }

  subirVideo(archivo: File, videoAyuda: VideoAyuda): Observable<HttpEvent<{}>> {

    let formData = new FormData();
    formData.append("archivo", archivo);
    formData.append("name", videoAyuda.name);

    const req = new HttpRequest('POST', `${this.urlEndPoint}video/upload`, formData, {
      reportProgress: true
    });

    return this.http.request(req).pipe(
      catchError(e => {
        return throwError(e);
      })
    );

  }

  updateVideo(archivo: File, videoAyuda: VideoAyuda): Observable<HttpEvent<{}>> {

    let formData = new FormData();
    formData.append("archivo", archivo);
    formData.append("name", videoAyuda.name);
    formData.append("id", videoAyuda.id.toString());

    const req = new HttpRequest('PUT', `${this.urlEndPoint}video/upload`, formData, {
      reportProgress: true
    });

    return this.http.request(req).pipe(
      catchError(e => {
        return throwError(e);
      })
    );
  }

  deleteVideo(id: number): Observable<any> {
    return this.http.delete<any>(`${this.urlEndPoint}videos/${id}`, { headers: this.httpHeaders })
  }



}