import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { faPen, faTrash } from '@fortawesome/free-solid-svg-icons';
import { AyudaService } from '../ayuda.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  urlVideo: SafeResourceUrl = undefined;
  faPen = faPen;
  faTrash = faTrash;

  constructor(private ayudaService: AyudaService, private  router: Router, private activatedRoute: ActivatedRoute, private sanitizer: DomSanitizer ) { 
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.ayudaService.getVideoFile(id).subscribe((video) => {
          this.urlVideo = sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(video));
          console.log(this.urlVideo)
        })
      }
    })
  }

  ngOnInit(): void {
  }

}
