import { Component, OnInit } from '@angular/core';
import { Ayuda } from '../ayuda';
import { AyudaService } from '../ayuda.service';
import { VideoAyuda } from '../video-ayuda';
import { faDownload, faPlay } from '@fortawesome/free-solid-svg-icons';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import * as dayjs from 'dayjs';
import { RoleService } from 'src/app/auth/role.service';
import { ActivatedRoute } from '@angular/router';
import { alert } from 'src/app/common/alerts';
import { IEBrowserService } from 'src/app/services/ie-browser.service';


@Component({
  selector: 'app-ayuda-solicitantes',
  templateUrl: './ayuda-solicitantes.component.html',
  styleUrls: ['./ayuda-solicitantes.component.css']
})
export class AyudaSolicitantesComponent implements OnInit {
  faDownload = faDownload;
  faPlay = faPlay;


  ayuda: Ayuda = new Ayuda();
  pdfSrc: SafeResourceUrl = '';
  videoUrl: SafeResourceUrl = '';
  videosAyuda: VideoAyuda[] = null;
  page = 1;
  load = true;
  videoTabActive = false;
  IEUrl: any;
  IE: boolean;
  fileUrlDowload: any;
  Hoy = new Date();
  HoyFormat = dayjs(this.Hoy).format('DD-MM-YYYY');


  constructor(
    private BrowserDetection: IEBrowserService,
    private ayudaService: AyudaService, 
    private sanitizer: DomSanitizer, 
    public roleService: RoleService,  
    private activatedRoute: ActivatedRoute) {
  }

  getAyuda() {
    this.ayudaService.getAyuda().subscribe((ayuda) => {
      this.ayuda = ayuda
    })

    this.ayudaService.getManual().subscribe((manual) => {
      this.pdfSrc = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(manual));
    }, error => {
      alert.alertError("Ocurrió un error al tratar de obtener el manual...")
    })


  }

  openTab() {
    this.activatedRoute.fragment.subscribe(fragment => {     
      if (fragment && fragment == 'pills-tab5') {
        this.videoTabActive = true;
      }
    });
  }

  async ngOnInit(): Promise<void> {
    this.IE = await this.BrowserDetection.detectionBrowser();
    if(this.IE == false){
      this.getAyuda();
    }
    this.openTab();
  }

  /*ViewIE(){
    this.ayudaService.getManualIE().subscribe((response) => {

      
      const blobToBase64 = (res: any) => {
        const reader = new FileReader();
        reader.readAsDataURL(res);
        return new Promise(resolve => {
          reader.onloadend = () => {
            resolve(reader.result);
          };
        });
      };

      blobToBase64(response).then(res => {
        // do what you wanna do
       this.IEUrl = res;
       console.log(this.IEUrl); // res is base64 now
      });
      
    }, error => {
      this.loadingService.stop();
    })
  }*/
  
  IEDowload(){
    this.ayudaService.getManualIE().subscribe((response) => {
      console.log(response);
      this.fileUrlDowload = response;
      var namefile = "Manual_Solicitante" + this.HoyFormat + ".pdf";
      window.navigator.msSaveBlob(response,namefile);
    }, error => {
      alert.alertError("Ocurrió un error al tratar de obtener el manual...");
    })
  }

}

