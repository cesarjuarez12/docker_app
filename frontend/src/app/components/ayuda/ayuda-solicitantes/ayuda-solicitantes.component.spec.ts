import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AyudaSolicitantesComponent } from './ayuda-solicitantes.component';

describe('AyudaSolicitantesComponent', () => {
  let component: AyudaSolicitantesComponent;
  let fixture: ComponentFixture<AyudaSolicitantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AyudaSolicitantesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AyudaSolicitantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
