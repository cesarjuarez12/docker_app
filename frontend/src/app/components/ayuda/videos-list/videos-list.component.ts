import { Component, OnInit } from '@angular/core';
import { faPen, faPlay, faTrash } from '@fortawesome/free-solid-svg-icons';
import { RoleService } from 'src/app/auth/role.service';
import { alert } from 'src/app/common/alerts';
import Swal from 'sweetalert2';
import { AyudaService } from '../ayuda.service';
import { VideoAyuda } from '../video-ayuda';

@Component({
  selector: 'app-videos-list',
  templateUrl: './videos-list.component.html',
  styleUrls: ['./videos-list.component.css']
})
export class VideosListComponent implements OnInit {

  faPlay = faPlay;
  faPen = faPen;
  faTrash = faTrash;

  videos: VideoAyuda[];

  constructor(private ayudaService: AyudaService, public roleService: RoleService) {
    this.getVideos();    
  }

  getVideos() {
    this.ayudaService.getVideosAyuda().subscribe((data) => {
      this.videos = data.content as VideoAyuda[];
    })
  }

  delete(id: number): void {
    Swal.fire({
      title: '¿Seguro que desea eliminar el video?',  
      text: 'No podrá deshacer esta acción',  
      icon: 'warning',  
      customClass: {
        title: 'text-primary-1 font-weight-bold',
        confirmButton: 'btn btn-primary btn-sm mr-2',
        cancelButton: 'btn btn-secondary btn-sm'
      },
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      backdrop: false,
      allowOutsideClick: false,
      input: null,
      inputAttributes: {
        type: 'hidden'
      },
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.ayudaService.deleteVideo(id).subscribe(
          () => {
            alert.alertOk("Video Eliminado Correctamente")
            this.getVideos();
          }
        )

      }
    });
  }

  ngOnInit(): void {
  }

}
