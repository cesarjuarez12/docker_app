import { HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import Swal from 'sweetalert2';
import { Ayuda } from '../ayuda';
import { AyudaService } from '../ayuda.service';

@Component({
  selector: 'app-ayuda-configuracion',
  templateUrl: './ayuda-configuracion.component.html',
  styleUrls: ['./ayuda-configuracion.component.css']
})
export class AyudaConfiguracionComponent implements OnInit {

  ayuda: Ayuda = new Ayuda();

  progreso: number = 0;
  manualSeleccionado: File;
  loadingUpload = false;

  constructor(private ayudaService: AyudaService, private sanitizer: DomSanitizer, private loadingService: NgxUiLoaderService, private router: Router) {
    this.getAyuda();
  }

  getAyuda() {
    this.ayudaService.getAyuda().subscribe((ayuda) => {
      this.ayuda = ayuda
    })
  }



  ngOnInit(): void {
  }

  seleccionarManual(event?: any) {
    this.manualSeleccionado = event.target.files[0];
    this.progreso = 0;
    console.log(this.manualSeleccionado);
    if (this.manualSeleccionado.type.indexOf('application/pdf') < 0) {
      alert.alertError("Error, debe ser un archivo PDF")      
      this.manualSeleccionado = null;
    }
  }

  subirManual() {
    if (!this.manualSeleccionado) {
      Swal.fire('Error: ', 'Debe seleccionar un manual', 'error');
    } else {
      this.loadingUpload = true;
      //this.loadingService.start();
      this.ayudaService.subirManual(this.manualSeleccionado)
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progreso = Math.round((event.loaded / event.total) * 100);
            console.log(this.progreso)
          } else if (event.type === HttpEventType.Response) {
            let response: any = event.body;
            this.ayuda = response.ayuda;
            this.loadingUpload = false;
            this.router.navigateByUrl("/ayuda")
            //this.loadingService.stop();
            alert.alertOk("El manual ha sido guardado correctamente")
          }
        }, error => {
          this.loadingUpload = false;
          alert.alertError("Ocurrió un error al intentar subir el manual, verifique el tipo de archivo")
          //this.loadingService.stop();
        });
    }
  }
}
