import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AyudaConfiguracionComponent } from './ayuda-configuracion.component';

describe('AyudaConfiguracionComponent', () => {
  let component: AyudaConfiguracionComponent;
  let fixture: ComponentFixture<AyudaConfiguracionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AyudaConfiguracionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AyudaConfiguracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
