import { HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import Swal from 'sweetalert2';
import { AyudaService } from '../ayuda.service';
import { VideoAyuda } from '../video-ayuda';

@Component({
  selector: 'app-nuevo-video-ayuda',
  templateUrl: './nuevo-video-ayuda.component.html',
  styleUrls: ['./nuevo-video-ayuda.component.css']
})
export class NuevoVideoAyudaComponent implements OnInit {

  videoAyuda: VideoAyuda = new VideoAyuda();
  faExclamationTriangle = faExclamationTriangle;

  progreso: number = 0;
  videoSeleccionado: File;

  constructor(private ayudaService: AyudaService, private router: Router,
    private activatedRoute: ActivatedRoute, private loadingService: NgxUiLoaderService) {
  }

  seleccionarVideo(event?: any) {
    this.loadingService.start();
    this.videoSeleccionado = event.target.files[0];
    this.progreso = 0;
    console.log(this.videoSeleccionado);
    if (this.videoSeleccionado.type.indexOf('video') < 0) {
      alert.alertError("Debe seleccionar un archivo de video válido...")
      this.videoSeleccionado = null;
    }
    this.loadingService.stop();
  }

  editVideo() {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.loadingService.start();
        this.ayudaService.getVideo(id).subscribe((video) => {
          this.videoAyuda = video;
          this.loadingService.stop();
        },
          error => {
            this.loadingService.stop();
            alert.alertError("Algo salió mal, no pudimos obtener el video seleccionado.")
            window.history.back();
          }
        )
      }
    })
  }

  save() {
    this.loadingService.start();

    if (!this.videoSeleccionado) {
      this.loadingService.stop();
      alert.alertError("Error, debe seleccionar un video...")
    } else {
      this.ayudaService.subirVideo(this.videoSeleccionado, this.videoAyuda)
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progreso = Math.round((event.loaded / event.total) * 100);
          } else if (event.type === HttpEventType.Response) {
            let response: any = event.body;
            this.videoAyuda = response.videoAyuda as VideoAyuda;
            this.loadingService.stop();
            alert.alertOk("¡Video guardado correctamente!")
            this.router.navigateByUrl('/ayuda#pills-tab5')
          }
        }, error => {
          this.loadingService.stop();
          alert.alertError("Ocurrió un error al guardar el video...")
        });
    }
  }

  update() {
    this.loadingService.start();

    this.ayudaService.updateVideo(this.videoSeleccionado, this.videoAyuda)
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progreso = Math.round((event.loaded / event.total) * 100);
        } else if (event.type === HttpEventType.Response) {
          let response: any = event.body;
          this.videoAyuda = response.videoAyuda as VideoAyuda;
          this.loadingService.stop();
          alert.alertOk("¡Video guardado correctamente!")
          this.router.navigateByUrl('/ayuda#pills-tab5')
        }
      }, error => {
        this.loadingService.stop();
        alert.alertError("Ocurrió un error al guardar el video...")
      });
  }

  ngOnInit(): void {
    this.editVideo();
  }

}
