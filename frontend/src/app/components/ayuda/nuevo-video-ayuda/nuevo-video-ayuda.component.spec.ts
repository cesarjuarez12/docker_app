import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoVideoAyudaComponent } from './nuevo-video-ayuda.component';

describe('NuevoVideoAyudaComponent', () => {
  let component: NuevoVideoAyudaComponent;
  let fixture: ComponentFixture<NuevoVideoAyudaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevoVideoAyudaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoVideoAyudaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
