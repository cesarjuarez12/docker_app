export class VideoAyuda{
    id!: number;
    name!: string;
    createdBy!: string;
    createdDate!: Date;
    lastModifiedBy!: string;
    lastModifiedDate!: Date;
}
