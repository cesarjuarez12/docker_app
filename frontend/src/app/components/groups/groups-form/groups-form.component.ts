import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { alert } from 'src/app/common/alerts';
import Swal from 'sweetalert2';
import { GroupAllowed } from '../group';
import { GroupService } from '../group.service';
import { RoleEnum } from '../role-enum';

@Component({
  selector: 'app-groups-form',
  templateUrl: './groups-form.component.html',
  styleUrls: ['./groups-form.component.css']
})
export class GroupsFormComponent implements OnInit {

  faExclamationTriangle = faExclamationTriangle;

  group: GroupAllowed = new GroupAllowed();
  form: FormGroup;
  id: number;

  roles = [
    {id: 'ROLE_ADMIN', name: 'Administrador'},
    {id: 'ROLE_ADMINFUNCIONAL', name: 'Administrador Funcional'},
    {id: 'ROLE_COORDINADOR', name: 'Coordinador'},
    {id: 'ROLE_INGENIERO', name: 'Ingeniero'},
    {id: 'ROLE_SOLICITANTE', name: 'Solicitante'},
  ]

  constructor(private groupService: GroupService, private router: Router,
    private activatedRoute: ActivatedRoute, private tokenStorageService: TokenStorageService) { }


    createForm() {
      this.form = new FormGroup({
        name: new FormControl('', [
          Validators.required
        ]),
        roleToAssign: new FormControl('', [Validators.required])
      })
    }

  getGrupo(): void{
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id']
      if (this.id) {
        this.groupService.getGroupAllowed(this.id).subscribe((data) => {
            this.group = data as GroupAllowed;
            console.log(this.group)
            this.form.patchValue(this.group);
        });
      }
    })
  }

  submit(){


    Swal.fire({
      title: '¿Seguro que desea editar este grupo e iniciar sesión nuevamente?',
      html: '<p class="text-danger">No podrá deshacer esta acción</p>',
      icon: 'warning',
      customClass: {
        title: 'text-primary-1 font-weight-bold',
        confirmButton: 'btn btn-primary btn-sm mr-2',
        cancelButton: 'btn btn-secondary btn-sm'
      },
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      backdrop: false,
      allowOutsideClick: false,
      input: null,
      inputAttributes: {
        type: 'hidden'
      },
      buttonsStyling: false
    }).then((result) => {
      if (result.value) { 
        this.group = this.form.value;
        this.group.id = this.id;
        this.groupService.put(this.group).subscribe((data =>{
          alert.alertOk("Grupo actualizado correctamente")
          this.tokenStorageService.signOut();
          localStorage.clear();
          window.location.replace('/login');
        }),
        (error => {
          alert.alertError("Ocurrió un error al intentar guardar el grupo")
        }))
      }
    });
  }


  ngOnInit(): void {
    this.createForm();
    this.getGrupo();
  }

}
