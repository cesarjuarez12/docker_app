import { RoleEnum } from "./role-enum";

export class GroupAllowed {
    id:number;
    name:string;
    roleToAssign: RoleEnum;
    lastModifiedBy: string;
    lastModifiedDate: Date;
}