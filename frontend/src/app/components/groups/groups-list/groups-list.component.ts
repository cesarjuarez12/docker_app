import { Component, OnInit, ViewChild } from '@angular/core';
import { faEdit, faEye, faPen, faSearch, faTimes, faTrash } from '@fortawesome/free-solid-svg-icons';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import { GroupAllowed } from '../group';
import { GroupService } from '../group.service';

@Component({
  selector: 'app-groups-list',
  templateUrl: './groups-list.component.html',
  styleUrls: ['./groups-list.component.css']
})
export class GroupsListComponent implements OnInit {

  title = "CONFIGURACIÓN GRUPOS DE AZURE AD"

  groups: GroupAllowed[];
  temp: GroupAllowed[];
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;

  faSearch = faSearch;
  faEdit = faEdit;
  faTimes = faTimes;
  faTrash = faTrash;
  faPen = faPen;
  faEye = faEye;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private groupService: GroupService, private loadingService: NgxUiLoaderService) {
   }

  getGroupsAllowed(): void{
      //this.loadingService.start()
      this.groupService.getGroupsAllowed().subscribe((data) => {
        this.groups = data as GroupAllowed[];
        //this.loadingService.stop()
      }, error => {
        alert.alertError("Ocurrió un error al intentar obtener los grupos")
        //this.loadingService.stop()
      })
  }

  ngOnInit(): void {
    this.getGroupsAllowed();
  }

}
