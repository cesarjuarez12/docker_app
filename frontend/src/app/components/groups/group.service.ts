import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { GroupAllowed } from "./group";

@Injectable({
    providedIn: 'root'
  })
export class GroupService {


    private urlEndPoint: string =  'api/groups-allowed/';

    private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })
    
    constructor(private http: HttpClient) { }

    getGroupsAllowed(): Observable<any> {
        return this.http.get<any>(`${this.urlEndPoint}`, { headers: this.httpHeaders })
    }

    getGroupAllowed(id: number): Observable<any> {
        return this.http.get<any>(`${this.urlEndPoint}` + id, { headers: this.httpHeaders })
    }

    put(groupAllowed: GroupAllowed): Observable<any> {
        return this.http.put<any>(`${this.urlEndPoint}`, groupAllowed, { headers: this.httpHeaders })
    }

}