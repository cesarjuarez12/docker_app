import { Component, OnInit } from '@angular/core';
import { faServer } from '@fortawesome/free-solid-svg-icons';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { alert } from 'src/app/common/alerts';
import { FileService } from '../file.service';
import { Request } from '../request';
import { RequestService } from '../request.service';

@Component({
  selector: 'app-request-details-modal',
  templateUrl: './request-details-modal.component.html',
  styleUrls: ['./request-details-modal.component.css']
})
export class RequestDetailsModalComponent implements OnInit {


  title: string;
  idRequest: number
  request: Request = new Request()
  tabs: any[] = []
  selectedServer = 1

  faServer = faServer

  constructor(public bsModalRef: BsModalRef, private requestService: RequestService, private fileService: FileService) { }

  ngOnInit(): void {
    this.getRequest()
  }

  generateTabs() {
    for (let index = 1; index <= this.request.numberOfServers; index++) {
      const newTabIndex = index;
      this.tabs.push({
        id: index,
        title: `Servidor ${newTabIndex}`,
        content: ``,
        disabled: false,
        removable: false,
        active: false
      });
    }
    this.tabs.find(tab => tab.id == 1).active = true;
  }

  selectTab(tabz: any) {
    this.selectedServer = tabz.id
  }

  getRequest() {
    this.requestService.getRequest(this.idRequest).subscribe(
      (data) => {
        this.request = data
        this.generateTabs()
      },
      (error) => alert.alertError("Ocurrió un error al tratar de obtener los detalles de la solicitud!"))
  }


  getFile(field: any) {
    this.fileService.getFile(field, this.request.id)
  }

  getBaseFile(field: any) {
    this.fileService.getBaseFile(field, this.request.id)
  }

}
