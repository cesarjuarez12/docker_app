import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialCoordinadorComponent } from './historial-coordinador.component';

describe('HistorialCoordinadorComponent', () => {
  let component: HistorialCoordinadorComponent;
  let fixture: ComponentFixture<HistorialCoordinadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistorialCoordinadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialCoordinadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
