import { Component, OnInit } from '@angular/core';
import { faEdit, faEye, faPen, faSearch, faTimes, faTrash, faCheckSquare, faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import { Page } from '../../users/users-list/page';
import { RequestService } from '../request.service';
import { ConfigVariables } from '../../../config/Variables';
import { FormulariosService } from '../../formularios/formulario.service';
import * as dayjs from 'dayjs';
import { _ } from 'core-js';
import { Request } from '../request';
import { alert } from 'src/app/common/alerts';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { cosh } from 'core-js/core/number';
import { IEBrowserService } from 'src/app/services/ie-browser.service';
var localizedFormat = require('dayjs/plugin/localizedFormat');


@Component({
  selector: 'app-historial-coordinador',
  templateUrl: './historial-coordinador.component.html',
  styleUrls: ['./historial-coordinador.component.css']
})
export class HistorialCoordinadorComponent implements OnInit {

  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  page = new Page();
  AllRequest: any[] = [];

  faCheckSquare = faCheckSquare;
  faEye = faEye;
  faPen = faPen;
  faTrash = faTrash;
  faFilePdf = faFilePdf

  Hoy = new Date();
  HoyFormat = dayjs(this.Hoy).format('DD-MM-YYYY');// H:m:ss

  options: any = [
    {
      value: 'COMPLETADA',
      description: 'COMPLETADA'
    },
    {
      value: 'ASIGNADA',
      description: 'ASIGNADA'
    },
    {
      value: 'ENVIADA',
      description: 'ENVIADA'
    },
    {
      value: 'RECHAZADA',
      description: 'RECHAZADA'
    }
  ];

  requestType: any = [];

  optionSelectEstado: string;
  optionSelectRequestType: any;
  enginnerName: string;
  nameSolicitante: string;

  fecha_inicial: Date;
  fecha_final: Date;

  opcionesSeleccionadas: any = [
    {
      estado_solicitud: '',
      fecha_inicial: '',
      fecha_final: '',
      nombre_del_solicitante: '',
      tipo_de_requerimiento: '',
      ing_asignado: '',
    }
  ];

  scrollBarHorizontal = (window.innerWidth < 1200);

  UrlFilter: any;
  fileUrlDowload: any;

  //Solicitantes
  ArraySolicitantes: any = [];
  ArrayIngenieros: any = [];

  //IE Env
  IE: boolean;

  //Persistencia Filtrado
  Filtros: any [] = [];

  constructor(
    private BrowserDetection: IEBrowserService,
    private _formService: FormulariosService,
    private _requestService: RequestService,
    private loadingService: NgxUiLoaderService,
    private localeService: BsLocaleService) {
    this.localeService.use('es');
    window.onresize = () => {
      this.scrollBarHorizontal = (window.innerWidth < 1200);
    };
    this.page.pageNumber = 0;
    this.page.size = 10;
  }


  async ngOnInit(): Promise<void> {
    this.IE = await this.BrowserDetection.detectionBrowser();
    this.PersistenciaFiltradoRead();
    this.setPage({ offset: 0 });
    this.getRequestType();
    this.getSolicitantes();
    this.getIngenieros();
  }

  find(_event?: any) { }

  setPage(pageInfo?: any) {
    //this.loadingService.start();
    this.page.pageNumber = pageInfo.offset;
    this._requestService.getRecordCordinadores(this.page, false).subscribe((response) => {
      this.page.totalElements = response.totalElements;
      this.page.pageNumber = response.number;
      this.page.size = response.size;
      this.page.totalPages = response.totalPages;
      this.AllRequest = response.content;
      //this.loadingService.stop();
    }, err => {
      alert.alertError(err.error.error);
  });
  }

  setPageFilter(pageInfo: any, filtro: string) {
    this.page.pageNumber = pageInfo.offset;
    this._requestService.getRecordCoordinadoresFilter(this.page, false, filtro).subscribe((response) => {
      this.page.totalElements = response.totalElements;
      this.page.pageNumber = response.number;
      this.page.size = response.size;
      this.page.totalPages = response.totalPages;
      this.AllRequest = response.content;
    }, err => {
      alert.alertError(err.error.error);
    });
  }

  getRequestType() {
    this._formService.getAllRequesType().subscribe((response) => {
      this.requestType = response.content;
      //this.loadingService.stop();
    })
  }

  getSolicitantes(){
    this._formService.getSolcitantes().subscribe((response)=>{
     this.ArraySolicitantes = response;
    })
  }

  getIngenieros(){
    this._formService.getIngenieros().subscribe((response)=>{
     this.ArrayIngenieros = response;
    })
  }

  filtrar(filtro: string) {
    //let Filtros: string[] = this.Filtros;
    //this.loadingService.start()
    if (filtro == 'estado_solicitud') {
      if (this.optionSelectEstado) {
        var x = 'status=' + this.optionSelectEstado;
        this.opcionesSeleccionadas.map( (dato: any) => {
          
          for (let i = 0; i < this.Filtros.length; i++) {
            if(this.Filtros[i].substr(0, 7) == 'status='){
              this.Filtros[i] = x;
              this.Filtros.splice(i, 1);
            }
          }
          this.Filtros.push(x);

          dato.estado_solicitud = x;
          
        });
      } else {
        this.opcionesSeleccionadas.map( (dato: any) => {
          dato.estado_solicitud = '';

          for (let i = 0; i < this.Filtros.length; i++) {
             if(this.Filtros[i].substr(0, 7) == 'status='){
               this.Filtros.splice(i, 1);
             }
           }

        });
      }
    } else if (filtro == 'fecha_inicial') {
      if (this.fecha_inicial) {
        var x = 'from=' + dayjs(this.fecha_inicial).format('DD-MM-YYYY');
        this.opcionesSeleccionadas.map( (dato: any) => {
          
          for (let i = 0; i < this.Filtros.length; i++) {
            if(this.Filtros[i].substr(0, 5) == 'from='){
              this.Filtros[i] = x;
              this.Filtros.splice(i, 1);
            }
          }
          this.Filtros.push(x);

          dato.fecha_inicial = x;
        });
      } else {
        this.opcionesSeleccionadas.map( (dato: any) => {
          dato.fecha_inicial = '';
          
          for (let i = 0; i < this.Filtros.length; i++) {
            if(this.Filtros[i].substr(0, 5) == 'from='){
              this.Filtros.splice(i, 1);
            }
          }

        });
      }
    } else if (filtro == 'fecha_final') {
      if (this.fecha_final) {
        var x = 'to=' + dayjs(this.fecha_final).format('DD-MM-YYYY');
        this.opcionesSeleccionadas.map( (dato: any) => {

          for (let i = 0; i < this.Filtros.length; i++) {
            if(this.Filtros[i].substr(0, 3) == 'to='){
              this.Filtros[i] = x;
              this.Filtros.splice(i, 1);
            }
          }
          this.Filtros.push(x);

          dato.fecha_final = x;
        });
      } else {
        this.opcionesSeleccionadas.map( (dato: any) => {
          dato.fecha_final = '';

          for (let i = 0; i < this.Filtros.length; i++) {
            if(this.Filtros[i].substr(0, 3) == 'to='){
              this.Filtros.splice(i, 1);
            }
          }

        });
      }
    } else if (filtro == 'nombre_solicitante') {
      if (this.nameSolicitante) {
        var x = 'createdBy=' + this.nameSolicitante;
        this.opcionesSeleccionadas.map( (dato: any) => {

          for (let i = 0; i < this.Filtros.length; i++) {
            if(this.Filtros[i].substr(0, 10) == 'createdBy='){
              this.Filtros[i] = x;
              this.Filtros.splice(i, 1);
            }
          }
          this.Filtros.push(x);

          dato.nombre_del_solicitante = x;
        });
      } else {
        this.opcionesSeleccionadas.map( (dato: any) => {
          dato.nombre_del_solicitante = '';

          for (let i = 0; i < this.Filtros.length; i++) {
            if(this.Filtros[i].substr(0, 10) == 'createdBy='){
              this.Filtros.splice(i, 1);
            }
          }

        });
      }
    } else if (filtro == 'tipo_requerimiento') {
      if (this.optionSelectRequestType) {
        var x = 'requestTypeId=' + this.optionSelectRequestType;
        this.opcionesSeleccionadas.map( (dato: any) => {

          for (let i = 0; i < this.Filtros.length; i++) {
            if(this.Filtros[i].substr(0, 14) == 'requestTypeId='){
              this.Filtros[i] = x;
              this.Filtros.splice(i, 1);
            }
          }
          this.Filtros.push(x);

          dato.tipo_de_requerimiento = x;
        });
      } else {
        this.opcionesSeleccionadas.map( (dato: any) => {
          dato.tipo_de_requerimiento = '';

          for (let i = 0; i < this.Filtros.length; i++) {
            if(this.Filtros[i].substr(0, 14) == 'requestTypeId='){
              this.Filtros.splice(i, 1);
            }
          }

        });
      }
    } else if (filtro == 'ingeniero_asignado') {
      if (this.enginnerName) {
        var x = 'engineerUsername=' + this.enginnerName;
        this.opcionesSeleccionadas.map( (dato: any) => {

          for (let i = 0; i < this.Filtros.length; i++) {
            if(this.Filtros[i].substr(0, 17) == 'engineerUsername='){
              this.Filtros[i] = x;
              this.Filtros.splice(i, 1);
            }
          }
          this.Filtros.push(x);

          dato.ing_asignado = x;
        });
      } else {
        this.opcionesSeleccionadas.map( (dato: any) => {
          dato.ing_asignado = '';

          for (let i = 0; i < this.Filtros.length; i++) {
            if(this.Filtros[i].substr(0, 17) == 'engineerUsername='){
              this.Filtros.splice(i, 1);
            }
          }

        });
      }
    }

    //this.Filtros = Filtros;
    //console.log(this.Filtros);
    this.PersistenciaFiltrado();
    //console.log(Filtros);

    this.enviar();
  }

  enviar() {

    let dates = Object.values(this.opcionesSeleccionadas[0]);

    var preUrl = '';
    for (let i = 0; i < dates.length; i++) {
      if (dates[i]) {
        if (preUrl !== null || preUrl !== '') {
          preUrl = preUrl + '&' + dates[i];
          //console.log(preUrl);
        }
      }
    }

    //console.log("?" + preUrl.substring(1));

    preUrl = "?" + preUrl.substring(1);
    if (preUrl) {
      //console.log(preUrl);
      this.setPageFilter({ offset: 0 }, preUrl);
      //this.loadingService.stop()
    } else {
      this.setPage({ offset: 0 });
      //this.loadingService.stop();
    }

  }

  DownloadFile() {

    let dates = Object.values(this.opcionesSeleccionadas[0]);

    var preUrl = '';
    for (let i = 0; i < dates.length; i++) {
      if (dates[i]) {
        if (preUrl !== null || preUrl !== '') {
          preUrl = preUrl + '&' + dates[i];
          //console.log(preUrl);
        }
      }
    }

    preUrl = "?" + preUrl.substring(1);
    if (preUrl) {
      this._requestService.getDownloadCoordinadores(preUrl).subscribe((response) => {
        this.fileUrlDowload = response;

        console.log(this.fileUrlDowload['changingThisBreaksApplicationSecurity']);
        const downloadLink = document.createElement('a');
        downloadLink.href = this.fileUrlDowload['changingThisBreaksApplicationSecurity'];

        downloadLink.setAttribute("download", "Historial_Solicitudes_" + this.HoyFormat + ".xlsx");
        document.body.appendChild(downloadLink);
        downloadLink.click();

      }, err => {
        console.error(err);
      });


      //this.loadingService.stop();
    } else {
      this.setPage({ offset: 0 });
      //this.loadingService.stop();
    }

  }

  getReport(request: Request) {
    this._requestService.getReportComplete(request);
  }

  getReportIE(request: Request){
    this.BrowserDetection.getReportCompleteIE(request);
  }


  IEDowload(){
    let dates = Object.values(this.opcionesSeleccionadas[0]);

    var preUrl = '';
    for (let i = 0; i < dates.length; i++) {
      if (dates[i]) {
        if (preUrl !== null || preUrl !== '') {
          preUrl = preUrl + '&' + dates[i];
          //console.log(preUrl);
        }
      }
    }

    preUrl = "?" + preUrl.substring(1);
    if (preUrl) {
      this._requestService.getDownloadIECoordinadores(preUrl).subscribe((response) => {
        this.fileUrlDowload = response;
        var namefile = "Historial_Solicitudes_" + this.HoyFormat + ".xlsx";
        window.navigator.msSaveBlob(response,namefile);
        console.log(response);
        console.log(namefile);
       /* if(window.navigator.msSaveOrOpenBlob) //IE & Edge
        {
          //msSaveBlob only available for IE & Edge
          window.navigator.msSaveBlob(this.fileUrlDowload['changingThisBreaksApplicationSecurity'],"ff-rocks-ie-sucks.txt");
        }
        else //Chrome & FF
        {
        console.log(this.fileUrlDowload['changingThisBreaksApplicationSecurity']);
        const downloadLink = document.createElement('a');
        downloadLink.href = this.fileUrlDowload['changingThisBreaksApplicationSecurity'];

        downloadLink.setAttribute("download", "Historial_Solicitudes_" + this.HoyFormat + ".xlsx");
        document.body.appendChild(downloadLink);
        downloadLink.click();*/
      //}

      }, err => {
        console.error(err);
        alert.alertError(err.error);
      });


      //this.loadingService.stop();
    } else {
      this.setPage({ offset: 0 });
      //this.loadingService.stop();
    }

  }

  onKeyUpFecha(x: any, model: any) {
      if (!/^([])\2(\d{4})$/i.test(x.target.value)) {
        x.target.value = x.target.value.replace(/[^]+/ig,"");
        if(model == 'fecha_inicial'){
          this.fecha_inicial = null;
        } else if(model == 'fecha_final'){
          this.fecha_final = null;
        }
    }
  }
  
  onKeydown(event: any, model: any) {
    if(event.code == 'Backspace'){
      if(model == 'fecha_inicial'){
        this.fecha_inicial = null;
      } else if(model == 'fecha_final'){
        this.fecha_final = null;
      }
    }
  }

  PersistenciaFiltrado(){
    sessionStorage.setItem('filtros_coord', JSON.stringify(this.Filtros));
  }

  PersistenciaFiltradoRead(){
    var guardado = sessionStorage.getItem('filtros_coord');
    //console.log('Guardado: ', JSON.parse(guardado));
    this.Filtros = JSON.parse(guardado);

    if(this.Filtros == null){ this.Filtros = []; }
    else {  
      for (let i = 0; i < this.Filtros.length; i++) {
          if(this.Filtros[i].substr(0, 7) == 'status='){ this.optionSelectEstado = this.Filtros[i].substr(7); this.opcionesSeleccionadas[0].estado_solicitud = this.Filtros[i] }
          
          if(this.Filtros[i].substr(0, 5) == 'from='){ 
            this.opcionesSeleccionadas[0].fecha_inicial = this.Filtros[i];
            let fecha_inicial = this.Filtros[i].substr(5).split('-');
            let day = Number(fecha_inicial[0]);
            let month = Number(fecha_inicial[1]);
            let year = Number(fecha_inicial[2]);
            
            dayjs.extend(localizedFormat)
            let total = year.toString() + '-' + month.toString() + '-' + day.toString();
            let fecha = dayjs(total).format('LLLL');
        
            //console.log(new Date(fecha));
            this.fecha_inicial = new Date(fecha);
             
          }

          if(this.Filtros[i].substr(0, 3) == 'to='){ 
            this.opcionesSeleccionadas[0].fecha_final = this.Filtros[i];
            let fecha_inicial = this.Filtros[i].substr(3).split('-');
            let day = Number(fecha_inicial[0]);
            let month = Number(fecha_inicial[1]);
            let year = Number(fecha_inicial[2]);
            
            dayjs.extend(localizedFormat)
            let total = year.toString() + '-' + month.toString() + '-' + day.toString();
            let fecha = dayjs(total).format('LLLL');
        
            this.fecha_final = new Date(fecha); 
          }
          
          if(this.Filtros[i].substr(0, 10) == 'createdBy='){ this.nameSolicitante = this.Filtros[i].substr(10); this.opcionesSeleccionadas[0].nombre_del_solicitante = this.Filtros[i]; }

          if(this.Filtros[i].substr(0, 14) == 'requestTypeId='){ this.optionSelectRequestType = Number(this.Filtros[i].substr(14)); this.opcionesSeleccionadas[0].tipo_de_requerimiento = this.Filtros[i]; }
          if(this.Filtros[i].substr(0, 17) == 'engineerUsername='){ this.enginnerName = this.Filtros[i].substr(17); this.opcionesSeleccionadas[0].ing_asignado = this.Filtros[i]; }
      }
    } 
  }

}
