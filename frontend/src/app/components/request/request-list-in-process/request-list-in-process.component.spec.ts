import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestListInProcessComponent } from './request-list-in-process.component';

describe('RequestListInProcessComponent', () => {
  let component: RequestListInProcessComponent;
  let fixture: ComponentFixture<RequestListInProcessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestListInProcessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestListInProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
