import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { faEye, faPen, faTrash } from '@fortawesome/free-solid-svg-icons';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import Swal from 'sweetalert2';
import { Page } from '../../users/users-list/page';
import { RequestService } from '../request.service';



@Component({
  selector: 'app-request-list-in-process',
  templateUrl: './request-list-in-process.component.html',
  styleUrls: ['./request-list-in-process.component.css']
})
export class RequestListInProcessComponent implements OnInit {

  title = ""

  STATUS_DRAFT = 'draft'
  STATUS_SEND = 'send'
    

  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  page = new Page();
  requests: Request[] = [];
  status: string = '';

  faEye = faEye;
  faPen = faPen;
  faTrash = faTrash;


  constructor(private requestService: RequestService, private ngxService: NgxUiLoaderService, private activatedRoute: ActivatedRoute) {
    this.page.pageNumber = 0;
    this.page.size = 10;
  }

  getPage(pageInfo?: any) {
    this.page.pageNumber = pageInfo.offset;

    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      //this.ngxService.start()


      if (this.activatedRoute.snapshot.paramMap.get('status')) {

         this.status = this.activatedRoute.snapshot.paramMap.get('status');

        if (this.status == 'draft') {
          this.requestService.getRequestInDraftByUser(this.page).subscribe((response) => {
            console.log(response);
            this.setPage(response);
            this.requests = response.content;
            this.title="SOLICITUDES EN BORRADOR"
            //this.ngxService.stop();
          }, error => {
            //this.ngxService.stop();
            alert.alertError("Ocurrió un error al intentar obtener las solicitudes!")
            window.history.back();
          });

        } else if (this.status == 'send') {
          this.requestService.getRequestInProcessByUser(this.page).subscribe((response) => {
            this.setPage(response);
            console.log(response)
            this.requests = response.content;
            this.title="SOLICITUDES EN PROCESO"
            //this.ngxService.stop();
          }, error => {
            //this.ngxService.stop();
            alert.alertError("Ocurrió un error al intentar obtener las solicitudes!")
            window.history.back();
          });
        }
      }
    })
  }

  setPage(response: any): void {
    this.page = {
      size: response.size,
      totalElements: response.totalElements,
      pageNumber: response.number,
      totalPages: response.totalPages
    }
  }

  find(event: any) {

  }

  delete(id: any) {
    Swal.fire({
      title: '¿Seguro que desea eliminar la solicitud?',
      text: 'No podrá deshacer esta acción',
      icon: 'warning',
      customClass: {
        title: 'text-primary-1 font-weight-bold',
        confirmButton: 'btn btn-primary btn-sm mr-2',
        cancelButton: 'btn btn-secondary btn-sm'
      },
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      backdrop: false,
      allowOutsideClick: false,
      input: null,
      inputAttributes: {
        type: 'hidden'
      },
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.ngxService.start()
        this.requestService.deleteRequest(id).subscribe((data) => {

          if (this.requests.length == 1 && this.page.pageNumber != 0) {
            this.getPage({ offset: this.page.pageNumber - 1 })
          } else {
            this.getPage({ offset: this.page.pageNumber })
          }
          alert.alertOk("Solicitud eliminada correctamente!")
          this.ngxService.stop()
        }, error => {
          this.ngxService.stop()
          alert.alertError("Ocurrió un error al eliminar la solicitud!")
        }
        );
      }
    });
  }


  ngOnInit(): void {
    this.getPage({ offset: 0 });
  }

}
