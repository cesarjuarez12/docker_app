import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import { Comment } from '../comment';
import { RequestService } from '../request.service';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent implements OnInit {
  title: string
  closeBtnName: string
  idRequest:number
  comment = new Comment();

  constructor(public bsModalRef: BsModalRef, private requestService: RequestService, private loadingService: NgxUiLoaderService, private router: Router) { }



  submit(){

    this.loadingService.start()

    this.requestService.addComment(this.comment, this.idRequest).subscribe(
      (data) =>{
        this.loadingService.stop()
        this.bsModalRef.hide()
        alert.alertOk("Comentario guardado correctamente")
      },
      (error) => {
        this.loadingService.stop()
        alert.alertError("Ocurrió un error")
      })
  }


  ngOnInit(): void {
  }

}
