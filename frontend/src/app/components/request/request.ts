import { DepartamentoInternoOm } from "src/app/departamentos/departamentos-internos-om/departamento-interno-om";
import { DepartamentoInterno } from "src/app/departamentos/departamentos-internos/departamento-interno";
import { Comment } from "./comment";
import { FieldOfRequest } from "./field-of-request";
import { Rejection } from "./rejection";
import { RequestStatus } from "./status-enum";

export class Request{
    id:number
    requestType: any
    projectName:string
    applicationSolution:string
    projectManager:string
    followUpContact:string
    departamentoRequerimiento: DepartamentoInterno
    departamentoSoporte: DepartamentoInternoOm
    priority:any
    draftStatus:boolean
    fields: FieldOfRequest[];
    saves: number;
    createdDate: Date;
    status: any;
    editsOnSend: number;
    numberOfServers: number;
    comments: Comment[]
    rejections: Rejection[]
    createdBy:string
    assignedEngineerName: string; 
    assignedEngineerUsername: string
    closingDateProcess:Date
    generatedEngineer: boolean
} 

    