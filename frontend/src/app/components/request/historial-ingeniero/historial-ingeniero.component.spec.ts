import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialIngenieroComponent } from './historial-ingeniero.component';

describe('HistorialIngenieroComponent', () => {
  let component: HistorialIngenieroComponent;
  let fixture: ComponentFixture<HistorialIngenieroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistorialIngenieroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialIngenieroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
