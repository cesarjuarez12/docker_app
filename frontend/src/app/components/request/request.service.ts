import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, map } from 'rxjs/operators';
import { Page } from "../users/users-list/page";
import { Comment } from "./comment";
import { FieldOfRequest } from "./field-of-request";
import { Rejection } from "./rejection";
import { Request } from "./request";
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { alert } from "src/app/common/alerts";
import * as dayjs from "dayjs";

@Injectable({
    providedIn: 'root'
})
export class RequestService {

  

    private urlEndPoint: string =  'api/request/';

    private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

    constructor(private http: HttpClient, private sanitizer: DomSanitizer) { }

    getRequest(id: number): Observable<any> {
        return this.http.get<any>(`${this.urlEndPoint}` + id, { headers: this.httpHeaders });
    }

    getRequestForEdit(id: number): Observable<any> {
        return this.http.get<any>(`${this.urlEndPoint}edit/` + id, { headers: this.httpHeaders });
    }

    getFile(idField: number) {
        return this.http.get(this.urlEndPoint + 'file/' + idField, { headers: this.httpHeaders, responseType: 'blob' });
    }

    getFileIE(idField: number, mime: any) {
        return this.http.get(this.urlEndPoint + "file/" + idField  , { responseType: "blob" }).pipe(map(result => {
          const blob = new Blob([result], { type: mime });
          return blob;
        }));
      }

    getRequestInProcessByUser(page: Page): Observable<any> {
        
        let params = new HttpParams()
            .set("size", page.size.toString())
            .set("page", page.pageNumber.toString())

        return this.http.get<any>(`${this.urlEndPoint}my-requests/in-process`, { params: params})
    }


    getRequestInDraftByUser(page: Page): Observable<any> {
        
        let params = new HttpParams()
            .set("size", page.size.toString())
            .set("page", page.pageNumber.toString())

        return this.http.get<any>(`${this.urlEndPoint}my-requests/in-draft`, { params: params})
    }



    getRequestUnassigned(page: Page): Observable<any> {
        let params = new HttpParams()
            .set("size", page.size.toString())
            .set("page", page.pageNumber.toString())
        return this.http.get<any>(`${this.urlEndPoint}unassigned-requests/`, { params: params})
    }

    getHistoryOfRequest(page: Page, idRequest: number): Observable<any> {        
        let params = new HttpParams()
            .set("size", page.size.toString())
            .set("page", page.pageNumber.toString())
        return this.http.get<any>(`${this.urlEndPoint}history-in-process/revision/` + idRequest, { params: params})
    }

    getVersionsOfRequest(idRequest: number): Observable<any>{
        return this.http.get<any>(`${this.urlEndPoint}revision/` + idRequest, { headers: this.httpHeaders })
    }

    getVersionOfRequest(idRequest:number, revNumber: number){
        return this.http.get<any>(`${this.urlEndPoint}revision/` + idRequest + '/' + revNumber, { headers: this.httpHeaders })
    }

    save(request: Request): Observable<any> {
        return this.http.post<any>(`${this.urlEndPoint}`, request, { headers: this.httpHeaders })
    }


    

    addComment(comment: Comment, idRequest: number): Observable<any> {
        return this.http.post<any>(`${this.urlEndPoint}comments/add-comment/${idRequest}`, comment, { headers: this.httpHeaders })
    }

    rejectARequest(rejection: Rejection, idRequest: number): Observable<any> {
        return this.http.post<any>(`${this.urlEndPoint}rejections/${idRequest}`, rejection, { headers: this.httpHeaders })
    }
    getComments(idRequest: number): Observable<any> {
        return this.http.get<any>(`${this.urlEndPoint}comments/${idRequest}`, { headers: this.httpHeaders })
    }

    updateDraft(request: Request): Observable<any> {
        return this.http.put<any>(`${this.urlEndPoint}`, request, { headers: this.httpHeaders })
    }

    deleteRequest(id: number): Observable<any> {
        return this.http.delete<any>(this.urlEndPoint + id, { headers: this.httpHeaders });
    }

    addFile(file: File, field: FieldOfRequest): Observable<any> {
        let formData = new FormData();
        formData.append("file", file);
        formData.append("idField", field.id.toString())
        const req = new HttpRequest('PUT', `${this.urlEndPoint}add-file`, formData, {
            reportProgress: true,
        });
        return this.http.request(req).pipe(
            catchError(e => {
                console.log(e);
                return throwError(e);
            })
        );
    }

    /***SERVICIO TEMPORAL PARA LOS CAMPOS DE UN TIPO DE SOLICITUD***/

    getRequestType(id: number): Observable<any> {
        return this.http.get<any>(`api/request-type/` + id, { headers: this.httpHeaders })
    }

    getRequestTypes(): Observable<any> {
        return this.http.get<any>(`api/request-type/`, { headers: this.httpHeaders })
    }


    getBaseFile(idField: number) {
        return this.http.get(this.urlEndPoint + "base-file/" + idField, { headers: this.httpHeaders, responseType: 'blob' });
    }
    
    getBaseFileIE(idField: number) {
        return this.http.get(this.urlEndPoint + "base-file/" + idField  , { responseType: "blob" }).pipe(map(result => {
          const blob = new Blob([result], { type: 'application/vnd.ms-excel' });
          //return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
          return blob;
        }));
      }

    /***VISTA ADMIN ****/
    getAllRequestInProcess(page: Page, draftStatus: boolean): Observable<any> {
        let params = new HttpParams()
            .set("size", page.size.toString())
            .set("page", page.pageNumber.toString())
            .set("draftStatus", draftStatus.toString());
            return this.http.get(this.urlEndPoint + 'all-in-process/' , { params: params, headers: this.httpHeaders});
    }

    /***LISTA INGENIEROS ****/
    getEngineer() {
        return this.http.get(`api/request/assign/engineers`, { headers: this.httpHeaders });
    }

    /** APROBAR**/
    sendAprobation(id_request: number, username_enginer: string){
        return this.http.put(`api/request/assign/` + id_request +`?engineerUsername=` + username_enginer , { headers: this.httpHeaders });
    }

    /***HISTORIAL SOLICITANTES ****/
    getMyCompletedRequests (page: Page): Observable<any> {
        let params = new HttpParams()
        .set("size", page.size.toString())
        .set("page", page.pageNumber.toString())
        return this.http.get(`api/request/my-completed-requests` , { params: params, headers: this.httpHeaders});
    }

    getRecordFilter (page: Page, draftStatus: boolean, status: string): Observable<any> {
        let params = new HttpParams()
        .set("size", page.size.toString())
        .set("page", page.pageNumber.toString())
        .set("draftStatus", draftStatus.toString());
        return this.http.get(`api/request/my-requests?status=` + status  , { params: params, headers: this.httpHeaders});
    }

    /***HISTORIAL COORDINADORES ****/
    getRecordCordinadores (page: Page, draftStatus: boolean): Observable<any> {
        let params = new HttpParams()
        .set("size", page.size.toString())
        .set("page", page.pageNumber.toString())
        .set("draftStatus", draftStatus.toString());
        return this.http.get(`api/request/for-coordinador` , { params: params, headers: this.httpHeaders});
    }

    getRecordCoordinadoresFilter (page: Page, draftStatus: boolean, filter: string): Observable<any> {
        let params = new HttpParams()
        .set("size", page.size.toString())
        .set("page", page.pageNumber.toString())
        .set("draftStatus", draftStatus.toString());
        return this.http.get(`api/request/for-coordinador` + filter , { params: params, headers: this.httpHeaders});
    }

    ///api/reports/requests-for-coordinador
    getDownloadCoordinadores(filter: string): Observable<SafeUrl> {
        return this.http.get(`api/reports/requests-for-coordinador` + filter  , { responseType: "blob" }).pipe(map(result => {
          const blob = new Blob([result], { type: 'application/vnd.ms-excel' });
          return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
        }));
      }
      
    getDownloadIECoordinadores(filter: string): Observable<SafeUrl> {
        return this.http.get(`api/reports/requests-for-coordinador` + filter  , { responseType: "blob" }).pipe(map(result => {
          const blob = new Blob([result], { type: 'application/vnd.ms-excel' });
          //return this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
          return blob;
        }));
      }

    /***HISTORIAL INGENIERO ****/
    getRecordIngeniero (page: Page, draftStatus: boolean): Observable<any> {
        let params = new HttpParams()
        .set("size", page.size.toString())
        .set("page", page.pageNumber.toString())
        .set("draftStatus", draftStatus.toString());
        return this.http.get(`api/request/for-engineer` , { params: params, headers: this.httpHeaders});
    }

    getRecordIngenieroFilter (page: Page, draftStatus: boolean, filter: string): Observable<any> {
        let params = new HttpParams()
        .set("size", page.size.toString())
        .set("page", page.pageNumber.toString())
        .set("draftStatus", draftStatus.toString());
        return this.http.get(`api/request/for-engineer` + filter  , { params: params, headers: this.httpHeaders});
    }

    /***HISTORIAL INGENIERO - COMPLETADAS ****/
    getRecordIngenieroComplete (page: Page): Observable<any> {
        let params = new HttpParams()
        .set("size", page.size.toString())
        .set("page", page.pageNumber.toString())
        return this.http.get(`api/request/all-complete` , { params: params, headers: this.httpHeaders});
        //return this.http.get(`${this.API_URL}api/request/all-complete` , { headers: this.httpHeaders});
    }

    getRecordIngenieroCompleteFilter (page: Page, filter: string): Observable<any> {
        let params = new HttpParams()
        .set("size", page.size.toString())
        .set("page", page.pageNumber.toString())
        //return this.http.get(`${this.API_URL}api/request/all-complete` , { params: params, headers: this.httpHeaders});
        return this.http.get(`api/request/all-complete?term=` + filter , {  params: params, headers: this.httpHeaders});
    }

    /***OPERACIONES QUE REALIZA EL INGENIERO*/
    saveRequestAdvanced(request: Request): Observable<any> {
        return this.http.put<any>(`${this.urlEndPoint}engineer/save`, request, { headers: this.httpHeaders })
    } 
   
    completeRequest(request: Request): Observable<any> {
        return this.http.put<any>(`${this.urlEndPoint}engineer/complete`, request, { headers: this.httpHeaders })
    } 

    getReport(idRequest: number) {
        return this.http.get('api/reports/pdf-final/' + idRequest, { headers: this.httpHeaders, responseType: 'blob' });
    }

    getReportComplete(request: Request) {
        this.getReport(request.id).subscribe((data) => {
          var url = window.URL.createObjectURL(data);
          let name = "INGENIERÍA-" + request.requestType.description + "-SOLICITUD" + request.id + "-" + request.projectName + "-" + dayjs().format('DDMMYY') + ".pdf";
          var anchor = document.createElement("a");
          anchor.download = name;
          anchor.href = url;
          anchor.click();
        }, error => {
          alert.alertError("Ocurrió un error al descargar la ingeniería")
        })
    }

    getRequestComplete(id: string): Observable<any> {
        return this.http.get<any>(`${this.urlEndPoint}engineer/${id}`, { headers: this.httpHeaders })
    }

    generateRequestForEngineerReference(request: Request): Observable<any> {
        return this.http.post<any>(`${this.urlEndPoint}engineer/`, request, { headers: this.httpHeaders })
    }
    
    /*USERS FROM AZURE*/
    getUsersFromAzure (term: string): Observable<any> {
        let params = new HttpParams()
        .set("term", term);
        return this.http.get(`api/users-azure/` , { params: params, headers: this.httpHeaders});
    }
}