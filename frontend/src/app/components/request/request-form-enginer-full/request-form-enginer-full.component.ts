import { HttpEventType } from '@angular/common/http';
import { Component, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { faExclamationCircle, faExclamationTriangle, faPlusCircle, faServer } from '@fortawesome/free-solid-svg-icons';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { concat, Observable, of, Subject } from 'rxjs';
import { catchError, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { alert } from 'src/app/common/alerts';
import { DepartamentoInternoOm } from 'src/app/departamentos/departamentos-internos-om/departamento-interno-om';
import { DepartamentoInternoOmService } from 'src/app/departamentos/departamentos-internos-om/departamento-interno-om.service';
import { DepartamentoInterno } from 'src/app/departamentos/departamentos-internos/departamento-interno';
import { DepartamentoInternoService } from 'src/app/departamentos/departamentos-internos/departamento-interno.service';
import Swal from 'sweetalert2';
import { FileTypeAllowed } from '../../config-file-types/file-type-allowed';
import { FileTypeAllowedService } from '../../config-file-types/file-type-allowed.service';
import { ConfiguracionGlobalService } from '../../configuracion-global/configuracion-global.service';
import { FormulariosService } from '../../formularios/formulario.service';
import { Page } from '../../users/users-list/page';
import { FieldOfRequest } from '../field-of-request';
import { FileService } from '../file.service';
import { Request } from '../request';
import { RequestService } from '../request.service';

const mime = require('mime');
@Component({
  selector: 'app-request-form-enginer-full',
  templateUrl: './request-form-enginer-full.component.html',
  styleUrls: ['./request-form-enginer-full.component.css']
})
export class RequestFormEnginerFullComponent implements OnInit {

  title = ""
  show: boolean = false;
  maxServers = 100;
  selectedServer = 1;
  newOption: string = "";
  validate: boolean = false;
  tabs: any[] = [];
  idS: number = null;
  readOnlyServers: boolean = false

  faPlus = faPlusCircle;
  faExclamationTriangle = faExclamationTriangle
  faExclamationCircle = faExclamationCircle
  faServer = faServer

  request: Request = new Request()
  requestType: any = {}
  requestTypes: any[] = []
  form: FormGroup;
  departamentosInternos: DepartamentoInterno[];
  departamentosInternosOm: DepartamentoInternoOm[];
  fields: FieldOfRequest[] = [];
  fileTypes: any = '';

  users: Observable<any[]>;
  usersLoading = false;
  usersInput$ = new Subject<string>();

  page = new Page();

  fieldId: number;


  //Validacion
  projectName: any;

  constructor(private dptoIntService: DepartamentoInternoService,
    private dptoIntOmService: DepartamentoInternoOmService,
    private requestService: RequestService,
    private fileTypesService: FileTypeAllowedService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private ngxService: NgxUiLoaderService,
    private localeService: BsLocaleService,
    private userService: TokenStorageService,
    private configuracionService: ConfiguracionGlobalService, public bsModalRef: BsModalRef, private formsService: FormulariosService, private fileService: FileService) {
    this.localeService.use('es')
    this.getMaxServers()
    this.page.pageNumber = 0;
    this.page.size = 10;
  }

  getPageOfRequestTypes(pageNumber: any) {
    this.page.pageNumber = pageNumber
    this.formsService.getFormularios(this.page)
      .subscribe((response) => {
        this.setPage(response);
        this.requestTypes = this.requestTypes.concat(response.content)
      }, error => alert.alertError("Ocurrió un error al tratar de obtener los tipos de solicitud"));
  }

  setPage(response: any): void {
    this.page = {
      size: response.size,
      totalElements: response.totalElements,
      pageNumber: response.number,
      totalPages: response.totalPages
    }
  }

  onScrollRequestTypesToEnd() {
    this.getPageOfRequestTypes(this.page.pageNumber + 1)
  }


  trackByFn(item: any) {
    return item.displayName;
  }

  loadUsersFromAzure() {
    this.users = concat(
      of([]),
      this.usersInput$.pipe(
        distinctUntilChanged(),
        tap(() => this.usersLoading = true),
        switchMap(term => this.requestService.getUsersFromAzure(term).pipe(
          catchError(() => of([])),
          tap(() => this.usersLoading = false)
        ))
      )
    );
  }

  createForm() {
    this.form = new FormGroup({
      projectName: new FormControl('', [Validators.required, Validators.pattern('^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.]{2,254})+')]),
      applicationSolution: new FormControl('', [Validators.required]),
      projectManager: new FormControl('', [Validators.required]),
      followUpContact: new FormControl('', [Validators.required]),
      departamentoRequerimiento: new FormControl('', [Validators.required]),
      departamentoSoporte: new FormControl('', [Validators.required]),
      priority: new FormControl('', [Validators.required]),
      numberOfServers: new FormControl('', [Validators.required, Validators.min(1), (control: AbstractControl) => Validators.max(this.maxServers)(control)])
    })
  }


  getDepartamentos(): void {
    this.dptoIntService.getDepartamentosInternos().subscribe((data) => {
      this.departamentosInternos = data as DepartamentoInterno[];
    })
    this.dptoIntOmService.getDepartamentosInternos().subscribe((data) => {
      this.departamentosInternosOm = data as DepartamentoInternoOm[];
    })
  }

  getMaxServers() {
    this.configuracionService.getConfiguracionGlobal().subscribe((conf) => this.maxServers = conf.maxServers)
  }

  getRequestType() {
    this.requestService.getRequestType(this.request.requestType.id).subscribe((data) => {
      this.title = "Formulario de " + data.description;
      this.request.requestType = data
      this.generateFields()
    }, error => {
      alert.alertError("Ocurrió un error al tratar de obtener esta solicitud")
    })
  }

  getFileTypesAllowed() {
    let fileTypes: FileTypeAllowed[];
    this.fileTypesService.getFileTypesAllowed().subscribe((data) => {
      fileTypes = data as FileTypeAllowed[];
      fileTypes.forEach(fileType => {
        this.fileTypes = fileType.fileExtension + ", " + this.fileTypes
      })
      this.fileTypes = this.fileTypes.substring(0, (this.fileTypes.length - 2))
    })
  }

  selectFile(event?: any, item?: FieldOfRequest) {

    let file: File = event.target.files[0];
    this.requestService.addFile(file, item).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        let progreso = Math.round((event.loaded / event.total) * 100);
      } else if (event.type === HttpEventType.Response) {
        alert.alertOk("El archivo se subió correctamente")
        item = event.body.field
        this.request.fields.find(field => field.id === item.id).refFileValue = item.refFileValue;
        this.request.fields.find(field => field.id === item.id).mimeType = item.mimeType;
        this.validateFields();
      }
    }, error => {
      alert.alertError("No se pudo subir este archivo, " + error.error.error)
    });
  }


  generateFields() {
    this.ngxService.start()
    this.assignValuesHeader()
    this.request.draftStatus = true
    this.requestService.generateRequestForEngineerReference(this.request).subscribe(data => {
      this.fields = data.requestHeader.fields as FieldOfRequest[];
      this.request = data.requestHeader;
      this.show = true;
      this.generateTabs()
      this.form.get('numberOfServers').disable({ onlySelf: true });
      this.ngxService.stop()
      alert.alertOk("Solicitud generada correctamente")
    }, error => {
      this.show = false
      this.ngxService.stop()
      alert.alertError("Ocurrió un error")
    }
    )
  }

  generateTabs() {
    for (let index = 1; index <= this.request.numberOfServers; index++) {
      const newTabIndex = index;
      this.tabs.push({
        id: index,
        title: `Servidor ${newTabIndex}`,
        content: ``,
        disabled: false,
        removable: false,
        active: false
      });
    }
    this.tabs.find(tab => tab.id == 1).active = true;
  }

  selectTab(tabz: any) {
    this.selectedServer = tabz.id
  }

  save() {
    this.requestService.saveRequestAdvanced(this.request).subscribe(
      data => {
        alert.alertOk("Guardado correctamente")
      },
      error => {
        alert.alertError("Han habido problemas para guardar la solicitud")
      }
    )
  }

  sendRequest() {
    this.ngxService.start();
    this.assignValuesHeader();
    if (this.request.status == 'BORRADOR' || this.request.status == 'RECHAZADA') {
      this.request.status = 'ENVIADA'
    }

    this.request.draftStatus = false;
    this.requestService.updateDraft(this.request).subscribe(data => {
      this.ngxService.stop()
      alert.alertOk("Se ha enviado la solicitud, redirigiendo al listado de solicitudes enviadas.")
      this.router.navigate(['/mis-solicitudes/send'])
    }, error => {
      alert.alertError("Ocurrió un error al intentar enviar la solicitud")
      console.log(error);
      this.ngxService.stop()
    })
  }

  preComplete() {
    this.validateFields()
    let stat = this.validateAll();
    if (stat == true) {
      Swal.fire({
        title: "Completar Solicitud",
        text: "¿Está seguro que desea aprobar esta solicitud?",
        icon: 'info',
        customClass: {
          title: 'text-primary-1 font-weight-bold',
          confirmButton: 'btn btn-primary btn-sm mr-2',
          cancelButton: 'btn btn-secondary btn-sm'
        },
        showCancelButton: true,
        confirmButtonText: 'Completar',
        cancelButtonText: 'Cancelar',
        backdrop: false,
        allowOutsideClick: false,
        input: null,
        inputAttributes: {
          type: 'hidden'
        },
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          this.completeRequest()
        }
      });
    } else {
      alert.alertError("Compruebe que la información que desea enviar es válida")
    }
  }

  completeRequest() {
    this.ngxService.start()
    this.requestService.completeRequest(this.request).subscribe(
      data => {
        this.ngxService.stop()
        this.request.status = 'COMPLETADA'
        this.getFinalReport()
        alert.alertOk("¡Solicitud completada correctamente!")
        this.bsModalRef.hide()
      },
      error => {
        this.ngxService.stop()
        alert.alertError("Ocurrió un error")
      }
    )
  }

  getFinalReport() {
    this.requestService.getReport(this.request.id).subscribe((data) => {
      var url = window.URL.createObjectURL(data);
      var anchor = document.createElement("a");
      anchor.download = "INGENIERÍA - " + this.request.requestType.description + " - SOLICITUD #" + this.request.id + " - " + this.request.projectName + ".pdf";
      anchor.href = url;
      anchor.click();
    }, error => {
      alert.alertError("Ocurrió un error al descargar la ingeniería")
    })
  }


  select(event: any) {
    console.log(event)
  }



  assignValuesHeader() {
    this.request.projectName = this.form.value['projectName']
    this.request.applicationSolution = this.form.value['applicationSolution']
    this.request.projectManager = this.form.value['projectManager']
    this.request.followUpContact = this.form.value['followUpContact']
    this.request.departamentoRequerimiento = this.form.value['departamentoRequerimiento']
    this.request.departamentoSoporte = this.form.value['departamentoSoporte']
    this.request.priority = this.form.value['priority']
    if(this.request.numberOfServers == null) {
      this.request.numberOfServers = this.form.value['numberOfServers']
    }
    
  }

  showFields() {
    this.show = true;
  }


  addOption(field: FieldOfRequest) {
    if (this.newOption != "") {
      let option = { id: "", title: this.newOption, selected: true }
      field.multiSelectValues.push(option);
    }
  }

  validateAll(): boolean {
    this.validateFields()
    this.validate = true
    let valid = false;
    let field = this.request.fields.find((field) => field.valid === false || field.valid == undefined);
    if (!field) {
      valid = true
    }
    return valid;
  }

  validateFields() {
    this.request.fields.forEach((field) => {

      field.valid = true

      /*  VALIDACIÓN DE CAMPOS DE IP */
      if (field.isIp != null && field.isIp == true && field.textValue != null && this.validateIp(field.textValue) == false) {
        field.valid = false;
      }

      /*  VALIDACIÓN DE CAMPOS DE HOSTNAME */
      if (field.isHostname != null && field.isHostname == true && field.textValue != null && this.validateHostname(field.textValue) == false) {
        field.valid = false;
      }

      /*  VALIDACIÓN DE CAMPOS NUMERICOS CON LIMITE MINIMO */
      if (field.min != null && field.integerValue != null && field.integerValue < field.min) {
        field.valid = false;
      }

      if (field.min != null && field.floatValue != null && field.floatValue < field.min) {
        field.valid = false;
      }

      /*  VALIDACIÓN DE CAMPOS NUMERICOS CON LIMITE MAXIMO */
      if (field.max != null && field.integerValue != null && field.integerValue > field.max) {
        field.valid = false;
      }

      if (field.max != null && field.floatValue != null && field.floatValue > field.max) {
        field.valid = false;
      }

      if(field.isRequestId == true && field.idRequestExist == null) {
        field.valid = false
      }

      /*  VALIDACIÓN DE CAMPOS CON VALORES REQUERIDOS */
      if (field.fieldType == 'TEXT' && field.required == true && ((field.textValue == null || field.textValue == ''))) {
        field.valid = false;
      } else if (field.fieldType == 'DINAMIC' && field.required == true && (field.dinamicValue == null || field.dinamicValue == '')) {
        field.valid = false;
      } else if (field.fieldType == 'INTNUMBER' && field.required == true && (field.integerValue == null || field.integerValue == undefined)) {
        field.valid = false;
      } else if (field.fieldType == 'FLOATNUMBER' && field.required == true && (field.floatValue == null || field.floatValue == undefined)) {
        field.valid = false;
      } else if (field.fieldType == 'SELECT' && field.required == true && field.multiSelect == false && (field.textValue == null || field.textValue == '')) {
        field.valid = false;
      } else if (field.fieldType == 'SELECT' && field.required == true && field.multiSelect == true && (field.multiSelectValues.find(msv => msv.selected == true) == null)) {
        field.valid = false;
      } else if (field.fieldType == 'DATE' && field.required == true && (field.dateValue == null || field.dateValue == undefined || field.dateValue == '')) {
        field.valid = false;
      } else if (field.fieldType == 'FILE' && field.required == true && (field.refFileValue == null || field.refFileValue == undefined || field.refFileValue == '')) {
        field.valid = false;
      }
      
      if(field.isRequestId == true && field.idRequestExist != null) {
        field.valid = true
      }
      

    })
  }

  validateIp(ip: string): boolean {
    let regex = /(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/g
    return regex.test(ip);
  }

  validateHostname(hostname: string): boolean {
    let regex = /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/g
    return regex.test(hostname);
  }

  loadForEdit() {
    if (this.idS != null) {
      this.ngxService.start()
      this.requestService.getRequestForEdit(this.idS).subscribe((data) => {
        this.request = data
        console.log(data)
        this.form.patchValue(this.request)
        this.form.get('numberOfServers').disable({ onlySelf: true });
        this.generateTabs();
        this.show = true
        this.ngxService.stop()
      }, error => {
        this.ngxService.stop()
        alert.alertError("¡Ocurrió un error al intentar obtener la solicitud!")
        this.router.navigateByUrl('/404')
      }
      )
    }
  }

  getFile(field: any) {
    this.fileService.getFile(field, this.request.id)
  }

  getBaseFile(field: any) {
    this.fileService.getBaseFile(field, this.request.id)
  }



  ngOnInit(): void {
    this.createForm()
    this.getDepartamentos()
    this.getFileTypesAllowed()
    this.loadUsersFromAzure()
    /*this.getRequestType()
    */
    this.loadForEdit()
    this.getPageOfRequestTypes(0)

    console.log(this.idS)

  }
}
