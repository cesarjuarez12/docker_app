import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestFormEnginerFullComponent } from './request-form-enginer-full.component';

describe('RequestFormEnginerFullComponent', () => {
  let component: RequestFormEnginerFullComponent;
  let fixture: ComponentFixture<RequestFormEnginerFullComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestFormEnginerFullComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestFormEnginerFullComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
