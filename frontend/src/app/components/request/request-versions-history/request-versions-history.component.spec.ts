import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestVersionsHistoryComponent } from './request-versions-history.component';

describe('RequestVersionsHistoryComponent', () => {
  let component: RequestVersionsHistoryComponent;
  let fixture: ComponentFixture<RequestVersionsHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestVersionsHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestVersionsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
