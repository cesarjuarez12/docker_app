import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import { FileService } from '../file.service';
import { Request } from '../request';
import { RequestService } from '../request.service';
import { Revision } from '../revision';

const mime = require('mime');
@Component({
  selector: 'app-request-versions-history',
  templateUrl: './request-versions-history.component.html',
  styleUrls: ['./request-versions-history.component.css']
})
export class RequestVersionsHistoryComponent implements OnInit {

  idReq = 0
  isCollapsed = true
  revisions: Revision[] = []
  modalRef: BsModalRef;
  request: Request = new Request()
  revisionNumber:number
  tabs: any[] = []
  selectedServer = 1
  loadingVersions = false

  constructor(private requestService: RequestService,
    private loadingService: NgxUiLoaderService,
    private activatedRoute: ActivatedRoute,
    private modalService: BsModalService, private fileService: FileService) { }

  ngOnInit(): void {
    this.getIdRequest();
  }



  getIdRequest() {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.idReq = id;
      }
    })
  }

  getVersions() {
    this.loadingVersions = true
    console.log(this.revisions)
    if (this.revisions.length == 0) {
      this.requestService.getVersionsOfRequest(this.idReq).subscribe(
        data => {
          let revisionsRejecteds = data.filter((rev: any, index: number) => rev.entity.status == "RECHAZADA" && index % 2 == 0)
          this.revisions = data.filter((rev: any) => rev.entity.status != "RECHAZADA")
          this.revisions = [...this.revisions, ...revisionsRejecteds].sort((a: any, b: any) => (a.revisionNumber > b.revisionNumber) ? 1 : -1) 
          let index = this.revisions.length - 2
          if(index > 0 && this.revisions[index] && this.revisions[index].entity.status == "ENVIADA") {
              this.revisions.splice(index, 1)
          }
          this.loadingVersions = false
        },
        error => {
          this.loadingVersions = false
          alert.alertError("No pudimos obtener las versiones de la solicitud")
        }
      )
    } else {
      this.loadingVersions = false
    }

  }

  openModal(template: any, request: Request, revisionNumber:number) {
    this.revisionNumber = revisionNumber
    this.request = request;
    this.request.fields.sort((a: any, b: any) => (a.id > b.id) ? 1 : -1)
    this.tabs = []
    this.generateTabs()
    template.show()
  }




  generateTabs() {
    for (let index = 1; index <= this.request.numberOfServers; index++) {
      const newTabIndex = index;
      this.tabs.push({
        id: index,
        title: `Servidor ${newTabIndex}`,
        content: ``,
        disabled: false,
        removable: false,
        active: false
      });
    }
    this.tabs.find(tab => tab.id == 1).active = true;
  }

  selectTab(tabz: any) {
    this.selectedServer = tabz.id
  }


  getFile(field: any) {
    this.fileService.getFile(field, this.request.id, this.revisionNumber)
  }

  getBaseFile(field: any) {
    this.fileService.getBaseFile(field, this.request.id)
  }

}
