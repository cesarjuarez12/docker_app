import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnassignedRequestsListComponent } from './unassigned-requests-list.component';

describe('UnassignedRequestsListComponent', () => {
  let component: UnassignedRequestsListComponent;
  let fixture: ComponentFixture<UnassignedRequestsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnassignedRequestsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnassignedRequestsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
