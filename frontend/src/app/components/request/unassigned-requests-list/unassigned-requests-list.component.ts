import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faEye, faPen, faTrash } from '@fortawesome/free-solid-svg-icons';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import { Page } from '../../users/users-list/page';
import { Request } from '../request';
import { RequestService } from '../request.service';

@Component({
  selector: 'app-unassigned-requests-list',
  templateUrl: './unassigned-requests-list.component.html',
  styleUrls: ['./unassigned-requests-list.component.css']
})
export class UnassignedRequestsListComponent implements OnInit {

  title = ""


  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  page = new Page();
  requests: Request[] = [];
  status: string = '';

  selected: Request[] = [];
  modalRef: BsModalRef
  selectedEnginner: any = {}
  engineerList: any[] = []

  faEye = faEye;
  faPen = faPen;
  faTrash = faTrash;

  scrollBarHorizontal = (window.innerWidth < 1200);

  constructor(private requestService: RequestService, private ngxService: NgxUiLoaderService, private activatedRoute: ActivatedRoute, private modalService: BsModalService) {
    this.page.pageNumber = 0;
    this.page.size = 10;
  }

  getPage(pageInfo?: any) {
    this.page.pageNumber = pageInfo.offset;

    //this.ngxService.start()
    this.requestService.getRequestUnassigned(this.page).subscribe((response) => {
      console.log(response);
      this.setPage(response);
      this.requests = response.content;
      this.title = "SOLICITUDES PENDIENTES DE ASIGNACIÓN"
      //this.ngxService.stop();
    }, error => {
      //this.ngxService.stop();
      alert.alertError("Ocurrió un error al intentar obtener las solicitudes!")
      window.history.back();
    });
  }


  setPage(response: any): void {
    this.page = {
      size: response.size,
      totalElements: response.totalElements,
      pageNumber: response.number,
      totalPages: response.totalPages
    }
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.requestService.getEngineer().subscribe((response: any = []) => {
      this.engineerList = response;
    })
  }

  assignMultiple() {
    this.modalRef.hide()
    let toassign = this.selected.length
    let assigneds: any[] = []
    let unassigneds: any[] = []
    this.selected.forEach((s, index) => {
      this.requestService.sendAprobation(s.id, this.selectedEnginner).subscribe((response) => {
        assigneds.push(s)
        this.selected.splice(index);
        this.getPage({ offset: 0 })
        this.evaluateToAlert(toassign, assigneds, unassigneds);
      }, error => {
        unassigneds.push(s)
        this.getPage({ offset: 0 })
        this.evaluateToAlert(toassign, assigneds, unassigneds);
        alert.alertError("La Solicitud #" + s.id + " no pudo ser asignada...")
      })
    })
  }

  evaluateToAlert(toassign: number, assigneds: any[], unassigneds: any[]) {

    if (toassign == (assigneds.length + unassigneds.length)) {

      if (toassign == assigneds.length) {
        alert.alertOk("Todas las solicitudes fueron asignadas correctamente")
      }

      if (toassign == unassigneds.length) {
        alert.alertError("Las solicitudes no pudieron ser asignadas...")
      }

      if (assigneds.length > 0 && unassigneds.length > 0) {

        alert.alertError("Algunas solicitudes no pudieron ser asignadas, compruébelo en el listado...")

      }

    }
  }

  onSelect({ selected }: any) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  assign() {
    console.log(this.selected)
    alert.alertOk("Asignar")
  }

  ngOnInit(): void {

    window.onresize = () => {
      this.scrollBarHorizontal = (window.innerWidth < 1200);
    };
    this.getPage({ offset: 0 });
  }

}
