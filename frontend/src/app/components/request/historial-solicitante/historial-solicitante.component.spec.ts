import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialSolicitanteComponent } from './historial-solicitante.component';

describe('HistorialSolicitanteComponent', () => {
  let component: HistorialSolicitanteComponent;
  let fixture: ComponentFixture<HistorialSolicitanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistorialSolicitanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialSolicitanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
