import { Component, OnInit } from '@angular/core';
import { faEdit, faEye, faPen, faSearch, faTimes, faTrash, faCheckSquare } from '@fortawesome/free-solid-svg-icons';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import { Page } from '../../users/users-list/page';
import { RequestService } from '../request.service';
import { ConfigVariables } from '../../../config/Variables';
import { alert } from 'src/app/common/alerts';

@Component({
  selector: 'app-historial-solicitante',
  templateUrl: './historial-solicitante.component.html',
  styleUrls: ['./historial-solicitante.component.css']
})
export class HistorialSolicitanteComponent implements OnInit {

  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  page = new Page();
  AllRequest: any[] = [];

  faCheckSquare = faCheckSquare;
  faEye = faEye;
  faPen = faPen;
  faTrash = faTrash;

  options: any = [
    {
      value: 'BORRADOR',
      description: 'BORRADOR'
    },
    {
      value: 'RECHAZADA',
      description: 'RECHAZADA'
    }
  ]

  optionSelect:string;

  scrollBarHorizontal = (window.innerWidth < 1200);

  constructor(
    private _requestService: RequestService, 
    private loadingService: NgxUiLoaderService) { 

    window.onresize = () => {
      this.scrollBarHorizontal = (window.innerWidth < 1200);
    };
    this.page.pageNumber = 0;
    this.page.size = 10;
  }


  ngOnInit(): void {
   this.setPage({ offset: 0 });
  }

  find(_event?: any) {}

  setPage(pageInfo?: any) {
    this.page.pageNumber = pageInfo.offset;
    this._requestService.getMyCompletedRequests(this.page).subscribe((response) => { 
      this.page.totalElements = response.totalElements;
      this.page.pageNumber = response.number;
      this.page.size = response.size;
      this.page.totalPages = response.totalPages;
      this.AllRequest = response.content;
    });
  }

  setPageFilter(pageInfo?: any, filtro?: string) {
    this.page.pageNumber = pageInfo.offset;
    this._requestService.getRecordFilter(this.page, false, filtro).subscribe((response) => { 
      this.page.totalElements = response.totalElements;
      this.page.pageNumber = response.number;
      this.page.size = response.size;
      this.page.totalPages = response.totalPages;
      this.AllRequest = response.content;
    });
  }

  filtrar(){
    console.log(this.optionSelect , this.options[0].value);

    let x = this.optionSelect;
    if(this.optionSelect == null)
    {
      this.ngOnInit();
    }  else{
      this.setPageFilter({ offset: 0 }, x);
    }
  }


}


