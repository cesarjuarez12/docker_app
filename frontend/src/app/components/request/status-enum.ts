export enum RequestStatus {
    BORRADOR,
    ENVIADA,
    ASIGNADA,
    COMPLETADA,
    RECHAZADA
}