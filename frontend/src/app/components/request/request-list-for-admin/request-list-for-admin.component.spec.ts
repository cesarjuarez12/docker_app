import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestListForAdminComponent } from './request-list-for-admin.component';

describe('RequestListForAdminComponent', () => {
  let component: RequestListForAdminComponent;
  let fixture: ComponentFixture<RequestListForAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestListForAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestListForAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
