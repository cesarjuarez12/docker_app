import { Component, OnInit } from '@angular/core';
import { faEdit, faEye, faPen, faSearch, faTimes, faTrash, faCheckSquare, faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import { Page } from '../../users/users-list/page';
import { RequestService } from '../request.service';
import { ConfigVariables } from '../../../config/Variables';
import { Request } from '../request';
import { alert } from 'src/app/common/alerts';
import { IEBrowserService } from 'src/app/services/ie-browser.service';

@Component({
  selector: 'app-request-list-for-admin',
  templateUrl: './request-list-for-admin.component.html',
  styleUrls: ['./request-list-for-admin.component.css']
})
export class RequestListForAdminComponent implements OnInit {

  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  page = new Page();
  AllRequest: any[] = [];

  faCheckSquare = faCheckSquare;
  faEye = faEye;
  faPen = faPen;
  faTrash = faTrash;
  faFilePdf = faFilePdf

  scrollBarHorizontal = (window.innerWidth < 1200);

  //IE Env
  IE: boolean = false;

  constructor(
    private BrowserDetection: IEBrowserService,
    private _requestService: RequestService, 
    private loadingService: NgxUiLoaderService) { 

    window.onresize = () => {
      this.scrollBarHorizontal = (window.innerWidth < 1200);
    };
    this.page.pageNumber = 0;
    this.page.size = 10;
  }


  async ngOnInit(): Promise<void> {
    this.IE = await this.BrowserDetection.detectionBrowser();
    this.setPage({ offset: 0 });
  }

  find(_event?: any) {}

  setPage(pageInfo?: any) {
    this.page.pageNumber = pageInfo.offset;
    this._requestService.getAllRequestInProcess(this.page, false).subscribe((response) => { 
      this.page.totalElements = response.totalElements;
      this.page.pageNumber = response.number;
      this.page.size = response.size;
      this.page.totalPages = response.totalPages;
      this.AllRequest = response.content;
    });
  }

  getReport(request: Request) {
    this._requestService.getReportComplete(request);
  }

  getReportIE(request: Request){
    this.BrowserDetection.getReportCompleteIE(request);
  }

  deleteRequest(ID: any){
    
    Swal.fire({
      title: '¿Seguro que desea eliminar la solicitud?',
      text: 'No podrá deshacer esta acción',
      icon: 'warning',
      customClass: {
        title: 'text-primary-1 font-weight-bold',
        confirmButton: 'btn btn-primary btn-sm mr-2',
        cancelButton: 'btn btn-secondary btn-sm'
      },
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      backdrop: false,
      allowOutsideClick: false,
      input: null,
      inputAttributes: {
        type: 'hidden'
      },
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        //this.loadingService.start();
        this._requestService.deleteRequest(ID).subscribe((response)=> {
          this.ngOnInit();
          //this.loadingService.stop();
          alert.alertOk("Solicitud eliminada")
        }, error => {
          //this.loadingService.stop();
          alert.alertError("Ocurrió un error al intentar eliminar la solicitud")
        });
      }
    });
  }

}


