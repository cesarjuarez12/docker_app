export class Revision {
    entity: any
    requiredRevisionInstant: any
    requiredRevisionNumber: number
    revisionInstant: any
    revisionNumber: number
    metadata: any
}