import { HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { faExclamationCircle, faExclamationTriangle, faPlusCircle, faServer } from '@fortawesome/free-solid-svg-icons';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { concat, Observable, of, Subject } from 'rxjs';
import { catchError, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { alert } from 'src/app/common/alerts';
import { DepartamentoInternoOm } from 'src/app/departamentos/departamentos-internos-om/departamento-interno-om';
import { DepartamentoInternoOmService } from 'src/app/departamentos/departamentos-internos-om/departamento-interno-om.service';
import { DepartamentoInterno } from 'src/app/departamentos/departamentos-internos/departamento-interno';
import { DepartamentoInternoService } from 'src/app/departamentos/departamentos-internos/departamento-interno.service';
import Swal from 'sweetalert2';
import { FileTypeAllowed } from '../../config-file-types/file-type-allowed';
import { FileTypeAllowedService } from '../../config-file-types/file-type-allowed.service';
import { FieldOfRequest } from '../field-of-request';
import { Request } from '../request';
import { RequestService } from '../request.service';
import * as dayjs from 'dayjs';
import { saveAs } from 'file-saver';
import { ConfiguracionGlobalService } from '../../configuracion-global/configuracion-global.service';
import { FileService } from '../file.service';
import { IEBrowserService } from 'src/app/services/ie-browser.service';
const mime = require('mime');

@Component({
  selector: 'app-request-form',
  templateUrl: './request-form.component.html'
})
export class RequestFormComponent implements OnInit {
  title = ""
  show: boolean = false;
  maxServers = 100;
  selectedServer = 1;
  newOption: string = "";
  validate: boolean = false;
  tabs: any[] = [];
  idS: number = null;
  readOnlyServers: boolean = false

  faPlus = faPlusCircle;
  faExclamationTriangle = faExclamationTriangle
  faExclamationCircle = faExclamationCircle
  faServer = faServer

  request: Request = new Request()
  requestType: any = {};
  form: FormGroup;
  departamentosInternos: DepartamentoInterno[];
  departamentosInternosOm: DepartamentoInternoOm[];
  fields: FieldOfRequest[] = [];
  fileTypes: any = '';

  users: Observable<any[]>;
  usersLoading = false;
  usersInput$ = new Subject<string>();

  //Validacion
  projectName: any;

  //Url
  fileUrlDowload: any;

  Hoy = new Date();
  HoyFormat = dayjs(this.Hoy).format('DD-MM-YYYY');// H:m:ss

  //IE
  IE: boolean = false;

  constructor(
    private BrowserDetection: IEBrowserService,
    private dptoIntService: DepartamentoInternoService,
    private dptoIntOmService: DepartamentoInternoOmService,
    private requestService: RequestService,
    private fileTypesService: FileTypeAllowedService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private ngxService: NgxUiLoaderService,
    private localeService: BsLocaleService,
    private userService: TokenStorageService,
    private configuracionService: ConfiguracionGlobalService, private fileService: FileService) {
    this.localeService.use('es');
    this.getMaxServers()
    this.createForm()
  }

  trackByFn(item: any) {
    return item.displayName;
  }

  loadUsersFromAzure() {
    this.users = concat(
      of([]),
      this.usersInput$.pipe(
        distinctUntilChanged(),
        tap(() => this.usersLoading = true),
        switchMap(term => this.requestService.getUsersFromAzure(term).pipe(
          catchError(() => of([])),
          tap(() => this.usersLoading = false)
        ))
      )
    );
  }

  createForm() {
    this.form = new FormGroup({
      //!/^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.]{2,254})+$
      projectName: new FormControl('', [Validators.required, Validators.pattern('^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.]{2,254})+')]),
      applicationSolution: new FormControl('', [Validators.required]),
      projectManager: new FormControl('', [Validators.required]),
      followUpContact: new FormControl('', [Validators.required]),
      departamentoRequerimiento: new FormControl('', [Validators.required]),
      departamentoSoporte: new FormControl('', [Validators.required]),
      priority: new FormControl('', [Validators.required]),
      numberOfServers: new FormControl('', [Validators.required, Validators.min(1), (control: AbstractControl) => Validators.max(this.maxServers)(control)])
    })
  }


  getDepartamentos(): void {
    this.dptoIntService.getDepartamentosInternos().subscribe((data) => {
      this.departamentosInternos = data as DepartamentoInterno[];
    })
    this.dptoIntOmService.getDepartamentosInternos().subscribe((data) => {
      this.departamentosInternosOm = data as DepartamentoInternoOm[];
    })
  }

  getMaxServers() {
    this.configuracionService.getConfiguracionGlobal().subscribe((conf) => this.maxServers = conf.maxServers)
  }

  getRequestType() {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.requestService.getRequestType(id).subscribe((data) => {
          this.title = "Formulario de " + data.description;
          this.requestType = data;
          this.ngxService.stop()
        }, error => {
          alert.alertError("Ocurrió un error al tratar de obtener esta solicitud")
          this.ngxService.stop()
          window.history.back();
        })
      } else {
        this.title = "EDITAR SOLICITUD DE PROYECTO "
      }
    })
  }

  getFileTypesAllowed() {
    let fileTypes: FileTypeAllowed[];
    this.fileTypesService.getFileTypesAllowed().subscribe((data) => {
      fileTypes = data as FileTypeAllowed[];
      fileTypes.forEach(fileType => {
        this.fileTypes = fileType.fileExtension + ", " + this.fileTypes
      })
      this.fileTypes = this.fileTypes.substring(0, (this.fileTypes.length - 2))
    })
  }

  selectFile(event?: any, item?: FieldOfRequest) {
    //this.ngxService.start()
    let file: File = event.target.files[0];
    this.requestService.addFile(file, item).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        let progreso = Math.round((event.loaded / event.total) * 100);
      } else if (event.type === HttpEventType.Response) {
        //this.ngxService.stop()
        alert.alertOk("El archivo se subió correctamente")
        item = event.body.field
        this.request.fields.find(field => field.id === item.id).refFileValue = item.refFileValue;
        this.request.fields.find(field => field.id === item.id).mimeType = item.mimeType;
        this.validateFields();
      }
    }, error => {
      //this.ngxService.stop()
      alert.alertError("No se pudo subir este archivo, " + error.error.error)
    });
  }


  saveAsDraft() {
    this.ngxService.start()
    this.assignValuesHeader();
    this.request.draftStatus = true;
    this.request.requestType = this.requestType;
    console.log(this.request)
    this.requestService.save(this.request).subscribe(data => {
      this.fields = data.requestHeader.fields as FieldOfRequest[];
      this.request = data.requestHeader;
      console.log(data)
      this.show = true;
      this.generateTabs()
      this.form.get('numberOfServers').disable({ onlySelf: true });
      this.ngxService.stop()
      alert.alertOk("Solicitud generada correctamente")
    }, error => {
      this.show = false
      this.ngxService.stop()
      alert.alertError("Ocurrió un error")
    }
    )
  }

  generateTabs() {
    for (let index = 1; index <= this.request.numberOfServers; index++) {
      const newTabIndex = index;
      this.tabs.push({
        id: index,
        title: `Servidor ${newTabIndex}`,
        content: ``,
        disabled: false,
        removable: false,
        active: false
      });
    }
    this.tabs.find(tab => tab.id == 1).active = true;
  }

  selectTab(tabz: any) {
    this.selectedServer = tabz.id
  }

  saveAsDraftWithFields() {
    //this.ngxService.start()
    this.assignValuesHeader();
    this.requestService.updateDraft(this.request).subscribe(data => {
      this.request = data.requestHeader;
      this.show = true;
      alert.alertOk("Solicitud guardada correctamente")
      //this.ngxService.stop()
    }, error => {
      //this.ngxService.stop()
      alert.alertError("Ocurrió un error al intentar guardar la solicitud")
    })
  }

  sendRequest() {
    this.ngxService.start();
    this.assignValuesHeader();
    if (this.request.status == 'BORRADOR' || this.request.status == 'RECHAZADA') {
      this.request.status = 'ENVIADA'
    }

    this.request.draftStatus = false;
    this.requestService.updateDraft(this.request).subscribe(data => {
      this.ngxService.stop()
      alert.alertOk("Se ha enviado la solicitud, redirigiendo al listado de solicitudes enviadas.")
      this.router.navigate(['/mis-solicitudes/send'])
    }, error => {
      alert.alertError("Ocurrió un error al intentar enviar la solicitud")
      console.log(error);
      this.ngxService.stop()
    })
  }



  preSend() {
    let stat = this.validateAll();
    if (stat == true) {
      Swal.fire({
        title: "Envío de Solicitud",
        text: "¿Está seguro que desea enviar esta solicitud?",
        icon: 'info',
        customClass: {
          title: 'text-primary-1 font-weight-bold',
          confirmButton: 'btn btn-primary btn-sm mr-2',
          cancelButton: 'btn btn-secondary btn-sm'
        },
        showCancelButton: true,
        confirmButtonText: 'Continuar',
        cancelButtonText: 'Cancelar',
        backdrop: false,
        allowOutsideClick: false,
        input: null,
        inputAttributes: {
          type: 'hidden'
        },
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          this.sendRequest()
        }
      });
    } else {
      alert.alertError("Compruebe que la información que desea enviar es válida")
    }
  }


  select(event: any) {
    console.log(event)
  }



  assignValuesHeader() {
    this.request.projectName = this.form.value['projectName']
    this.request.applicationSolution = this.form.value['applicationSolution']
    this.request.projectManager = this.form.value['projectManager']
    this.request.followUpContact = this.form.value['followUpContact']
    this.request.departamentoRequerimiento = this.form.value['departamentoRequerimiento']
    this.request.departamentoSoporte = this.form.value['departamentoSoporte']
    this.request.priority = this.form.value['priority']
    if(this.request.numberOfServers == null) {
      this.request.numberOfServers = this.form.value['numberOfServers']
    }
  }

  showFields() {
    this.show = true;
  }


  addOption(field: FieldOfRequest) {
    if (this.newOption != "") {
      let option = { id: "", title: this.newOption, selected: true }
      field.multiSelectValues.push(option);
    }
  }

  validateAll(): boolean {
    this.validateFields()
    this.validate = true
    let valid = false;
    let field = this.request.fields.find((field) => field.engineerField == false && (field.valid === false || field.valid == undefined));
    if (!field) {
      valid = true
    }
    return valid;
  }

  validateFields() {
    this.request.fields.forEach((field) => {

      field.valid = true

      /*  VALIDACIÓN DE CAMPOS DE IP */
      if (field.isIp != null && field.isIp == true && field.textValue != null && this.validateIp(field.textValue) == false) {
        field.valid = false;
      }

      /*  VALIDACIÓN DE CAMPOS DE HOSTNAME */
      if (field.isHostname != null && field.isHostname == true && field.textValue != null && this.validateHostname(field.textValue) == false) {
        field.valid = false;
      }

      /*  VALIDACIÓN DE CAMPOS NUMERICOS CON LIMITE MINIMO */
      if (field.min != null && field.integerValue != null && field.integerValue < field.min) {
        field.valid = false;
      }

      if (field.min != null && field.floatValue != null && field.floatValue < field.min) {
        field.valid = false;
      }

      /*  VALIDACIÓN DE CAMPOS NUMERICOS CON LIMITE MAXIMO */
      if (field.max != null && field.integerValue != null && field.integerValue > field.max) {
        field.valid = false;
      }

      if (field.max != null && field.floatValue != null && field.floatValue > field.max) {
        field.valid = false;
      }

      /*  VALIDACIÓN DE CAMPOS CON VALORES REQUERIDOS */
      if (field.fieldType == 'TEXT' && field.required == true && ((field.textValue == null || field.textValue == ''))) {
        field.valid = false;
      } else if (field.fieldType == 'DINAMIC' && field.required == true && (field.dinamicValue == null || field.dinamicValue == '')) {
        field.valid = false;
      } else if (field.fieldType == 'INTNUMBER' && field.required == true && (field.integerValue == null || field.integerValue == undefined)) {
        field.valid = false;
      } else if (field.fieldType == 'FLOATNUMBER' && field.required == true && (field.floatValue == null || field.floatValue == undefined)) {
        field.valid = false;
      } else if (field.fieldType == 'SELECT' && field.required == true && field.multiSelect == false && (field.textValue == null || field.textValue == '')) {
        field.valid = false;
      } else if (field.fieldType == 'SELECT' && field.required == true && field.multiSelect == true && (field.multiSelectValues.find(msv => msv.selected == true) == null)) {
        field.valid = false;
      } else if (field.fieldType == 'DATE' && field.required == true && (field.dateValue == null || field.dateValue == undefined || field.dateValue == '')) {
        field.valid = false;
      } else if (field.fieldType == 'FILE' && field.required == true && (field.refFileValue == null || field.refFileValue == undefined || field.refFileValue == '')) {
        field.valid = false;
      }

    })
  }

  validateIp(ip: string): boolean {
    let regex = /(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/g
    return regex.test(ip);
  }

  validateHostname(hostname: string): boolean {
    let regex = /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/g
    return regex.test(hostname);
  }

  loadForEdit() {
    this.ngxService.start()
    if (this.activatedRoute.snapshot.paramMap.get('idS') != '') {
      this.idS = Number.parseInt(this.activatedRoute.snapshot.paramMap.get('idS'));
      this.requestService.getRequestForEdit(this.idS).subscribe((data) => {
        this.request = data
        this.form.patchValue(this.request)
        this.form.get('numberOfServers').disable({ onlySelf: true });
        this.generateTabs();
        this.show = true
        let createdBy = this.userService.getUser();
        if (createdBy.email != this.request.createdBy) {
          alert.alertError("¡No tienes acceso a esta solicitud!")
          this.router.navigateByUrl('/404')
        }
        this.ngxService.stop()
      }, error => {
        this.ngxService.stop()
        alert.alertError("¡Ocurrió un error al intentar obtener la solicitud!")
        this.router.navigateByUrl('/404')
      }
      )
    }
  }

  getFile(field: any) {
    this.fileService.getFile(field, this.request.id)
  }
  
  getFileIE(field: any) {
    let mimetype = field.mimeTypeOfBaseFile;
    var ext = mime.getExtension(mimetype);
    this.requestService.getFileIE(field.id, mimetype).subscribe((data) => {
      var namefile = "ARCHIVO-" + field.title.toUpperCase() + "-" + this.request.id  + "." + ext;
      window.navigator.msSaveBlob(data,namefile);
    }, error => {
      alert.alertError("Ocurrió un error al intentar obtener el archivo");
    })
  }
  
  getBaseFile(field: any) {
    this.fileService.getBaseFile(field, this.request.id)
  }
  
  getBaseFileIE(field: any) {
    this.requestService.getBaseFileIE(field.id).subscribe((data) => {
      var namefile = "BASE-" + field.title.toUpperCase() + "-" + this.request.id  + "." + "xls";
      window.navigator.msSaveBlob(data,namefile);
    }, error => {
      alert.alertError("Ocurrió un error al intentar obtener el archivo")
    })
  }

  addItem(event: any, item: FieldOfRequest) {
    console.log(event);
    console.log(item)
  }

  exit() {

    Swal.fire({
      title: '¿Seguro que desea salir?',
      text: 'Perderá los cambios que no haya guardado...',
      icon: 'warning',
      customClass: {
        title: 'text-primary-1 font-weight-bold',
        confirmButton: 'btn btn-primary btn-sm mr-2',
        cancelButton: 'btn btn-secondary btn-sm'
      },
      showCancelButton: true,
      confirmButtonText: 'Salir',
      cancelButtonText: 'Cancelar',
      backdrop: false,
      allowOutsideClick: false,
      input: null,
      inputAttributes: {
        type: 'hidden'
      },
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        window.history.back();
      }
    });
  }

  validation_Name(x: any) {
    if (!/^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.]{2,254})+$/i.test(this.projectName)) {
      this.form.controls["projectName"].setValue(this.projectName.replace(/[^a-zA-ZÀ-ÿ \: \( \) \- \_ \#0-9,.]+/ig, ""));
    }
  }
  
  async ngOnInit(): Promise<void> {
    this.IE = await this.BrowserDetection.detectionBrowser();
    
    this.getDepartamentos()
    this.getFileTypesAllowed()
    this.getRequestType()
    this.loadForEdit()
    this.loadUsersFromAzure()
  }

}
