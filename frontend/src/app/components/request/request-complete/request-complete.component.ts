import { Component, OnInit } from '@angular/core';
import { faEdit, faEye, faPen, faSearch, faTimes, faTrash, faCheckSquare, faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import { Page } from '../../users/users-list/page';
import { RequestService } from '../request.service';
import { ConfigVariables } from '../../../config/Variables';
import { FormulariosService } from '../../formularios/formulario.service';
import * as dayjs from 'dayjs';
import { _ } from 'core-js';
import { Request } from '../request';
import { alert } from 'src/app/common/alerts';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { IEBrowserService } from 'src/app/services/ie-browser.service';
var localizedFormat = require('dayjs/plugin/localizedFormat');


@Component({
  selector: 'app-request-complete',
  templateUrl: './request-complete.component.html',
  styleUrls: ['./request-complete.component.css']
})
export class RequestCompleteComponent implements OnInit {

  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  page = new Page();
  AllRequest: any[] = [];

  faCheckSquare = faCheckSquare;
  faEye = faEye;
  faPen = faPen;
  faTrash = faTrash;
  faFilePdf = faFilePdf

  ip:any;
  ipValido: any;

  scrollBarHorizontal = (window.innerWidth < 1200);

  //IE
  IE: boolean

  constructor(
    private BrowserDetection: IEBrowserService,
    private _formService: FormulariosService,
    private _requestService: RequestService,
    private loadingService: NgxUiLoaderService,
    private localeService: BsLocaleService) {

      window.onresize = () => {
        this.scrollBarHorizontal = (window.innerWidth < 1200);
      };
      
      this.page.pageNumber = 0;
      this.page.size = 10;
     }

  async ngOnInit(): Promise<void> {
    this.IE = await this.BrowserDetection.detectionBrowser();
    this.setPage({ offset: 0 });
  }

  setPage(pageInfo?: any) {
    //this.loadingService.start();
    this.page.pageNumber = pageInfo.offset;
    this._requestService.getRecordIngenieroComplete(this.page).subscribe((response) => {
      this.page.totalElements = response.totalElements;
      this.page.pageNumber = response.number;
      this.page.size = response.size;
      this.page.totalPages = response.totalPages;
      this.AllRequest = response.content;
      //this.loadingService.stop();
    }, err => {
      alert.alertError(err.error.error);
    });
  }

  setPageFilter(pageInfo: any, filtro: string) {
    console.log(filtro)
    this.page.pageNumber = pageInfo.offset;
    this._requestService.getRecordIngenieroCompleteFilter(this.page, filtro).subscribe((response) => {
      this.page.totalElements = response.totalElements;
      this.page.pageNumber = response.number;
      this.page.size = response.size;
      this.page.totalPages = response.totalPages;
      this.AllRequest = response.content;
    }, err => {
      alert.alertError(err.error.error);
    });
  }


  filtrarIp(x: any) {

    if (!/^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.])+$/i.test(this.ip)){
      x.target.value = x.target.value.replace(/[^a-zA-ZÀ-ÿ \: /\ \( \) \- \_ \#0-9,.]+/ig,"");
      this.ip = '';
      this.setPage({ offset: 0 });
    
    } else if(this.ip == '' || this.ip == undefined || this.ip == null ){
      this.setPage({ offset: 0 });
    }

    this.setPageFilter({ offset: 0 }, this.ip);
  }

  getReport(request: Request) {
    this._requestService.getReportComplete(request);
  }

  getReportIE(request: Request){
    this.BrowserDetection.getReportCompleteIE(request);
  }

}
