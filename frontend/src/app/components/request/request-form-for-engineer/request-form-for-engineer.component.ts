import { HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { faExclamationCircle, faExclamationTriangle, faEye, faFilePdf, faPen, faPlusCircle, faServer } from '@fortawesome/free-solid-svg-icons';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { concat, Observable, of, Subject } from 'rxjs';
import { catchError, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { alert } from 'src/app/common/alerts';
import { DepartamentoInternoOmService } from 'src/app/departamentos/departamentos-internos-om/departamento-interno-om.service';
import { DepartamentoInternoService } from 'src/app/departamentos/departamentos-internos/departamento-interno.service';
import Swal from 'sweetalert2';
import { FileTypeAllowed } from '../../config-file-types/file-type-allowed';
import { FileTypeAllowedService } from '../../config-file-types/file-type-allowed.service';
import { FieldOfRequest } from '../field-of-request';
import { FileService } from '../file.service';
import { Request } from '../request';
import { RequestDetailsModalComponent } from '../request-details-modal/request-details-modal.component';
import { RequestFormEnginerFullComponent } from '../request-form-enginer-full/request-form-enginer-full.component';
import { RequestService } from '../request.service';

@Component({
  selector: 'app-request-form-for-engineer',
  templateUrl: './request-form-for-engineer.component.html',
  styleUrls: ['./request-form-for-engineer.component.css']
})
export class RequestFormForEngineerComponent implements OnInit {

  title = ""
  show: boolean = true;

  selectedServer = 1;

  faPlus = faPlusCircle

  faExclamationTriangle = faExclamationTriangle
  faExclamationCircle = faExclamationCircle
  faServer = faServer
  faEye = faEye
  faPen = faPen
  faFilePdf = faFilePdf

  request: Request = new Request()
  requestType: any = {};;
  fields: FieldOfRequest[] = [];
  fileTypes: any = '';

  validate: boolean = false;
  newOption: string = ""


  tabs: any[] = [];


  idS: number = null;

  requests = new Array<Request>()

  bsModalRef: BsModalRef;

  reqs: Observable<any>;
  reLoading = false;
  reqInput$ = new Subject<string>();


  constructor(private dptoIntService: DepartamentoInternoService,
    private dptoIntOmService: DepartamentoInternoOmService,
    private requestService: RequestService,
    private fileTypesService: FileTypeAllowedService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private ngxService: NgxUiLoaderService,
    private localeService: BsLocaleService, private modalService: BsModalService, private fileService: FileService) {
    this.localeService.use('es');
  }


  trackByFn(item: any) {
    return item;
  }

  loadReqs() {
    this.reqs = concat(
      of([]),
      this.reqInput$.pipe(
        distinctUntilChanged(),
        tap(() => this.reLoading = true),
        switchMap(term => this.requestService.getRequestComplete(term).pipe(
          catchError(() => of([])),
          tap(() => this.reLoading = false)
        ))
      )
    );

  }

  openRequestModal(idRequest: number) {
    const initialState = {
      title: 'SOLICITUD NO. ' + idRequest,
      idRequest: idRequest
    };
    this.bsModalRef = this.modalService.show(RequestDetailsModalComponent, { initialState, class: 'modal-lg' });
  }



  openRequestFormModal(field: FieldOfRequest) {
    const initialState = {
      title: 'NUEVA SOLICITUD',
      fieldId: field.id,
      idS: field.idRequestRefNew
    };
    this.bsModalRef = this.modalService.show(RequestFormEnginerFullComponent, { initialState, class: 'modal-lg' });

    this.modalService.onHide.subscribe((reason: string | any) => {
      console.log(this.bsModalRef.content)
      console.log(this.bsModalRef.content.fieldId)
      if (this.bsModalRef.content.request.id != null) {
        let refRequest = this.bsModalRef.content.request
        console.log(refRequest)
        this.request.fields.find(FieldOfRequest => FieldOfRequest.id == this.bsModalRef.content.fieldId).idRequestRefNew = refRequest.id
        this.request.fields.find(FieldOfRequest => FieldOfRequest.id == this.bsModalRef.content.fieldId).idRequestExist = null
        this.request.fields.find(FieldOfRequest => FieldOfRequest.id == this.bsModalRef.content.fieldId).statusOfRequestNew = refRequest.status
        this.save()
      }

    })
  }

  getReport(id: number) {
    this.requestService.getRequest(id).subscribe(data => this.requestService.getReportComplete(data));
  }

  getFileTypesAllowed() {
    let fileTypes: FileTypeAllowed[];
    this.fileTypesService.getFileTypesAllowed().subscribe((data) => {
      fileTypes = data as FileTypeAllowed[];
      fileTypes.forEach(fileType => {
        this.fileTypes = fileType.fileExtension + ", " + this.fileTypes
      })
      this.fileTypes = this.fileTypes.substring(0, (this.fileTypes.length - 2))
    })
  }

  selectFile(event?: any, item?: FieldOfRequest) {
    //this.ngxService.start()
    let file: File = event.target.files[0];
    this.requestService.addFile(file, item).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        let progreso = Math.round((event.loaded / event.total) * 100);
      } else if (event.type === HttpEventType.Response) {
        //this.ngxService.stop()
        alert.alertOk("Subida de archivo completa")
        item = event.body.field
        this.request.fields.find(field => field.id === item.id).refFileValue = item.refFileValue;
        this.request.fields.find(field => field.id === item.id).mimeType = item.mimeType;
        this.validateFields()
      }
    }, error => {
      //this.ngxService.stop()
      alert.alertError("No se pudo subir este archivo, " + error.error.error)
    });
  }


  generateTabs() {
    for (let index = 1; index <= this.request.numberOfServers; index++) {
      const newTabIndex = index;
      this.tabs.push({
        id: index,
        title: `Servidor ${newTabIndex}`,
        content: ``,
        disabled: false,
        removable: false,
        active: false
      });
    }
    this.tabs.find(tab => tab.id == 1).active = true;
  }

  selectTab(tabz: any) {
    this.selectedServer = tabz.id
  }


  validateFields() {
    this.request.fields.forEach((field) => {
      field.valid = true

      /*  VALIDACIÓN DE CAMPOS DE IP */
      if (field.isIp != null && field.isIp == true && field.textValue != null && this.validateIp(field.textValue) == false) {
        field.valid = false;
      }

      /*  VALIDACIÓN DE CAMPOS DE HOSTNAME */
      if (field.isHostname != null && field.isHostname == true && field.textValue != null && this.validateHostname(field.textValue) == false) {
        field.valid = false;
      }

      /*  VALIDACIÓN DE CAMPOS NUMERICOS CON LIMITE MINIMO */
      if (field.min != null && field.integerValue != null && field.integerValue < field.min) {
        field.valid = false;
      }

      if (field.min != null && field.floatValue != null && field.floatValue < field.min) {
        field.valid = false;
      }

      /*  VALIDACIÓN DE CAMPOS NUMERICOS CON LIMITE MAXIMO */
      if (field.max != null && field.integerValue != null && field.integerValue > field.max) {
        field.valid = false;
      }

      if (field.max != null && field.floatValue != null && field.floatValue > field.max) {
        field.valid = false;
      }

      /* VALIDACION DE CAMPOS CON REFERENCIA A SOLICITUD EXISTENTE*/
      if (field.isRequestId == true && field.booleanValue == true && field.idRequestExist == null) {
        field.valid = false
      }

      /* VALIDACION DE CAMPOS CON REFERENCIA A SOLICITUD NUEVA*/
      if (field.isRequestId == true && field.booleanValue == false && (field.idRequestRefNew == null || field.statusOfRequestNew == 'ASIGNADA')) {
        field.valid = false
      }


      /*  VALIDACIÓN DE CAMPOS CON VALORES REQUERIDOS */
      if (field.fieldType == 'TEXT' && field.required == true && (field.textValue == null || field.textValue == '')) {
        field.valid = false;
      } else if (field.fieldType == 'DINAMIC' && field.required == true && (field.dinamicValue == null || field.dinamicValue == '')) {
        field.valid = false;
      } else if (field.fieldType == 'INTNUMBER' && field.required == true && field.isRequestId == false && (field.integerValue == null || field.integerValue == undefined)) {
        field.valid = false;
      } else if (field.fieldType == 'FLOATNUMBER' && field.required == true && (field.floatValue == null || field.floatValue == undefined)) {
        field.valid = false;
      } else if (field.fieldType == 'SELECT' && field.required == true && field.multiSelect == false && (field.textValue == null || field.textValue == '')) {
        field.valid = false;
      } else if (field.fieldType == 'SELECT' && field.required == true && field.multiSelect == true && (field.multiSelectValues.find(msv => msv.selected == true) == null)) {
        field.valid = false;
      } else if (field.fieldType == 'DATE' && field.required == true && (field.dateValue == null || field.dateValue == undefined || field.dateValue == '')) {
        field.valid = false;
      } else if (field.fieldType == 'FILE' && field.required == true && (field.refFileValue == null || field.refFileValue == undefined || field.refFileValue == '')) {
        field.valid = false;
      }

    })

  }


  validateIp(ip: string): boolean {
    let regex = /(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/g
    return regex.test(ip);
  }

  validateHostname(hostname: string): boolean {
    let regex = /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/g
    return regex.test(hostname);
  }


  validateAll(): boolean {
    this.validateFields()
    this.validate = true
    let valid = false
    if (this.request != null) {
      let field = this.request.fields.find((field) => field.engineerField === true && (field.valid === false || field.valid == undefined));
      if (!field) {
        valid = true
      }
    }
    return valid;
  }


  save() {
    this.requestService.saveRequestAdvanced(this.request).subscribe(
      data => {
        alert.alertOk("Guardado correctamente")
      },
      error => {
        alert.alertError("Han habido problemas para guardar la solicitud")
      }
    )
  }


  preComplete() {
    this.validateFields()
    let stat = this.validateAll();
    if (stat == true) {
      Swal.fire({
        title: "Completar Solicitud",
        text: "¿Está seguro que desea aprobar esta solicitud?",
        icon: 'info',
        customClass: {
          title: 'text-primary-1 font-weight-bold',
          confirmButton: 'btn btn-primary btn-sm mr-2',
          cancelButton: 'btn btn-secondary btn-sm'
        },
        showCancelButton: true,
        confirmButtonText: 'Completar',
        cancelButtonText: 'Cancelar',
        backdrop: false,
        allowOutsideClick: false,
        input: null,
        inputAttributes: {
          type: 'hidden'
        },
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          this.completeRequest()
        }
      });
    } else {
      alert.alertError("Compruebe que la información que desea enviar es válida")
    }
  }

  completeRequest() {
    this.ngxService.start()
    this.requestService.completeRequest(this.request).subscribe(
      data => {
        this.ngxService.stop()
        alert.alertOk("¡Solicitud completada correctamente!")
        this.getFinalReport()
        this.router.navigate(["/historial-solicitudes-ing"])
        console.log(data)
      },
      error => {
        this.ngxService.stop()
        alert.alertError("Ocurrió un error")
        console.log(error)
      }
    )
  }


  getFinalReport() {
    this.requestService.getReport(this.request.id).subscribe((data) => {
      var url = window.URL.createObjectURL(data);
      var anchor = document.createElement("a");
      anchor.download = "INGENIERÍA - " + this.request.requestType.description + " - SOLICITUD #" + this.request.id + " - " + this.request.projectName + ".pdf";
      anchor.href = url;
      anchor.click();
    }, error => {
      alert.alertError("Ocurrió un error al descargar la ingeniería")
    })
  }


  loadForEdit() {
    if (this.activatedRoute.snapshot.paramMap.get('id')) {
      this.idS = Number.parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
      this.requestService.getRequestForEdit(this.idS).subscribe((data) => {
        this.request = data
        this.generateTabs();
        this.show = true
        this.ngxService.stop()
      }, error => {
        this.ngxService.stop()
      }
      )
    }
  }

  getFile(field: any) {
    this.fileService.getFile(field, this.request.id)
  }

  getBaseFile(field: any) {
    this.fileService.getBaseFile(field, this.request.id)
  }

  addItem(event: any, item: FieldOfRequest) {
    console.log(event);
    console.log(item)
  }

  addOption(field: FieldOfRequest) {
    if (this.newOption != "") {
      let option = { id: "", title: this.newOption, selected: true }
      field.multiSelectValues.push(option);
    }
  }

  return() {
    window.history.back()
  }


  ngOnInit(): void {
    this.ngxService.start();
    this.getFileTypesAllowed();
    this.loadForEdit();
    this.loadReqs()



  }

}
