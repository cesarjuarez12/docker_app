import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestFormForEngineerComponent } from './request-form-for-engineer.component';

describe('RequestFormForEngineerComponent', () => {
  let component: RequestFormForEngineerComponent;
  let fixture: ComponentFixture<RequestFormForEngineerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestFormForEngineerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestFormForEngineerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
