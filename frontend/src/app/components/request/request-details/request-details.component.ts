import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { RoleService } from 'src/app/auth/role.service';
import { alert } from 'src/app/common/alerts';
import { Comment } from '../comment';
import { CommentFormComponent } from '../comment-form/comment-form.component';
import { Request } from '../request';
import { RequestService } from '../request.service';
import { Router } from '@angular/router';
import { faServer, faPlane } from '@fortawesome/free-solid-svg-icons';
import { FieldOfRequest } from '../field-of-request';
import { IEBrowserService } from 'src/app/services/ie-browser.service';
const mime = require('mime');

declare var $: any;

@Component({
  selector: 'app-request-details',
  templateUrl: './request-details.component.html',
  styleUrls: ['./request-details.component.css']
})
export class RequestDetailsComponent implements OnInit {


  title = ''
  request: Request = new Request()
  comments: Comment[] = [];

  tabs: any[] = []

  selectedServer = 1;

  bsModalRef: BsModalRef

  //ModalDenegar
  MotivoDengacion: any;
  FormDenegacion: any = {
    justification: ''
  }

  //ModalAprobar
  engineerList: any = [];
  selectedEnginner = '';

  //IE Env
  IE: boolean;

  faPlane = faPlane;
  faServer = faServer;

  constructor(
    private BrowserDetection: IEBrowserService,
    private requestService: RequestService,
    private activatedRoute: ActivatedRoute,
    private loadingService: NgxUiLoaderService,
    public roleService: RoleService,
    private modalService: BsModalService,
    private router: Router) {
  }

  openModalComment() {
    const initialState = {
      title: 'AGREGAR COMENTARIO',
      idRequest: this.request.id
    };
    this.bsModalRef = this.modalService.show(CommentFormComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Cancelar';
    this.modalService.onHide.subscribe((reason: string | any) => {
      this.getCom()
    })
  }


  getRequest() {
    this.loadingService.start()
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      this.requestService.getRequest(id).subscribe(
        (data) => {
          this.request = data
          this.title = "SOLICITUD PARA "
          this.generateTabs()
          this.getCom()
          this.loadingService.stop()
        },
        (error) => {
          window.history.back()
          this.loadingService.stop()
          alert.alertError("Ocurrió un error!")
        })
    })
  }


  getCom() {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      this.requestService.getComments(id).subscribe(
        (data) => {
          this.comments = data
          this.loadingService.stop();
        },
        (error) => {
          alert.alertError("Ocurrió un error al tratar de obtener los comentarios!")
        })
    })
  }

  generateTabs() {
    for (let index = 1; index <= this.request.numberOfServers; index++) {
      const newTabIndex = index;
      this.tabs.push({
        id: index,
        title: `Servidor ${newTabIndex}`,
        content: ``,
        disabled: false,
        removable: false,
        active: false
      });
    }
    this.tabs.find(tab => tab.id == 1).active = true;
  }

  selectTab(tabz: any) {
    this.selectedServer = tabz.id
  }


  getFile(field: FieldOfRequest) {
    //this.loadingService.start()

    this.requestService.getFile(field.id).subscribe((data) => {
      var url = window.URL.createObjectURL(data);
      var ext = mime.getExtension(data.type);
      var anchor = document.createElement("a");
      var title = field.title
      if (field.isRequiredConditional == true && field.secondaryTitle != null) {
        title = field.secondaryTitle
      }
      anchor.download = "ARCHIVO-" + field.title.toUpperCase() + "-" + this.request.id + "." + ext;
      anchor.href = url;
      anchor.click();
      //this.loadingService.stop()
    }, error => {
      alert.alertError("Ocurrió un error al intentar obtener el archivo")
      //this.loadingService.stop()
    })

  }

  getFileIE(field: any) {
    let mimetype = field.mimeTypeOfBaseFile;
    var ext = mime.getExtension(mimetype);
    this.requestService.getFileIE(field.id, mimetype).subscribe((data) => {
      var namefile = "ARCHIVO-" + field.title.toUpperCase() + "-" + this.request.id + "." + "xls";
      window.navigator.msSaveBlob(data, namefile);
    }, error => {
      alert.alertError("Ocurrió un error al intentar obtener el archivo");
    })
  }

  getBaseFile(field: any) {
    this.requestService.getBaseFile(field.id).subscribe((data) => {
      var url = window.URL.createObjectURL(data);
      var ext = mime.getExtension(data.type);
      var anchor = document.createElement("a");
      var title = field.title
      if (field.isRequiredConditional == true && field.secondaryTitle != null) {
        title = field.secondaryTitle
      }
      anchor.download = "BASE-" + field.title.toUpperCase() + "-" + this.request.id + "." + ext;
      anchor.href = url;
      anchor.click();
    }, error => {
      alert.alertError("Ocurrió un error al intentar obtener el archivo")
      //this.loadingService.stop()
    })
  }


  getBaseFileIE(field: any) {
    this.requestService.getBaseFileIE(field.id).subscribe((data) => {
      //this.fileUrlDowload = response;
      var namefile = "BASE-" + field.title.toUpperCase() + "-" + this.request.id + "." + "xls";
      window.navigator.msSaveBlob(data, namefile);
    }, error => {
      alert.alertError("Ocurrió un error al intentar obtener el archivo")
      //this.loadingService.stop()
    })
  }

  async ngOnInit(): Promise<void> {
    this.IE = await this.BrowserDetection.detectionBrowser();
    this.getRequest();
  }

  openModaldenegar() {
    /* this.departamentoInternoService.getDepartamentoInterno(id).subscribe((departamentoInterno) => this.departamentoInterno = departamentoInterno)
       */
    $('#ShowModalCenter').modal('show');
  }

  sendDenegar() {
    this.loadingService.start();
    $('#ShowModalCenter').modal('hide');
    this.FormDenegacion.justification = this.MotivoDengacion;
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.requestService.rejectARequest(this.FormDenegacion, parseInt(id)).subscribe((response) => {
      this.router.navigate(['/solicitudes/para-asignacion']);
      this.loadingService.stop();
      alert.alertOk("¡Solicitud rechazada correctamente!")
    }, error => {
      this.loadingService.stop();
      alert.alertError("Ocurrió un error, no se pudo completar el rechazo")
    });
  }


  openModalAprobar() {
    $('#ShowModalAprobar').modal('show');
    this.requestService.getEngineer().subscribe((response: any = []) => {
      this.engineerList = response;
    })
  }

  sendAprobar() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.loadingService.start();
    $('#ShowModalAprobar').modal('hide');

    this.requestService.sendAprobation(parseInt(id), this.selectedEnginner).subscribe((response) => {
      this.router.navigate(['/solicitudes/para-asignacion']);

      this.loadingService.stop();
      alert.alertOk("¡Solicitud Asignada!")
    }, error => {
      this.loadingService.stop();
      alert.alertError("Ocurrió un error al intentar realizar la asignación")
    })
  }

  return() {
    window.history.back()
  }

}
