import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { alert } from "src/app/common/alerts";
import { FieldOfRequest } from "./field-of-request";

const mime = require('mime')

@Injectable({
    providedIn: 'root'
})
export class FileService {


    private urlEndPoint: string = 'api/request/';

    private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

    constructor(private http: HttpClient) { }

    getFile(field: FieldOfRequest, idRequest: number, revisionNumber?:number) {

        let url = this.urlEndPoint + 'file/' + field.id
        
        if(revisionNumber) {
            url = url + '?revisionNumber=' + revisionNumber
        }

        this.http.get(url, { headers: this.httpHeaders, responseType: 'blob' }).subscribe(data => {
            var url = window.URL.createObjectURL(data);
            var ext = mime.getExtension(data.type);
            var anchor = document.createElement("a");
            var title = field.title
            if(field.isRequiredConditional == true && field.secondaryTitle != null) {
                title = field.secondaryTitle
            }
            anchor.download = title.toUpperCase() + "-" + idRequest  + "." + ext;
            anchor.href = url;
            anchor.click();
        }, error => alert.alertError("No se encontró el archivo solicitado"));
    }
    
    getBaseFile(field: FieldOfRequest, idRequest: number) {
        return this.http.get(this.urlEndPoint + "base-file/" + field.id, { headers: this.httpHeaders, responseType: 'blob' }).subscribe(data => {
            var url = window.URL.createObjectURL(data);
            var ext = mime.getExtension(data.type);
            var anchor = document.createElement("a");
            var title = field.title
            if(field.isRequiredConditional == true && field.secondaryTitle != null) {
                title = field.secondaryTitle
            } 
            anchor.download = "BASE-" + title.toUpperCase() + "-" + idRequest  + "." + ext;
            anchor.href = url;
            anchor.click();
        });
    }
}