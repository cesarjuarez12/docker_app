import { AfterViewInit, ChangeDetectorRef, Component, Directive, Host, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import { FormulariosService } from '../../formularios/formulario.service';
import { Page } from '../../users/users-list/page';

@Component({
  selector: 'app-request-options-list',
  templateUrl: './request-options-list.component.html',
  styleUrls: ['./request-options-list.component.css']
})
export class RequestOptionsListComponent implements OnInit, AfterViewInit {

  ColumnMode = ColumnMode;
  SelectionType = SelectionType;

  title = "ELIJA UN FORMULARIO PARA COMPLETAR"

  requestTypes: any[] = [];
  page = new Page();

  
  @ViewChild(DatatableComponent) table: DatatableComponent;
  

  constructor(private formulariosService: FormulariosService, private ngxLoadingService: NgxUiLoaderService, private activatedRoute: ActivatedRoute, private changeDetectorRef: ChangeDetectorRef) {
    this.page.pageNumber = 0;
    this.page.size = 10;
  }


  ngAfterViewInit(): void {
    this.table.columnMode = ColumnMode.force;
  }

  find(_event?: any) {
    const val = _event.target.value.toLowerCase();
    this.page.pageNumber = 0
    this.getPage({offset: 0}, val)
  }

  
  getPage(pageInfo?: any, term?: string) {
    this.page.pageNumber = pageInfo.offset;
    this.formulariosService.getFormularios(this.page, term)
      .subscribe((response) => {
        this.setPage(response);
        this.requestTypes = response.content;
      }, error => {
        alert.alertError("Ocurrió un error " + error.error)
      });
  }

  setPage(response: any): void {
    this.page = {
      size: response.size,
      totalElements: response.totalElements,
      pageNumber: response.number,
      totalPages: response.totalPages
    }
  }

  ngOnInit(): void {
    this.getPage({ offset : 0 })
  }

}
