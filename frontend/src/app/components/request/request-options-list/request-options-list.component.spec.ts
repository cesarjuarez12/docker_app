import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestOptionsListComponent } from './request-options-list.component';

describe('RequestOptionsListComponent', () => {
  let component: RequestOptionsListComponent;
  let fixture: ComponentFixture<RequestOptionsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestOptionsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestOptionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
