export class FieldOfRequest {
    dateValue: any
    fieldType: any
    refFileValue: any
    floatValue: number
    dinamicValue: any
    booleanValue: boolean
    idRequestRefNew: number
    statusOfRequestNew: string
    idRequestExist: number
    multiSelectValues?: any[]
    id: number
    integerValue: number
    textValue: string
    valid: boolean
    mimeType: string
    serverNumber: number

    /*METADATOS DEL CAMPO*/
    title:string;
    secondaryTitle:string
    //options: any[];
    required: boolean
    engineerField:boolean
    multiSelect:boolean
    permittedAdd:boolean
    defaultValue:any
    refBaseFile:any
    mimeTypeOfBaseFile:string
    min: number
    max: number
    isIp: boolean
    isHostname: boolean
    isRequestId: boolean
    isRequiredConditional: boolean
}