import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormulariosService } from '../formulario.service';
import { DomSanitizer } from '@angular/platform-browser';
import { UpdateFile } from '../fileUpdate';
import { FileTypeAllowedService } from '../../config-file-types/file-type-allowed.service';
import { faPen } from '@fortawesome/free-solid-svg-icons';
import { ViewChild, ElementRef } from '@angular/core';
import { alert } from 'src/app/common/alerts';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { OrderQuestionsComponent } from '../order-questions/order-questions.component';
import { FieldOfRequest } from '../../request/field-of-request';
import { IEBrowserService } from 'src/app/services/ie-browser.service';
import { base64StringToBlob, canvasToBlob } from 'blob-util'
const mime = require('mime');

declare var $: any;


@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})

export class FormularioComponent implements OnInit {


  faPen = faPen;

  @ViewChild('inputFileBase') inputFileBase: ElementRef;

  Header: string = '';
  Titulo: string = '';
  New_formulario: boolean;
  loading = false

  //checkMultiSelect
  MultiSelect: boolean = false;

  //Fields
  form: any = [];

  New_Form: any;
  fields: any = [];

  pregunta = '';
  required: boolean = false;
  engineer: boolean = false;
  permittedAdd: boolean = false;
  ip: boolean = false;
  hostname: boolean = false;
  reference: boolean = false;

  //Select
  Type = 'TEXT';
  isSelect: boolean;
  Opcion = '';

  types = [
    { id: 'TEXT', name: 'TEXT' },
    { id: 'INTNUMBER', name: 'INTNUMBER' },
    { id: 'FLOATNUMBER', name: 'FLOATNUMBER' },
    { id: 'DINAMIC', name: 'DINAMIC' },
    { id: 'FILE', name: 'FILE' },
    { id: 'DATE', name: 'DATE' },
    { id: 'SELECT', name: 'SELECT' }
  ];

  options: { id: string; title: string; }[] = [];

  NoFileds: any = [];

  //Validacion
  validacion_titulo: boolean = true;
  validacion_titulo_invalido: boolean = true;
  validacion_pregunta: boolean = true;
  validacion_options: boolean = true;
  validacion_options_input: boolean = true;
  validacion_FileType: boolean = true;
  validacion_FilePath: boolean = true;

  //Edicion
  paramID = 0;
  UpdateMode: boolean = false;

  //Archivos
  Files: any[] = [];
  isFileType: boolean = false;
  isFile: boolean = false;
  isFiles: any[] = [];

  FilePath: any = '';

  fileUrl: any;
  idFile: number;
  uploadForm: FormGroup;

  fileToUpload: File = null;
  request: any;

  //Chechk Subida
  UploadFile: boolean = false;
  HabilitarSubida: boolean = false;

  //TiposPermitidos
  TiposPermitidos: any[] = [];
  CadenaTiposPermitidos: string = '';

  num: number;
  isText: boolean = false;
  isNumber: boolean = false;
  isIntNumber: boolean = false;
  isDinamic:boolean = false;
  
  //Min-Max
  min: number;
  max: number;

  //Reference
  secondaryTitle: any = '';
  engineerDisabled: boolean = false;
  requiredConditional: boolean = false;

  defaultValue: string = null;

  bsModalRef: BsModalRef;
  //IE
  IE: boolean = false;

  constructor(
    private BrowserDetection: IEBrowserService,
    private _RequesType: FormulariosService, 
    private _TypeAllowerService: FileTypeAllowedService,
    private router: Router, 
    private rutaActiva: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private loadingService: NgxUiLoaderService, 
    private modalService: BsModalService) { }

    async ngOnInit(): Promise<void> {
      this.IE = await this.BrowserDetection.detectionBrowser();
      this.cargarExtensiones();
      this.uploadForm = this.formBuilder.group({
        profile: ['']
      });

    if (this.rutaActiva.snapshot.params['id']) {
      this.loadingService.start();
      this.paramID = this.rutaActiva.snapshot.params['id'];
      this.UpdateMode = true;
      this.Header = 'EDICIÓN DE FORMULARIOS';
      this.cargarformulario();
      this.loadingService.stop();
    } else {
      this.UpdateMode = false;
      this.Header = 'CREACIÓN DE FORMULARIOS';
      this.nuevoFormulario();
    }
  }

  openModal() {
    const initialState = {
      title: 'ORDENAR CAMPOS',
      fields: this.fields
    };
    this.bsModalRef = this.modalService.show(OrderQuestionsComponent, { initialState, class: 'modal-lg' });
    this.modalService.onHide.subscribe((reason: string | any) => {
      this.fields = this.bsModalRef.content.fields
    })
  }


  nuevoFormulario() {
    this.New_formulario = true;
    this.fields.push({
      title: 'Pregunta sin titulo',
      fieldType: 'TEXT',
      required: true
    });
    this.NoFileds.push({ pregunta: false });
    this.isFiles.push({ pregunta: false });
  }

  cancelarFormulario() {
    this.New_formulario = false;
    this.validacion_pregunta = true;
    this.validacion_options = true;
    this.form = [];
    this.fields = [];
    this.options = [];
    this.Titulo = '';
  }

  removeOptionSelect(num: any) {
    this.options.splice(num, 1);
  }

  onChangeSelect() {
    if (this.Type == 'SELECT') {
      this.reference = false;
      this.isIntNumber = false;
      this.isSelect = true;
      this.isText = false;
      this.isNumber = false;
      this.isFileType = false;
      this.requiredConditional = false;
    } else if (this.Type == 'TEXT') {
      this.reference = false;
      this.isIntNumber = false;
      this.isText = true;
      this.isNumber = false;
      this.options = [];
      this.isSelect = false;
      this.isFileType = false;
      this.requiredConditional = false;
    } else if (this.Type == 'INTNUMBER') {
      this.reference = false;
      this.isIntNumber = true;
      this.isNumber = true;
      this.isText = false;
      this.options = [];
      this.isSelect = false;
      this.isFileType = false;
      this.requiredConditional = false;
    } else if (this.Type == 'FLOATNUMBER') {
      this.reference = false;
      this.isIntNumber = false;
      this.isNumber = true;
      this.isText = false;
      this.options = [];
      this.isSelect = false;
      this.isFileType = false;
      this.requiredConditional = false;
    } else if(this.Type == 'FILE'){
      this.reference = false;
      this.isIntNumber = false;
      this.isNumber = false;
      this.options = [];
      this.isSelect = false;
      this.isText = false;
      this.isFileType = true;
      this.requiredConditional = false;
    } else if(this.Type == 'DATE') {
      this.reference = false;
      this.isIntNumber = false;
      this.isNumber = false;
      this.options = [];
      this.isSelect = false;
      this.isText = false;
      this.isFileType = false;
      this.requiredConditional = false;
    } else if(this.Type == 'DINAMIC') {
      this.reference = false;
      this.isDinamic = true;
      this.isIntNumber = false;
      this.isNumber = false;
      this.options = [];
      this.isSelect = false;
      this.isText = false;
      this.isFileType = false;
      this.requiredConditional = false;
    }
  }

  addOptionSelect() {
    if (this.Opcion == null || this.Opcion.length == 0 || (/^\s+$/).test(this.Opcion)) {
      this.validacion_options_input = false;
    }
    else {
      this.options.push({
        id: '',
        title: this.Opcion,
      });
      this.Opcion = '';
      this.validacion_options_input = true;
      this.validacion_options = true;
    }
  }


  addField() {
    if (this.pregunta == null || this.pregunta.length == 0 || (/^\s+$/).test(this.pregunta)) {
      this.validacion_pregunta = false;
    } else if (this.Type == 'SELECT' && this.options.length <= 1) {
      this.validacion_options = false;
    }
    else {
      if (this.Type == 'SELECT') {
        this.fields.push({
          title: this.pregunta,
          fieldType: this.Type,
          options: this.options,
          required: true
        });
        this.NoFileds.push({ pregunta: false });
      } else {
        this.fields.push({
          title: this.pregunta,
          fieldType: this.Type,
          required: true,
        });
        this.NoFileds.push({ pregunta: false });
      }
      this.validacion_pregunta = true;
      this.validacion_options = true;
      this.pregunta = '';
      this.Type = 'TEXT';
      this.options = [];
      this.isSelect = false;
    }
  }


  removeField(num: any) {
    this.fields.splice(num, 1);
    var count = 0;
    for (let index = 0; index < this.fields.length; index++) {
      if (!this.fields[index].id && this.fields[index].fieldType == 'FILE') {
        count = count + 1;
      }
    }
    if (count < 1) {
      this.HabilitarSubida = false; 
    }

  }

  show(num: any) {
    this.MultiSelect = false;
    this.permittedAdd = false;
    this.engineer = false;
    this.pregunta = '';
    this.Type = '';
    this.min = null;
    this.max = null;
    this.required = false;
    this.isSelect = false;
    this.fields[num]['options'];
    this.options = [];
    this.isNumber = false;
    this.isText = false;
    this.isIntNumber = false;
    this.secondaryTitle = '';
    this.reference = false;
    this.isFileType = false;
    this.requiredConditional = false;
    this.secondaryTitle = '';
    this.isDinamic = false;

    if (this.NoFileds[num].pregunta == true) {
      for (let index = 0; index < this.NoFileds.length; index++) {
        this.NoFileds[index].pregunta = false;
      }
    }
    else {
      for (let index = 0; index < this.NoFileds.length; index++) {
        this.NoFileds[index].pregunta = false;
      }
      this.NoFileds[num].pregunta = true;
      this.MultiSelect = this.fields[num]['multiSelect'];
      this.permittedAdd = this.fields[num]['permittedAdd'];
      this.engineer = this.fields[num]['engineerField'];
      this.pregunta = this.fields[num]['title'];
      this.Type = this.fields[num]['fieldType'];
      this.required = this.fields[num]['required'];
      this.ip = this.fields[num]['isIp'];
      this.hostname = this.fields[num]['isHostname'];
      if (this.Type == 'FILE') {
        this.isFileType = true;
        this.requiredConditional = this.fields[num].isRequiredConditional;
        this.secondaryTitle = this.fields[num].secondaryTitle;
      }
      if (this.Type == 'SELECT') { this.isSelect = true; this.options = this.fields[num]['options']; }
      else if (this.Type == 'TEXT') {
        this.isText = true;
      } else if (this.Type == 'INTNUMBER') {
        this.isIntNumber = true;
        this.reference = this.fields[num]['isRequestId'];
        this.secondaryTitle = this.fields[num]['secondaryTitle'];
        this.min = this.fields[num]['min'];
        this.max = this.fields[num]['max'];
        this.isNumber = true;
      } else if (this.Type == 'FLOATNUMBER') {
        this.min = this.fields[num]['min'];
        this.max = this.fields[num]['max'];
        this.isNumber = true;
      } else if (this.Type == 'DINAMIC') {
        this.defaultValue = this.fields[num]['defaultValue'];
        this.isDinamic = true;
      } else { this.isSelect = false; this.options = []; }
    }
  }

  agregarPregunta() {
    this.fields.push({
      title: 'Pregunta sin titulo',
      fieldType: 'TEXT',
      required: true,
      engineerField: false,
      defaultValue: null,
    });
    this.defaultValue = null
    this.NoFileds.push({ pregunta: false });
    this.isFiles.push({ pregunta: false });
    this.abrir_ultimo();
  }

  agregarArrayBoleano() {
    this.NoFileds.push({ pregunta: true });
    for (let index = 0; index < this.NoFileds.length; index++) {
      console.log(this.NoFileds[index].pregunta = false);
    }
    this.NoFileds[0].pregunta = false;
  }

  modificarForm(num: any) {
    if (!this.fields[num].id && this.Type == 'FILE') { this.HabilitarSubida = true }
    if (this.pregunta == null || this.pregunta.length == 0 || (/^\s+$/).test(this.pregunta)) {
      this.validacion_pregunta = false;
    } else if (this.Type == 'SELECT' && this.options.length <= 1) {
      this.validacion_options = false;
    }
    else {
      if (this.Type == 'TEXT') {
          this.fields[num].id = this.fields[num].id,
          this.fields[num].title = this.pregunta,
          this.fields[num].fieldType = this.Type,
          this.fields[num].required = this.required,
          this.fields[num].engineerField = this.engineer,
          this.fields[num].isIp = this.ip,
          this.fields[num].isHostname = this.hostname
      } else if (this.Type == 'SELECT') {
          this.fields[num].id = this.fields[num].id,
          this.fields[num].title = this.pregunta,
          this.fields[num].fieldType = this.Type,
          this.fields[num].required = this.required,
          this.fields[num].engineerField = this.engineer
          this.fields[num].options = this.options,
          this.fields[num].multiSelect = this.MultiSelect,
          this.fields[num].permittedAdd = this.permittedAdd,
          this.fields[num].isIp = false,
          this.fields[num].isHostname = false,
          this.min = null,
          this.max = null,
          this.NoFileds.push({ pregunta: false });
      } else if (this.Type == 'FLOATNUMBER') {
          this.fields[num].id = this.fields[num].id,
          this.fields[num].title = this.pregunta,
          this.fields[num].fieldType = this.Type,
          this.fields[num].required = this.required,
          this.fields[num].engineerField = this.engineer,
          this.fields[num].min = this.min;
          this.fields[num].max = this.max;
          this.fields[num].isIp = false,
          this.fields[num].isHostname = false
      } else if (this.Type == 'INTNUMBER') {
          this.fields[num].id = this.fields[num].id,
          this.fields[num].title = this.pregunta,
          this.fields[num].fieldType = this.Type,
          this.fields[num].required = this.required,
          this.fields[num].engineerField = this.engineer,
          this.fields[num].min = this.min;
          this.fields[num].max = this.max;
        if (this.reference == true) {
          this.fields[num].isRequestId = true;
          this.fields[num].secondaryTitle = this.secondaryTitle;
        }
        this.fields[num].isIp = false,
        this.fields[num].isHostname = false
      } else if (this.Type == 'DINAMIC') {
        this.fields[num].id = this.fields[num].id,
        this.fields[num].title = this.pregunta,
        this.fields[num].fieldType = this.Type,
        this.fields[num].required = this.required,
        this.fields[num].engineerField = this.engineer
        this.fields[num].defaultValue = this.defaultValue
    } else {
          this.fields[num].id = this.fields[num].id,
          this.fields[num].title = this.pregunta,
          this.fields[num].fieldType = this.Type,
          this.fields[num].required = this.required,
          this.fields[num].engineerField = this.engineer,
          this.fields[num].isIp = false,
          this.fields[num].isHostname = false,
          this.min = null,
          this.max = null
        if (this.Type == 'FILE') {
          if(this.requiredConditional){
            this.fields[num].isRequiredConditional = this.requiredConditional;
            this.fields[num].secondaryTitle = this.secondaryTitle;
          }
        } else {
          this.fields[num].mimeTypeOfBaseFile = null;
          this.fields[num].baseFile = null;
        }
        this.NoFileds.push({ pregunta: false });
      }
      this.isFileType = false;
      this.requiredConditional =false;
      this.reference = false;
      this.secondaryTitle = '';
      this.isIntNumber = false;
      this.engineer = false;
      this.MultiSelect = false;
      this.validacion_pregunta = true;
      this.validacion_options = true;
      this.pregunta = '';
      this.Type = 'TEXT';
      this.options = [];
      this.isSelect = false;
      this.required = false;
      this.ip = false;
      this.hostname = false;
      this.min = null;
      this.max = null;
      this.defaultValue = null;
      this.isDinamic = false
      this.show(num);
    }
  }


  enviar() {
    if (!/^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.])+$/i.test(this.Titulo)) {
      this.Titulo.replace(/[^a-zA-ZÀ-ÿ \: \( \) \- \_ \#0-9,.]+/ig, "");
      this.validacion_titulo_invalido = false;
    } else if (this.Titulo == null || this.Titulo.length == 0 || (/^\s+$/).test(this.Titulo)) {
      this.validacion_titulo = false;
    } else {
      this.fields.forEach((field:any, index:number) => field.ord = index )
      this.form = [];
      this.form.push({
        description: this.Titulo,
        fields: this.fields
      });
      this.loading = true;
      this._RequesType.CreateRequestType(this.form[0])
        .subscribe(res => {
          this.loading = false;
          alert.alertOk("Formulario guardado correctamente")
          if (this.UploadFile == true) {
            console.log("Respuesta", res.requestType.id);
            console.log('/formulario/edit-form/')
            this.router.navigate(['/formulario/edit-form/', res.requestType.id])
          } else {
            this.router.navigate(['/formularios'])
          }
        }, err => {
          this.loading = false;
          let separador = ":";
          var arregloDeSubCadenas = err.error.error.split(separador);
          if (arregloDeSubCadenas[2] === ' ORA-00001') {
            alert.alertError("Este título ya esta en uso!");
          }
          else if (arregloDeSubCadenas[2] === ' ORA-12899') {
            alert.alertError("Título demasiado grande - Máximo 255 caracteres!");
          } else {
            alert.alertError("Ha ocurrido algún error!");
          }
        });
      this.validacion_titulo = true;
    }
  }

  cargarformulario() {
    this.loadingService.start();
    this._RequesType.getById(this.paramID).subscribe((response) => {
      for (let index = 0; index < response.fields.length; index++) {
        this.NoFileds.push({ pregunta: false });
        this.isFiles.push({ pregunta: false });
      }
      this.Titulo = response.description;
      this.fields = response.fields;
      this.fields.forEach((field:any, index:number) => field.ord = index )
    });
    this.loadingService.stop();
  }

  actualizar_Enviar() {
    if (!/^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.])+$/i.test(this.Titulo)) {
      this.Titulo.replace(/[^a-zA-ZÀ-ÿ \: \( \) \- \_ \#0-9,.]+/ig, "");
      this.validacion_titulo_invalido = false;
    } else if (this.Titulo == null || this.Titulo.length == 0 || (/^\s+$/).test(this.Titulo)) {
      this.validacion_titulo = false;
    } else {
      this.fields.forEach((field:any, index:number) => field.ord = index )
      this.form = [];
      this.form.push({
        id: this.paramID,
        description: this.Titulo,
        fields: this.fields
      });
      this.loading = true;
      this._RequesType.UpdateRequestType(this.form[0])
        .subscribe(res => {
          alert.alertOk("Formulario actualizado correctamente!");
          this.loading = false;
          if (this.UploadFile == true) {
            window.location.reload(false);
          } else {
            this.router.navigate(['/formularios'])
          }
        }, err => {
          this.loading = false;
          console.error(err);
        });
      this.validacion_titulo = true;
    }
  }

  handleFileInput(event?: any) {
    console.log(event.target.files[0]);
    this.fileToUpload = event.target.files[0];
  }

  SubirArchivosNoId() {
    this._RequesType.UploadFilesNoId(this.fileToUpload)
      .subscribe((res: any = []) => {

        var x = res.field;
        this.fields[this.num].baseFile = res.file.baseFile;
        this.fields[this.num].mimeTypeOfBaseFile = res.file.mimeTypeOfBaseFile;
        this.FilePath = '';
        this.isFiles[this.num].pregunta = false;
        alert.alertOk("Archivo cargado correctamente!");

      }, err => {
        console.error(err);
      });
  }
  
  onChangeFile(event?: any, num?: any, id?: any) {
    this.num = id;
    console.log("Ha cambiado", id);
    this.FilePath = event.target.files[0]['name'];
    var extension = this.FilePath.split(".").slice(-1);
    if (this.TiposPermitidos.indexOf(extension[0]) === -1) {
      alert.alertError("Extensión no Admitida");
      this.inputFileBase.nativeElement.value = '';
    } else {
      alert.alertOk("Archivo cargado correctamente!");
      this.idFile = num;
      this.fileToUpload = event.target.files[0];
      this.SubirArchivosNoId();
    }
  }

  getFileBlob(field: any) {
    //console.log("Base - Field: ",field['baseFile']);
    //console.log("Base - Mime: ",field['mimeTypeOfBaseFile']);
    const blob = base64StringToBlob(field['baseFile'], field['mimeTypeOfBaseFile']);

    var url = window.URL.createObjectURL(blob);
    var ext = mime.getExtension(field['mimeTypeOfBaseFile']);
    var anchor = document.createElement("a");

    var title = field.title;
    if(field.isRequiredConditional == true && field.secondaryTitle != null) {
      title = field.secondaryTitle
    }
    anchor.download = "BASE-" + field.title.toUpperCase() + "-" + field.id  + "." + ext;
    anchor.href = url;
    anchor.click();
  }
  
  getFileBlobIE(field: any) {
    var ext = mime.getExtension(field['mimeTypeOfBaseFile']);
    //console.log("Base - Field: ",field['baseFile']);
    //console.log("Base - Mime: ",field['mimeTypeOfBaseFile']);
    const blob = base64StringToBlob(field['baseFile'], field['mimeTypeOfBaseFile']);

    var namefile = "ARCHIVO-" + field.title.toUpperCase() + "." + ext;
    window.navigator.msSaveBlob(blob,namefile);
  }

  getFile(field: FieldOfRequest) {
    this._RequesType.getFile(field.id).subscribe((data) => {
      var url = window.URL.createObjectURL(data);
      var ext = mime.getExtension(data.type);
      var anchor = document.createElement("a");
      var title = field.title
      if(field.isRequiredConditional == true && field.secondaryTitle != null) {
          title = field.secondaryTitle
      }
      anchor.download = "BASE-" + field.title.toUpperCase() + "-" + field.id  + "." + ext;
      anchor.href = url;
      anchor.click();
    }, error => {
      alert.alertError("Ocurrió un error al intentar obtener el archivo")
    })

  }
  
  getFileIE(field: any) {
    //mimeTypeOfBaseFile
    let mimetype = field.mimeTypeOfBaseFile;
    var ext = mime.getExtension(mimetype);
    this._RequesType.getFileIE(field.id, mimetype).subscribe((data) => {
      console.log(data);
      var namefile = "ARCHIVO-" + field.title.toUpperCase() + "." + ext;
      window.navigator.msSaveBlob(data,namefile);
    }, error => {
      alert.alertError("Ocurrió un error al intentar obtener el archivo");
    })
  }

  validarFile(event?: any, num?: number) {
    console.log('Ha pasado', this.isFile);
  }

  checkValue(event: any, num: any, info: any) {
    console.log(event);
    if (event == 'Oculto') {
      this.FilePath = '';
    } else {
      for (let index = 0; index < this.isFiles.length; index++) {
        this.isFiles[index].pregunta = false;
      }
      this.isFiles[num].pregunta = true;
    }

  }


  checkIp(event: any) {
    if (event == 'Requerido') {
      this.ip = true;
      this.hostname = false;
    } else {
      this.ip = false;
    }
  }

  checkRequiredCondition(event: any) {
    if (event == 'Requerido') {
      this.requiredConditional = true;
      this.secondaryTitle = '';
    } else {
      this.requiredConditional = false;
      this.secondaryTitle = '';
    }
  }

  checkHostname(event: any) {
    if (event == 'Requerido') {
      this.hostname = true;
      this.ip = false;
    } else {
      this.hostname = false;
    }
  }

  checkReference(event: any) {
    if (event == 'Requerido') {
      this.engineerDisabled = true;
      this.reference = true;
      this.engineer = true;
    } else {
      this.engineerDisabled = false;
      this.reference = false;
      this.engineer = false;
    }
  }

  checkRequired(event: any) {
    if (event == 'Requerido') {
      this.required = true;
    } else {
      this.required = false;
    }
  }

  checkMultiSelect(event: any) {
    if (event == 'Requerido') {
      this.MultiSelect = true;
      this.permittedAdd = false;
    } else {
      this.MultiSelect = false;
      this.permittedAdd = false;
    }
  }

  checkIsEngineer(event: any) {
    if (this.reference == true) {
      this.engineer = true;
      console.log('Ingenieria es true');
    } else {
      if (event == 'Requerido') {
        this.engineer = true;
      } else {
        this.engineer = false;
      }
    }
  }

  checkAddMore(event: any) {
    if (event == 'Requerido') {
      this.permittedAdd = true;
    } else {
      this.permittedAdd = false;
    }
  }

  checkUploadFile(event: any) {
    if (event == 'Requerido') {
      this.UploadFile = true;
    } else {
      this.UploadFile = false;
    }
  }

  cargarExtensiones() {
    this._TypeAllowerService.getFileTypesAllowed().subscribe(res => {
      for (let i = 0; i < res.length; i++) {
        this.TiposPermitidos.push(res[i].fileExtension.substring(1));
        this.CadenaTiposPermitidos = this.CadenaTiposPermitidos + '.' + res[i].fileExtension.substring(1) + ',';
      }
    this.CadenaTiposPermitidos = this.CadenaTiposPermitidos.substring(0, this.CadenaTiposPermitidos.length - 1);
    }, err => {
      console.error(err);
    });
  }


  openUpLoadFile(id: any) {
    this.UploadFile = true;
    console.log(id);
    $('#UploadFileModalCenter').modal('show');
  }

  validacion_titulo_event(x: any) {
    if (!/^([a-zA-ZÀ-ÿ \: \= \( \) \- \_ \#0-9,.])\2(\d{4})$/i.test(x.target.value)) {
      x.target.value = x.target.value.replace(/[^a-zA-ZÀ-ÿ \: \( \) \- \_ \#0-9,.]+/ig, "");
    }
  }


  abrir_ultimo() {
    let ultima_pregunta = this.fields.length - 1;
    this.show(ultima_pregunta);
  }

  keyupNumberMin(x: any, model: any) {
    if (!/^([0-9]{2,254})\2(\d{4})$/i.test(this.min.toString())) {
      this.min = x.target.value.replace(/[^0-9]+/ig, "");
    }
  }

  keyupNumberMax(x: any, model: any) {
    if (this.max == undefined || this.max == null) { }
    else if (!/^([0-9]{2,254})\2(\d{4})$/i.test(this.max.toString())) {
      this.max = x.target.value.replace(/[^0-9]+/ig, "");
    }
  }

}
