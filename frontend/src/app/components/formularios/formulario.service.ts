import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Page } from '../users/users-list/page';

@Injectable()
export class FormulariosService {

  private urlEndPoint: string = 'api/request-type/';

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) { }

  CreateRequestType(Datos: any[]): Observable<any> {
    return this.http.post<any>(this.urlEndPoint, Datos, { headers: this.httpHeaders });
  }

  getFormularios(page: Page, searchTerm?: string): Observable<any> {
    if(searchTerm == undefined) {
      searchTerm = ""
    }
    return this.http.get(this.urlEndPoint + '?page=' + `${page.pageNumber}&size=${page.size}&term=${searchTerm}`, { headers: this.httpHeaders });
  }

  getById(id: number): Observable<any> {
    return this.http.get(this.urlEndPoint + `${id}`, { headers: this.httpHeaders });
  }

  deleteById(id: number): Observable<any> {
    return this.http.delete(this.urlEndPoint + `${id}`, { headers: this.httpHeaders });
  }

  UpdateRequestType(Datos: any[]): Observable<any> { //id: number, 
    return this.http.put(this.urlEndPoint, Datos, { headers: this.httpHeaders });
  }

  /*UploadFiles(archivo: File, id: any): Observable<HttpEvent<{}>> {

    let formData = new FormData();
    formData.append("file", archivo);
    formData.append("idField", id);

    const req = new HttpRequest('PUT', `${this.urlEndPoint}add-base-file/`, formData, {
      reportProgress: true, 
    });

    return this.http.request(req).pipe(
      catchError(e => {
        return throwError(e);
      })
    );

  }*/

  UploadFiles(archivo: File, id: any):Observable<any> {
    const formData = new FormData();    
    formData.append("file", archivo);
    formData.append("idField", id);  
    return this.http.put(`${this.urlEndPoint}add-base-file/`, formData) 
  } 

  UploadFilesNoId(archivo: File):Observable<any> {
    const formData = new FormData();    
    formData.append("file", archivo);
    return this.http.post(`${this.urlEndPoint}add-base-file/`, formData) 
  } 

  getFile(idField: number) {
    return this.http.get(this.urlEndPoint + 'file/' + idField, { headers: this.httpHeaders, responseType: 'blob' });
  }

  getFileIE(idField: number, mime: any) {
    return this.http.get(this.urlEndPoint + "file/" + idField  ,  { headers: this.httpHeaders, responseType: 'blob' }).pipe(map(result => {
      const blob = new Blob([result], { type: mime });
      return blob;
    }));
  }

  getAllRequesType(): Observable<any>{
    return this.http.get(this.urlEndPoint, { headers: this.httpHeaders });
  }

  getSolcitantes(){
    return this.http.get('api/user/solicitantes', { headers: this.httpHeaders });
  }

  getIngenieros(){
    return this.http.get('api/request/assign/engineers', { headers: this.httpHeaders });
  }

}