import { Component, OnInit } from '@angular/core';
import { faEdit, faEye, faPen, faSearch, faTimes, faTrash } from '@fortawesome/free-solid-svg-icons';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import Swal from 'sweetalert2';
import { Page } from '../../users/users-list/page';
import { FormulariosService } from '../formulario.service';
import { Formulario } from '../formularios';
import { ConfigVariables } from '../../../config/Variables';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';



@Component({
  selector: 'app-formulario-list',
  templateUrl: './formulario-list.component.html',
  styleUrls: ['./formulario-list.component.css']
})
export class FormularioListComponent implements OnInit {


  
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  page = new Page();
  forms = new Array<Formulario>();

  faEye = faEye;
  faPen = faPen;
  faTrash = faTrash;
  
  constructor(private formulariosService: FormulariosService, private loadingService: NgxUiLoaderService) { 
    this.page.pageNumber = 0;
    this.page.size = ConfigVariables.No_Paginate;
  }

  ngOnInit(): void {
    this.getPage({ offset: 0 });
  }

  find(_event?: any) {
    const val = _event.target.value.toLowerCase();
    this.page.pageNumber = 0
    this.getPage({offset: 0}, val)
  }

  
  getPage(pageInfo?: any, term?: string) {
    this.page.pageNumber = pageInfo.offset;
    this.formulariosService.getFormularios(this.page, term)
      .subscribe((response) => {
        this.setPage(response);
        this.forms = response.content;
      }, error => {
        alert.alertError("Ocurrió un error " + error.error)
      });
  }

  setPage(response: any): void {
    this.page = {
      size: response.size,
      totalElements: response.totalElements,
      pageNumber: response.number,
      totalPages: response.totalPages
    }
  }

  delete(id: number) {
    Swal.fire({
      title: '¿Seguro que desea eliminar la plantilla?',
      text: 'No podrá deshacer esta acción',
      icon: 'warning',
      customClass: {
        title: 'text-primary-1 font-weight-bold',
        confirmButton: 'btn btn-primary btn-sm mr-2',
        cancelButton: 'btn btn-secondary btn-sm'
      },
      showCancelButton: true,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      backdrop: false,
      allowOutsideClick: false,
      input: null,
      inputAttributes: {
        type: 'hidden'
      },
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.loadingService.start();
        this.formulariosService.deleteById(id).subscribe(
          data => {
            this.loadingService.stop()
            alert.alertOk("Plantilla eliminada correctamente!")
            this.getPage({ offset: 0 });
          }, error => {
            this.loadingService.stop()
            alert.alertError("Ocurrió un error, asegúrese que la plantilla no se esté utilizando en alguna solicitud")
          }
        )
      }
    });
  }



}
