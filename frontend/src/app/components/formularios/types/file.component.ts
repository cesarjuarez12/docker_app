import { Component, Input, OnInit } from '@angular/core';
import { FormulariosService } from '../formulario.service';
import { DomSanitizer } from '@angular/platform-browser';
import { alert } from 'src/app/common/alerts';
import { FieldOfRequest } from '../../request/field-of-request';
import { IEBrowserService } from '../../../services/ie-browser.service';
const mime = require('mime');

@Component({
  selector: 'app-file',
  template: `
  <input type="file" class="form-control-file mt-2"  >
  <!--label>{{item}}</label-->
  <div class="text-left form-control-label d-block d-sm-block d-md-block d-lg-block " *ngIf="base">
  <button class="btn btn-secondary btn-sm"  *ngIf="base && IE == false" (click)="DownloadFile(item)">
    <i class="fas fa-file"></i> Archivo Base
  </button>
  <button class="btn btn-secondary btn-sm" *ngIf="base && IE == true"  (click)="DownloadFileIE(item)">
  <i class="fas fa-file"></i> Archivo Base
  </button>
  </div>
  `
})
export class FileComponent implements OnInit {
  @Input() id: any | undefined;
  @Input() item: any | undefined;
  @Input() name: string | undefined;
  @Input() base: any | undefined;
  @Input() mime: any | undefined;
  
  fileUrl: any;
  IE: boolean = false;
  
  constructor(
    private BrowserDetection: IEBrowserService,
    private _RequesType: FormulariosService, 
    private IEBrowser: IEBrowserService) { }

  async ngOnInit(): Promise<void> {
    this.IE = await this.BrowserDetection.detectionBrowser();
  }
  

  DownloadFile(field: FieldOfRequest) {
    //this.loadingService.start()
    this._RequesType.getFile(field.id).subscribe((data) => {
      var url = window.URL.createObjectURL(data);
      var ext = mime.getExtension(data.type);
      var anchor = document.createElement("a");
      var title = field.title
      if(field.isRequiredConditional == true && field.secondaryTitle != null) {
          title = field.secondaryTitle
      }
      anchor.download = "BASE-" + field.title.toUpperCase() + "-" + field.id  + "." + ext;
      anchor.href = url;
      anchor.click();
    }, error => {
      alert.alertError("Ocurrió un error al intentar obtener el archivo")
      //this.loadingService.stop()
    })
  }

  DownloadFileIE(field: any) {
    let mimetype = field.mimeTypeOfBaseFile;
    var ext = mime.getExtension(mimetype);
    this._RequesType.getFileIE(field.id, mimetype).subscribe((data) => {
      var namefile = "ARCHIVO-" + field.title.toUpperCase()  + "." + ext;
      window.navigator.msSaveBlob(data,namefile);
    }, error => {
      alert.alertError("Ocurrió un error al intentar obtener el archivo");
    })
  }

}
