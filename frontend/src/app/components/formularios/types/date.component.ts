import { Component, Input, OnInit } from '@angular/core';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-date',
  template: `
  <input type="text" class="form-control custom-columna-21-input" style="display: inline-block !important;" bsDatepicker [bsConfig]="{ dateInputFormat: 'DD/MM/YYYY', containerClass: 'theme-dark-blue'}"
  (keyup)="onKeyUpFecha($event)" (keydown.backspace)="onKeydown($event)">`
})
export class DateComponent implements OnInit {
  //
  @Input() name: string | undefined;
  constructor(private localeService: BsLocaleService) { 
    this.localeService.use('es');
   }

  ngOnInit(): void {
  }
  
onKeyUpFecha(x: any) {
  if (!/^([])\2(\d{4})$/i.test(x.target.value)) {
    x.target.value = x.target.value.replace(/[^]+/ig,"");
   
}
}

onKeydown(event: any) {
if(event.code == 'Backspace'){
  
}
}

}
