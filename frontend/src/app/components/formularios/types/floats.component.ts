import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-float',
  //templateUrl: './input.component.html',
  //styleUrls: ['./input.component.css']
  template: `<input type="text" class="form-control" name="{{name}}" (keyup)="onKeyUp($event)">`
})
export class FloatComponent implements OnInit {
  @Input() name: string | undefined;
 
  constructor() { }

  ngOnInit(): void {
  }

  onKeyUp(x: any) {
    if (!/^\d*\.?\d*$/i.test(x.target.value)) {
      x.target.value = x.target.value.replace(/[^\d*\.?\d*$]+/ig,"");
  }
  
  } 

}
