import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-int',
  //templateUrl: './input.component.html',
  //styleUrls: ['./input.component.css']
  template: `<input type="text" class="form-control" name="{{name}}" (keyup)="onKeyUp($event)">`
})
export class IntegerComponent implements OnInit {
  @Input() name: string | undefined;
 
  constructor() { }

  ngOnInit(): void {
  }

  onKeyUp(x: any) {
    if (!/^[0-9]*$/i.test(x.target.value)) {
      x.target.value = x.target.value.replace(/[^0-9]+/ig,"");
    }
  }
}