import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-select',
  //templateUrl: './select.component.html',
  //styleUrls: ['./select.component.css']
  template: `
  <!--select class="form-control" name="{{name}}">
    <option *ngFor="let item of options;" value="{{item.id}}">{{item.title}}</option>
</select-->


<ng-select 
      class="custom mb-3"
      [items]="options"
      bindLabel="title"
      bindValue="id"
      [searchable]="false"
      [(ngModel)]="Type"
    >
  </ng-select>
`,
})
export class SelectComponent implements OnInit {

  @Input() name: string | undefined;
  @Input() options: any | undefined;

  Type ='';
  constructor() { }

  ngOnInit(): void {
   this.Type = this.options[0]['id'];
    //console.log(this.options[0]['id']);
  }

}
