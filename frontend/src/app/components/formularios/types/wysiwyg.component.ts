import { Component, Input, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-wysiwyg',
  template: `<angular-editor name="{{name}}" [(ngModel)]="htmlContent" [config]="config"></angular-editor>`,
})
export class WYSIWYGComponent implements OnInit {

  ngOnInit(): void {
  }


  @Input() name: string | undefined;

  htmlContent :any = '';

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: false,
    height: '10rem',
    minHeight: '5rem',
    placeholder: 'Escribe aquí...',
    translate: 'yes',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',

    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ],
    toolbarHiddenButtons: [
      [
        'subscript',
        'outdent',
      ],
      [ 'backgroundColor',
        'customClasses',
       'insertImage',
        'insertVideo',
        'toggleEditorMode'
      ]
    ]
    };
  }
