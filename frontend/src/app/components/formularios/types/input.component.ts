import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  //templateUrl: './input.component.html',
  //styleUrls: ['./input.component.css']
  template: `<input type="text" class="form-control" name="{{name}}">`
})
export class InputComponent implements OnInit {
  @Input() name: string | undefined;
  
  constructor() { }

  ngOnInit(): void {
  }

}
