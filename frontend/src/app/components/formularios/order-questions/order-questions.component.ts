import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-order-questions',
  templateUrl: './order-questions.component.html',
  styleUrls: ['./order-questions.component.css']
})
export class OrderQuestionsComponent implements OnInit {

  title: string
  fields: any[] = []

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit(): void {
    this.reorder()
  }

  reorder() {
    this.fields.forEach((field, index) => field.ord = index )
  }

}
