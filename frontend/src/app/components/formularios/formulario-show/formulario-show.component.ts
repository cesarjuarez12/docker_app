import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { FormulariosService } from '../formulario.service';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FileTypeAllowedService } from '../../config-file-types/file-type-allowed.service';
import { IEBrowserService } from '../../../services/ie-browser.service';

@Component({
  selector: 'app-formulario-show',
  templateUrl: './formulario-show.component.html',
  styleUrls: ['./formulario-show.component.css']
})
export class FormularioShowComponent implements OnInit {

  Header:string = '';
  Titulo:string = '';
  fields:any = [];
  isFiles: any[] = [];
  paramID = 0;

  //IE
  IE: boolean = false;

  constructor(
    private BrowserDetection: IEBrowserService,
    private _RequesType: FormulariosService, 
    private _TypeAllowerService: FileTypeAllowedService,
    private router: Router, private rutaActiva: ActivatedRoute,
    private sanitizer : DomSanitizer,
    private formBuilder: FormBuilder,
    private ngxLoadingService: NgxUiLoaderService) { }


  async ngOnInit(): Promise<void> {
    this.IE = await this.BrowserDetection.detectionBrowser();
    this.Header = 'VISUALIZACION DE FORMULARIOS';
    this.paramID = this.rutaActiva.snapshot.params['id'];
    this.cargarformulario();
  }

  cargarformulario(){
    this.ngxLoadingService.start();
    this._RequesType.getById(this.paramID).subscribe((response) => { 

      for (let index = 0; index < response.fields.length; index++) {
        this.isFiles.push({ pregunta: false });
      }
      this.Titulo = response.description;
      this.fields = response.fields;
            
      this.ngxLoadingService.stop();
    });
  }
}
