import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioShowComponent } from './formulario-show.component';

describe('FormularioShowComponent', () => {
  let component: FormularioShowComponent;
  let fixture: ComponentFixture<FormularioShowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioShowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
