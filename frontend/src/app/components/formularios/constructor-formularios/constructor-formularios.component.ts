import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-constructor-formularios',
  templateUrl: './constructor-formularios.component.html',
  styleUrls: ['./constructor-formularios.component.css']
})
export class ConstructorFormulariosComponent implements OnInit {
  @Input() id:any;
  @Input() item:any;
  @Input() type:any;
  @Input() name:any;
  @Input() base:any;
  @Input() mime:any;
  @Input() options:any;

  constructor() { }

  ngOnInit(): void {
  }

}
