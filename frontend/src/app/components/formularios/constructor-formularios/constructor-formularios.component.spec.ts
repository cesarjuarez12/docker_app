import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructorFormulariosComponent } from './constructor-formularios.component';

describe('ConstructorFormulariosComponent', () => {
  let component: ConstructorFormulariosComponent;
  let fixture: ComponentFixture<ConstructorFormulariosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConstructorFormulariosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructorFormulariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
