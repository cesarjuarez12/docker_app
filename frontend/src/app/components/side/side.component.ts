import { BreakpointObserver, Breakpoints, BreakpointState, MediaMatcher } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { faAngleDoubleLeft, faBars, faBell, faCheck, faCheckDouble, faChevronRight, faClipboardList, faCog, faEnvelope, faEraser, faFile, faFlag, faHistory, faIdCard, faInbox, faInfo, faList, faListOl, faObjectGroup, faPencilAlt, faServer, faSignOutAlt, faStickyNote, faTasks, faTimes, faUsers } from '@fortawesome/free-solid-svg-icons';
import { RoleService } from 'src/app/auth/role.service';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { IEBrowserService } from 'src/app/services/ie-browser.service';
import { InfoUserService } from 'src/app/services/info-user.service';

@Component({
  selector: 'app-side',
  templateUrl: './side.component.html',
  styleUrls: ['./side.component.css']
})
export class SideComponent implements OnInit {
  
  opened = true;
  dock = true;

  overMode: 'over'
  pushMode: 'push'
  slideMode: 'slide'

  mode:any;

  backdrop = false;
  clickBackdrop = false;
  roles: string[];
  isLoggedIn = false;
  username: string;
  visible = false;
  customClass = "custom-class"
  work: string;
  apellidos: string;
  nombres: string;
  img: any;
  _opened: boolean;
  closeOnNavigate = false
  isSolicitudesOpened: boolean = false;
  isConfiguracionOpened: boolean = false;
  isPlantillasOpened: boolean = false;



  faBars = faBars;
  faTimes = faTimes;
  faList = faList;
  faHistory = faHistory;
  faPencilAlt = faPencilAlt;
  faListOl = faListOl;
  faInfo = faInfo;
  faCog = faCog;
  faUsers = faUsers;
  faEnvelope = faEnvelope;
  faIdCard = faIdCard;
  faBell = faBell;
  faObjectGroup = faObjectGroup;
  faStickyNote = faStickyNote;
  faSignOut = faSignOutAlt;
  faAngle = faAngleDoubleLeft;
  faFlag = faFlag;
  faCheckDouble = faCheckDouble;
  faCheck = faCheck;
  faEraser = faEraser;
  faInbox = faInbox;
  faChevronRight = faChevronRight
  faTasks = faTasks
  faClipboardList = faClipboardList
  faServer = faServer

  //Animation
  animated: boolean;
  src: any;

  constructor(
    private BrowserDetection: IEBrowserService,
    private tokenStorageService: TokenStorageService, 
    public router: Router, 
    public roleService: RoleService, 
    public breakpointObserver: BreakpointObserver, 
    private UserServ: InfoUserService, 
    private sanitizer: DomSanitizer, 
    public mediaMatcher: MediaMatcher) { 
      this.mode = this.overMode;
    }

  async ngOnInit(): Promise<void> {
    this.animated = await this.BrowserDetection.detectionBrowser();
    //this.detectionBrowser();
    this._opened = true;
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;
      this.username = user.displayName;

    }
    this.get_photo();
    this.cargar_info();
 
    
    this.detectionSizeOnInit()
    this.detectionScreen()
  }


detectionSizeOnInit(){
  if (this.breakpointObserver.isMatched('(min-width: 0px) and  (max-width: 959.98px)') == true) {
    this.configSmall()
  } else
  if (this.breakpointObserver.isMatched('(min-width: 960px) and  (max-width: 1279.98px)') == true) {
    this.configMedium()
  } else 
  if (this.breakpointObserver.isMatched('((min-width: 1280px) and  (max-width: 1920.98px)') == true) {
    this.configLarge()
  } 
}

  detectionScreen() {
    this.breakpointObserver
      .observe([Breakpoints.Small])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          console.log(state.breakpoints)
          this.configSmall()
        }
      });

    this.breakpointObserver
      .observe([Breakpoints.Medium])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          console.log(state.breakpoints)
          this.configMedium()
        }
      });

    this.breakpointObserver
      .observe([Breakpoints.Large])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          console.log(state.breakpoints)
          this.configLarge()
        }
      });
  }


  configSmall() {
    this.closeOnNavigate = true
    this.mode = this.overMode;
    this.dock = false
    this.backdrop = true
    this.clickBackdrop = true
  }

  configMedium() {
    this.mode = this.pushMode;
    this.dock = true;   
    this.backdrop = false
    this.clickBackdrop = false
  }

  configLarge() {
    this.mode = this.pushMode;
    this.dock = true;
    this.backdrop = false;
    this.clickBackdrop = false
  }

  cargar_info() {
    this.work = localStorage.getItem('trabajo');
    this.username = localStorage.getItem('nombres');
    this.img = localStorage.getItem('photo');
  }

  closeAll() {
    this.isSolicitudesOpened = false;
    this.isConfiguracionOpened = false;
    this.isPlantillasOpened = false;
  }

  openSolicitudes() {
    this.isSolicitudesOpened = !this.isSolicitudesOpened;
    this.isConfiguracionOpened = false;
    this.isPlantillasOpened = false;
  }

  openPlantillas() {
    this.isPlantillasOpened = !this.isPlantillasOpened;
    this.isSolicitudesOpened = false;
    this.isConfiguracionOpened = false;
  }

  openConfig() {
    this.isConfiguracionOpened = !this.isConfiguracionOpened;
    this.isSolicitudesOpened = false;
    this.isPlantillasOpened = false;
  }

  toggleSidebar() {
    this.opened = !this.opened;
  }


  _toggleOpened(): void {
    this._opened = !this._opened;
  }

  _onOpenStart(): void {

  }

  _onOpened(): void {

  }

  _onCloseStart(): void {

  }

  _onClosed(): void {

  }

  _onTransitionEnd(): void {

  }

  _onBackdropClicked(): void {
  }


  closeOnClick(){
    if(this.closeOnNavigate == true){
      this._opened = false;
    }
  }


  openMenu() {
    this.visible = true;
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.assign('https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=' + window.location.href);
  }

get_photo(){
  this.UserServ.get_photo().subscribe((response: any) => 
  {
    this.src = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(response));
  });
}


  /*detectionBrowser(){
    let isIEOrEdge = /msie\s|trident\/|Edge\//i.test(window.navigator.userAgent)
    if(isIEOrEdge){
      this.animated = false;
    } else {
      this.animated = true;
    }
  }*/
}
