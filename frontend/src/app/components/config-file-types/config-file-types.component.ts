import { Component, OnInit } from '@angular/core';
import { FileTypeAllowed } from './file-type-allowed';
import { FileTypeAllowedService } from './file-type-allowed.service';
import mime  from 'src/assets/json/mime-type.json'
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { alert } from 'src/app/common/alerts';

@Component({
  selector: 'app-config-file-types',
  templateUrl: './config-file-types.component.html',
  styleUrls: ['./config-file-types.component.css']
})
export class ConfigFileTypesComponent implements OnInit {

  fileTypesAllowed: FileTypeAllowed[];
  mimeTypes = mime;
  faTimes = faTimes;
  loading:boolean = true;

  constructor(private fileTypesAllowedService: FileTypeAllowedService) { }


  getFileTypes(): void {
    this.fileTypesAllowedService.getFileTypesAllowed().subscribe((data) => {
      this.fileTypesAllowed = data as FileTypeAllowed[];
      this.loading = false;
    })
  }

  addFileType(event?: any) {
    this.loading = true;
    let fileTypeAllowed: FileTypeAllowed = new FileTypeAllowed(event.extension, event.mimeType);

    this.fileTypesAllowedService.post(fileTypeAllowed).subscribe(
      (data) => {
        this.getFileTypes();
        alert.alertOk("Extensión agregada correctamente.")
      },
      (error) => {
        this.loading = false;
        alert.alertError("Ocurrió un error al guardar el tipo de archivo, probablemente la extensión ya está registrada.")
      })
  }

  delete(id: number) {
    this.loading = true;
    this.fileTypesAllowedService.delete(id).subscribe(
      (data) => {
        this.getFileTypes();
        alert.alertOk("Tipo de archivo eliminado correctamente!");
      },
      (error) => {
        this.loading = false;
        alert.alertError("Ocurrió un error al intentar eliminar el tipo de archivo")
      })
  }



  ngOnInit(): void {
    this.getFileTypes();
  }



}
