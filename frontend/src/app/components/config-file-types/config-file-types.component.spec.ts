import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigFileTypesComponent } from './config-file-types.component';

describe('ConfigFileTypesComponent', () => {
  let component: ConfigFileTypesComponent;
  let fixture: ComponentFixture<ConfigFileTypesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigFileTypesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigFileTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
