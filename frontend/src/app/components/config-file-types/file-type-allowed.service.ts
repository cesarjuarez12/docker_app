import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { FileTypeAllowed } from "./file-type-allowed";


@Injectable({
    providedIn: 'root'
  })
export class FileTypeAllowedService {

    private urlEndPoint: string = 'api/file-types/';

    private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })
    
    constructor(private http: HttpClient) { }

    getFileTypesAllowed(): Observable<FileTypeAllowed[]> {
        return this.http.get<FileTypeAllowed[]>(`${this.urlEndPoint}`, { headers: this.httpHeaders })
    }

    post(fileTypeAllowed: FileTypeAllowed): Observable<any> {
        return this.http.post<any>(`${this.urlEndPoint}`, fileTypeAllowed, { headers: this.httpHeaders })
    }

    delete(id: number): Observable<any> {
        return this.http.delete<any>(`${this.urlEndPoint}` + id, { headers: this.httpHeaders })
    }

}