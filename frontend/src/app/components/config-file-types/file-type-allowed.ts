export class FileTypeAllowed{
    id:number;
    fileExtension:string;
    mimeType:string;

    constructor(fileExtension:string, mimeType:string){
        this.fileExtension = fileExtension;
        this.mimeType = mimeType;
    }
}