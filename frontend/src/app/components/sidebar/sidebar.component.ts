import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Config } from './types';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(private router: Router) {}

  title = 'navfinal';
  public href: string = "";


  // CollapseSide
  SidebarCollapse(){
    $('.menu-collapsed').toggleClass('d-none');
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
    $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
    $('#imagen-user-collapse').css('display', 'none');
  }
  
  ngOnInit() {
    //Toggle Click Function
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
    setTimeout(()=>{                           //<<<---using ()=> syntax
      this.deteccion()
 }, 100);
  }

  deteccion(){
     this.href = this.router.url;
    for(var i=0;i < this.menus.length; i++ ){
      if(this.menus[i].url){
        if(this.menus[i].url == this.href){ 
          this.menus[i].active = true;
         }
      }else{
        for(var j=0;j < this.menus[i].submenu.length; j++ ){
          if(this.menus[i].submenu[j].url== this.href){ 
            this.menus[i].active = true;
           }
        }
      }
     }
     console.log(this.menus);
   }
  
  // signle open mode
  options: Config = { multi: false };
  
  menus: any[] = [
    { 
      card: 'profile',
      iconClass: 'fa fa-globe',
      active: false,
      url: '/perfil'
    },
    { 
      name: 'Solicitudes',
      iconClass: 'fa fa-calendar',
      active: false,
      submenu: [
        { name: 'En proceso', iconClass: 'fas fa-history', url: '/departamento-interno/form' },
        { name: 'Historial', iconClass: 'fa fa-list-ol', url: '/departamento-interno/form' },
        { name: 'Creacion', iconClass: 'fas fa-pencil-alt', url: '/departamento-interno/form' }
      ]
    },
    { 
      name: 'Ayuda',
      iconClass: 'fas fa-info',
      active: false,
      url: '/ayuda'
    },
    { 
      name: 'Configuracion Global',
      iconClass: 'fa fa-calendar',
      active: false,
      submenu: [
        { name: 'Dpto. Interno Solicitud', iconClass: 'fas fa-envelope', url: '/departamentos-internos' },
        { name: 'Dpto. Interno Soporte', iconClass: 'fas fa-id-card', url: '/departamentos-internos-om' },
        { name: 'Recardotorio', iconClass: 'fas fa-bell', url: '/configuracion-global' },
        { name: 'Grupos', iconClass: 'fas fa-users-cog', url: '/configuracion-grupos' },
        { name: 'Activacion de Notificaciones', iconClass: 'fas fa-flag', url: '/coordinadores' }
      ]
    },
    { 
      name: 'Usuarios',
      iconClass: 'fas fa-users',
      active: false,
      submenu: [
        { name: 'Plantilla de Solicitudes', iconClass: 'fas fa-sticky-note', url: '/departamentos-internos' },
        { name: 'Tipo de Archivos Solicitudes', iconClass: 'fas fa-envelope', url: '/departamentos-internos-om' },
        { name: 'Formularios', iconClass: 'fas fa-id-card', url: '/configuracion-global' }
      ]
    }

  ];
}
