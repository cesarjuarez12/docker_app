export type rutas = [
    { 
      card: 'profile',
      iconClass: 'fa fa-globe',
      active: false,
      url: '/perfil'
    },
    { 
      name: 'Front-end',
      iconClass: 'fa fa-code',
      active: false,
      submenu: [
        { name: 'HTML', iconClass: 'fab fa-firefox', url: '/html' },
        { name: 'CSS', iconClass: 'fa fa-code', url: '/css' },
        { name: 'Javascript', iconClass: 'fa fa-code', url: '/javascript' }
      ]
    },
    { 
      name: 'Web Ayuda',
      iconClass: 'fa fa-globe',
      active: false,
      submenu: [
        { name: 'Chrome', iconClass: 'fa fa-code', url: '#' }
      ]
    },
    { //http://localhost:4200/html
      name: 'Responsive web',
      iconClass: 'fa fa-mobile',
      active: false,
      submenu: [
        { name: 'Tablets', iconClass: 'fa fa-code', url: '#' },
        { name: 'Mobiles', iconClass: 'fa fa-code', url: '#' },
        { name: 'Desktop', iconClass: 'fa fa-code', url: '#' }
      ]
    },
    { 
      name: 'Web Browser',
      iconClass: 'fa fa-globe',
      active: false,
      submenu: [
        { name: 'Chrome', iconClass: 'fa fa-code', url: '#' },
        { name: 'Firefox', iconClass: 'fa fa-code', url: '#' },
        { name: 'Desktop', iconClass: 'fa fa-code', url: '#' }
      ]
    },
    { 
      name: 'Web Browser',
      iconClass: 'fa fa-globe',
      active: false,
      url: '/ayuda'
    },
    { 
      name: 'card',
      iconClass: 'fa fa-globe',
      active: false,
      url: '/perfil'
    },
    { 
      name: 'card',
      iconClass: 'fa fa-globe',
      active: false,
      url: '/css'
    }
  ];