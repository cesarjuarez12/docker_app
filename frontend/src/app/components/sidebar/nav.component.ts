import { Component, OnInit, Input } from '@angular/core';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { Config } from './types';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private tokenStorageService: TokenStorageService) { }
  
  @Input() options: any;
  @Input() menus: any[] | any;

  config: Config | any;
  trabajo: string;
  apellidos: string;
  nombres: string;
  img: any;
  user: any;
  rol: any;
  
  ngOnInit() {
    this.config = this.mergeConfig(this.options);
    this.cargar_info();
    this.getRol();
  }

  
  cargar_info(){
    this.trabajo = localStorage.getItem('trabajo');
    this.nombres = localStorage.getItem('nombres');
    this.apellidos = localStorage.getItem('apellidos');
    this.img =localStorage.getItem('photo');
  }

  getRol(){
    this.user = this.tokenStorageService.getUser();
    this.rol = this.user.roles[0];
  }


  mergeConfig(options: Config) {
    // 기본 옵션
    const config = {
      // selector: '#accordion',
      multi: true
    };

    return { ...config, ...options };
  }

  toggle(index: number) {
    // 멀티 오픈을 허용하지 않으면 타깃 이외의 모든 submenu를 클로즈한다.
    if (!this.config.multi) {
      this.menus.filter(
        (menu: { active: any; }, i: number) => i !== index && menu.active
      ).forEach((menu: { active: boolean; }) => menu.active = !menu.active);
    }

    // Menu의 active를 반전
    this.menus[index].active = !this.menus[index].active;
  }

  // CollapseSide
  SidebarCollapse(){

    $('#cerrar-session-collapse').removeClass('btn-cerrar-session');
    $('#cerrar-session-collapse').addClass('btn-cerrar-session-collapsed');
    $('#cerrar-session-icon').css('visibility', 'visible');
    $('#text-cerrar-session').css('visibility', 'hidden');
    $('#expand-sidebar').css('visibility', 'visible');


    $('.menu-collapsed').toggleClass('d-none');
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
    $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');

    $('#imagen-user-collapse').css('display', 'block');
  }


  logout(): void {
    this.tokenStorageService.signOut();
    localStorage.clear();
    window.location.reload();
  }
}
