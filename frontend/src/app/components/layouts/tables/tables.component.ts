import { Component, OnInit, ViewChild } from '@angular/core';
import { ColumnMode, DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { faSearch } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})
export class TablesComponent implements OnInit {

  faSearch = faSearch;

  rows = [
    { name: 'Austin', gender: 'Male', company: 'Swimlane' },
    { name: 'Dany', gender: 'Male', company: 'KFC' },
    { name: 'Molly', gender: 'Female', company: 'Burger King' },
    { name: 'David', gender: 'Male', company: 'Campero' },
    { name: 'Martin', gender: 'Male', company: 'Swimlane' },
    { name: 'Mark', gender: 'Male', company: 'KFC' },
    { name: 'Teresa', gender: 'Female', company: 'Burger King' },
    { name: 'Lucas', gender: 'Male', company: 'Campero' },
    { name: 'Fernando', gender: 'Male', company: 'Swimlane' },
    { name: 'Gabriel', gender: 'Male', company: 'KFC' },
    { name: 'Gabriela', gender: 'Female', company: 'Burger King' },
    { name: 'Ethan', gender: 'Male', company: 'Campero' },
    { name: 'Diego', gender: 'Male', company: 'Swimlane' },
    { name: 'Charles', gender: 'Male', company: 'KFC' },
    { name: 'Scarleth', gender: 'Female', company: 'Burger King' },
    { name: 'Anthony', gender: 'Male', company: 'Campero' },
    { name: 'Chris', gender: 'Male', company: 'Swimlane' },
    { name: 'Paul', gender: 'Male', company: 'KFC' },
    { name: 'Sophie', gender: 'Female', company: 'Burger King' },
    { name: 'Christian', gender: 'Male', company: 'Campero' },
  ];

  temp = [{ name: '', gender: '', company: '' }];

  columns = [{ prop: 'name' }, { name: 'Gender' }, { name: 'Company' }];
  @ViewChild(DatatableComponent)
  table!: DatatableComponent;

  SelectionType = SelectionType;
  ColumnMode = ColumnMode;

  constructor() {
    this.temp = [...this.rows];
    this.rows = this.rows;
  }

  updateFilter(event?: any) {
    const val = event.target.value.toLowerCase();
    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  ngOnInit(): void {
  }

}
