import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardAlternativeComponent } from './wizard-alternative.component';

describe('WizardAlternativeComponent', () => {
  let component: WizardAlternativeComponent;
  let fixture: ComponentFixture<WizardAlternativeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WizardAlternativeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardAlternativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
