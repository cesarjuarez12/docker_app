import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';


@Component({
  selector: 'app-wizard-alternative',
  templateUrl: './wizard-alternative.component.html',
  styleUrls: ['./wizard-alternative.component.css']
})
export class WizardAlternativeComponent implements OnInit {

  current: number = 1;
  max: number = 3;
  semicircle: boolean = false;
  radius: number = 125;

  constructor() {
  }

  getOverlayStyle() {
    const isSemi = this.semicircle;
    const transform = (isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

    return {
      top: isSemi ? 'auto' : '50%',
      bottom: isSemi ? '5%' : 'auto',
      left: '50%',
      transform,
      'font-size': this.radius / 3.5 + 'px',
    };
  }

  @ViewChild('before') before: ElementRef | undefined;

  showPreviousStep() {
    if (this.current != 1) {
      this.current = this.current - 1;
    }
  }

  showNextStep() {
    if (this.current != this.max) {
      this.current = this.current + 1;
    }
  }

  ngOnInit() {
  }

}
