import { Component, OnInit } from '@angular/core';

declare const $: any;

@Component({
  selector: 'app-siderbarhead',
  templateUrl: './siderbarhead.component.html',
  styleUrls: ['./siderbarhead.component.css']
})
export class SiderbarheadComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    $('#expand-sidebar').css('visibility', 'hidden');
  }

  expand(){
    $('#cerrar-session-collapse').removeClass('btn-cerrar-session-collapsed');
    $('#cerrar-session-icon').css('visibility', 'hidden');
    $('#text-cerrar-session').css('visibility', 'visible');
    $('#cerrar-session-collapse').addClass('btn-cerrar-session');
    $('#imagen-user-collapse').css('display', 'none');

    $('.menu-collapsed').toggleClass('d-none');
    $('.sidebar-submenu').toggleClass('d-none');
    $('.submenu-icon').toggleClass('d-none');
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
    var SeparatorTitle = $('.sidebar-separator-title');
     if (SeparatorTitle.hasClass('d-flex')) {
         SeparatorTitle.removeClass('d-flex');
     } else {
         SeparatorTitle.addClass('d-flex');
     }
     //$('.Cuadrado').toggleClass('d-block');
     //$('#cuadrado').css('visibility', 'visible');
     $('#close-sidebar').css('visibility', 'visible');
     $('#expand-sidebar').css('visibility', 'hidden');
     $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
     
  }

}
