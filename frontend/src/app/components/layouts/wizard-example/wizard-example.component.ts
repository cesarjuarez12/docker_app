import { Component, OnInit } from '@angular/core';
import { NgWizardConfig, NgWizardService, StepChangedArgs, StepValidationArgs, STEP_STATE, THEME } from 'ng-wizard';
import { of } from 'rxjs';
import { faSquare } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-wizard-example',
  templateUrl: './wizard-example.component.html',
  styleUrls: ['./wizard-example.component.css']
})
export class WizardExampleComponent implements OnInit {

  faSquare = faSquare;

  stepStates = {
    normal: STEP_STATE.normal,
    disabled: STEP_STATE.disabled,
    error: STEP_STATE.error,
    hidden: STEP_STATE.hidden
  };

  config: NgWizardConfig = {
    selected: 0,
    theme: THEME.circles,
    cycleSteps: true,
    /*lang: { next: 'Continuar', previous: 'Regresar' },*/
    toolbarSettings: {
      showNextButton: false,
      showPreviousButton: false,
      /*toolbarExtraButtons: [
        { text: 'Terminar', class: 'btn btn-secondary', event: () => { alert("Finished!!!"); } }
      ],*/
    }
  };

  constructor(private ngWizardService: NgWizardService) {
  }


  showPreviousStep(event?: Event) {
    this.ngWizardService.previous();
  }

  showNextStep(event?: Event) {
    this.ngWizardService.next();
  }

  resetWizard(event?: Event) {
    this.ngWizardService.reset();
  }

  setTheme(theme: THEME) {
    this.ngWizardService.theme(theme);
  }

  stepChanged(args: StepChangedArgs) {
    console.log(args.step);
  }

  isValidTypeBoolean: boolean = true;

  isValidFunctionReturnsBoolean(args: StepValidationArgs) {
    return true;
  }

  isValidFunctionReturnsObservable(args: StepValidationArgs) {
    return of(true);
  }

  finish() {
    alert("Finalizado!");
  }


  ngOnInit() {
  }

}
