import { Component, OnInit } from '@angular/core';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.css']
})
export class InputsComponent implements OnInit {


  faExclamationTriangle = faExclamationTriangle;

  selectedCar: number | undefined;

  cars = [
    { id: 1, name: 'Opción 1' },
    { id: 2, name: 'Opción 2' },
    { id: 3, name: 'Opción 3' },
    { id: 4, name: 'Opción 4' },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
