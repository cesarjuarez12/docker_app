export class ConfiguracionGlobal{
    id!: number;
    diaNotificaciones!: any;
    horaNotificaciones!: any;
    emailNotificaciones!: string;
    password:string;
    host:string;
    port:number;
    protocol:string;
    auth:boolean
    starttls: boolean
    ssl: boolean
    maxServers: number
}
