import { AbstractControl, FormControl, ValidationErrors } from "@angular/forms";

export class CustomValidation {

    public static ValidateEmailTigo(control: FormControl) : ValidationErrors {
        let email = control.value;
        if(email.endsWith("@tigo.com.gt")){
            return null;
        }
        return { ValidateEmailTigo : true};       
    }

    public static ValidateHour(control: FormControl) : ValidationErrors {
        let value = control.value
        let date:Date = new Date(value)
        let minutes = date.getMinutes()
        
        if(minutes % 15 === 0){
            return null;
        }

        return { ValidateHour : true};       
    }

}