import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { faExclamationTriangle, faEye } from '@fortawesome/free-solid-svg-icons';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import { ConfiguracionGlobal } from './configuracion-global';
import { ConfiguracionGlobalService } from './configuracion-global.service';
import { CustomValidation } from './custom-validation';

@Component({
  selector: 'app-configuracion-global',
  templateUrl: './configuracion-global.component.html',
  styleUrls: ['./configuracion-global.component.css']
})
export class ConfiguracionGlobalComponent implements OnInit {

  faExclamationTriangle = faExclamationTriangle;
  faEye = faEye;

  conf: ConfiguracionGlobal = new ConfiguracionGlobal();

  conForm: FormGroup;

  mytime: string;

  days = [
    { id: 'MONDAY', name: 'Lunes' },
    { id: 'TUESDAY', name: 'Martes' },
    { id: 'WEDNESDAY', name: 'Miércoles' },
    { id: 'THURSDAY', name: 'Jueves' },
    { id: 'FRIDAY', name: 'Viernes' },
    { id: 'SATURDAY', name: 'Sábado' },
    { id: 'SUNDAY', name: 'Domingo' },
  ];

  constructor(private configuracionService: ConfiguracionGlobalService, private ngxUiLoaderService: NgxUiLoaderService) { }

  createForm() {
    this.conForm = new FormGroup({
      emailNotificaciones: new FormControl('', [
        Validators.required, CustomValidation.ValidateEmailTigo, Validators.email
      ]),
      diaNotificaciones: new FormControl('', [Validators.required]),
      horaNotificaciones: new FormControl('', [Validators.required, CustomValidation.ValidateHour]),
      password: new FormControl(''),
      host: new FormControl('', [Validators.required]),
      port: new FormControl('', [Validators.required, Validators.min(1), Validators.max(65536)]),
      protocol: new FormControl('', [Validators.required]),
      auth: new FormControl('', [Validators.required]),
      starttls: new FormControl('', [Validators.required]),
      ssl: new FormControl('', [Validators.required])
    })
  }



  getConfig() {
    this.ngxUiLoaderService.start();
    this.configuracionService.getConfiguracionGlobal().subscribe((conf) => {
      this.conf = conf
      this.conForm.controls['emailNotificaciones'].setValue(this.conf.emailNotificaciones)
      this.conForm.controls['diaNotificaciones'].setValue(this.conf.diaNotificaciones)
      this.conForm.controls['horaNotificaciones'].setValue(this.conf.horaNotificaciones)
      this.conForm.controls['password'].setValue(this.conf.password)
      this.conForm.controls['host'].setValue(this.conf.host)
      this.conForm.controls['port'].setValue(this.conf.port)
      this.conForm.controls['protocol'].setValue(this.conf.protocol)
      this.conForm.controls['auth'].setValue(this.conf.auth)
      this.conForm.controls['starttls'].setValue(this.conf.starttls)
      this.conForm.controls['ssl'].setValue(this.conf.ssl)
      this.ngxUiLoaderService.stop();
    }, (error) => {
      this.ngxUiLoaderService.stop();
      alert.alertError("¡Tenemos problemas para obtener la configuración!")
      history.back()
    })
  }


  submit() {
    this.ngxUiLoaderService.start();
    this.conf = {
      id: this.conf.id,
      diaNotificaciones: this.conForm.value['diaNotificaciones'],
      horaNotificaciones: this.conForm.value['horaNotificaciones'],
      emailNotificaciones: this.conForm.value['emailNotificaciones'],
      password: this.conForm.value['password'],
      host: this.conForm.value['host'],
      port: this.conForm.value['port'],
      protocol: this.conForm.value['protocol'],
      auth: this.conForm.value['auth'],
      starttls: this.conForm.value['starttls'],
      ssl: this.conForm.value['ssl'],
      maxServers: this.conf.maxServers
    }
    this.configuracionService.patch(this.conf).subscribe((data) => {
      this.conf = data.conf;
      alert.alertOk("Configuración guardada correctamente!")
      this.ngxUiLoaderService.stop();
    },
      error => {
        console.log(error)
        alert.alertError("Ocurrió un error al guardar la configuración")
        this.ngxUiLoaderService.stop();
      })
  }


  ngOnInit(): void {
    this.createForm();
    this.getConfig();
  }

}
