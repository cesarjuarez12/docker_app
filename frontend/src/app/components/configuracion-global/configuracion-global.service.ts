import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ConfiguracionGlobal } from "./configuracion-global";


@Injectable()
export class ConfiguracionGlobalService {

    private urlEndPoint: string = 'api/configuracion-global/';

    private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })
    
    constructor(private http: HttpClient) { }

    getConfiguracionGlobal(): Observable<ConfiguracionGlobal> {
        return this.http.get<ConfiguracionGlobal>(`${this.urlEndPoint}`, { headers: this.httpHeaders })
    }

    patch(conf: ConfiguracionGlobal): Observable<any> {
        return this.http.patch<any>(`${this.urlEndPoint}`, conf, { headers: this.httpHeaders })
    }

}