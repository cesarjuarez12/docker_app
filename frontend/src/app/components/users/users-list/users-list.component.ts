import { Component, OnInit } from '@angular/core';
import { faEye, faPen, faTrash } from '@fortawesome/free-solid-svg-icons';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { data } from 'jquery';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { alert } from 'src/app/common/alerts';
import { UserProfile } from 'src/app/pages/template/profile/user-profile';
import { InfoUserService } from 'src/app/services/info-user.service';
import { Page } from './page';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  ColumnMode = ColumnMode;
  SelectionType = SelectionType;
  page = new Page();
  users = new Array<UserProfile>();

  faEye = faEye;
  faPen = faPen;
  faTrash = faTrash;


  constructor(private userService: InfoUserService, private loadingService: NgxUiLoaderService) { 
    this.page.pageNumber = 0;
    this.page.size = 10;
  }

  ngOnInit(): void {
    this.setPage({ offset: 0 });
  }

  find(_event?: any) {}

  setPage(pageInfo?: any) {
    //this.loadingService.start()
    this.page.pageNumber = pageInfo.offset;
    this.userService.getUsers(this.page).subscribe((response) => { 
      //this.loadingService.stop()
      this.page.totalElements = response.totalElements;
      this.page.pageNumber = response.number;
      this.page.size = response.size;
      this.page.totalPages = response.totalPages;
      this.users = response.content;
    }, error => {
      //this.loadingService.stop()
      alert.alertError("Ocurrió un error al intentar obtener los usuarios")
    });
  }


  getGroups(roles: any[]) {
    let groups: any[] = new Array();

    roles.forEach((_role: any) => {
      if(_role.name == "ROLE_ADMIN"){
        groups.push("Administradores")
      }
      
      if(_role.name == "ROLE_ADMINFUNCIONAL"){
        groups.push("Administradores Funcionales")
      }
      if(_role.name == "ROLE_COORDINADOR"){
        groups.push("Coordinadores")
      }
      if(_role.name == "ROLE_INGENIERO"){
        groups.push("Ingenieros")
      }
      if(_role.name == "ROLE_SOLICITANTE"){
        groups.push("Solicitantes")
      }
    });
    return groups;
  }



}
