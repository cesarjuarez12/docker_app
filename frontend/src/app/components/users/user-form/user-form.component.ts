import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/auth/user.service';
import { alert } from 'src/app/common/alerts';
import { UserProfile } from 'src/app/pages/template/profile/user-profile';
import { InfoUserService } from 'src/app/services/info-user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  userProfile: UserProfile = new UserProfile();
  groups = [{ id: 'ROLE_ADMIN', name: 'Administradores' },
  { id: 'ROLE_ADMINFUNCIONAL', name: 'Administradores Funcionales' },
  { id: 'ROLE_COORDINADOR', name: 'Coordinadores' },
  { id: 'ROLE_INGENIERO', name: 'Ingenieros' },
  { id: 'ROLE_SOLICITANTE', name: 'Solicitantes' }]



  constructor(private userService: InfoUserService, private router: Router,
    private activatedRoute: ActivatedRoute, private loadingService: NgxUiLoaderService) {
    this.cargarUser();
  }


  cargarUser(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.loadingService.start()
        this.userService.getUser(id).subscribe((userProfile) => {
          this.userProfile = userProfile
          this.loadingService.stop()
          //console.log(this.userProfile)
        }, error => {
          alert.alertError("Ocurrió un error")
          this.loadingService.stop()
        })
      }
    })
  }


  getGroups(roles: any[] | undefined = []) {
    let groups: any[] = new Array();

    roles.forEach((_role: any) => {
      if (_role.name == "ROLE_ADMIN") {
        groups.push("Administradores")
        this.groups.splice(0, 1);
      }

      if (_role.name == "ROLE_ADMINFUNCIONAL") {
        groups.push("Administradores Funcionales")
        this.groups.splice(1, 1);
      }
      if (_role.name == "ROLE_COORDINADOR") {
        groups.push("Coordinadores")
        this.groups.splice(2, 1);
      }
      if (_role.name == "ROLE_INGENIERO") {
        groups.push("Ingenieros")
        this.groups.splice(3, 1);
      }
      if (_role.name == "ROLE_SOLICITANTE") {
        groups.push("Solicitantes")
        this.groups.splice(4, 1);
      }
    });
    return groups;
  }


  ngOnInit(): void {
  }

}
