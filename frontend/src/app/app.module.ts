import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgWizardModule, NgWizardConfig, THEME } from 'ng-wizard';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { LayoutModule } from '@angular/cdk/layout';
import { SortableModule } from 'ngx-bootstrap/sortable';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { VgCoreModule, VgControlsModule, VgOverlayPlayModule, VgBufferingModule } from 'ngx-videogular';

import { HttpClientModule } from '@angular/common/http';
import { authInterceptorProviders } from './_helpers/auth.interceptor';

import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { InicioComponent } from './pages/template/inicio/inicio.component';
import { ComponentesComponent } from './pages/template/componentes/componentes.component';
import { SiderbarheadComponent } from './components/layouts/siderbarhead/siderbarhead.component';

import { TabComponent } from './components/layouts/tab/tab.component';
import { ButtonsComponent } from './components/layouts/buttons/buttons.component';
import { InputsComponent } from './components/layouts/inputs/inputs.component';
import { WizardExampleComponent } from './components/layouts/wizard-example/wizard-example.component';
import { TextComponent } from './components/layouts/text/text.component';
import { TablesComponent } from './components/layouts/tables/tables.component';
import { WizardAlternativeComponent } from './components/layouts/wizard-alternative/wizard-alternative.component';
import { ProfileComponent } from './pages/template/profile/profile.component';
import { CommonModule, HashLocationStrategy, LocationStrategy, registerLocaleData } from '@angular/common';
import { DepartamentosInternosFormComponent } from './departamentos/departamentos-internos/departamentos-internos-form/departamentos-internos-form.component';
import { DepartamentosInternosListComponent } from './departamentos/departamentos-internos/departamentos-internos-list/departamentos-internos-list.component';

import { DepartamentoInternoService } from './departamentos/departamentos-internos/departamento-interno.service';
import { DepartamentosInternosOmListComponent } from './departamentos/departamentos-internos-om/departamentos-internos-om-list/departamentos-internos-om-list.component';
import { DepartamentosInternosOmFormComponent } from './departamentos/departamentos-internos-om/departamentos-internos-om-form/departamentos-internos-om-form.component';
import { DepartamentoInternoOmService } from './departamentos/departamentos-internos-om/departamento-interno-om.service';
import { LoginComponent } from './login/login.component';
import { CoordinadoresListComponent } from './components/coordinadores/coordinadores-list/coordinadores-list.component';
import { AyudaSolicitantesComponent } from './components/ayuda/ayuda-solicitantes/ayuda-solicitantes.component';
import { AyudaConfiguracionComponent } from './components/ayuda/ayuda-configuracion/ayuda-configuracion.component';
import { AyudaService } from './components/ayuda/ayuda.service';

import { NuevoVideoAyudaComponent } from './components/ayuda/nuevo-video-ayuda/nuevo-video-ayuda.component';
import { ConfiguracionGlobalComponent } from './components/configuracion-global/configuracion-global.component';
import { ConfiguracionGlobalService } from './components/configuracion-global/configuracion-global.service';
import { UsersListComponent } from './components/users/users-list/users-list.component';
import { UserFormComponent } from './components/users/user-form/user-form.component';
import { VideoComponent } from './components/ayuda/video/video.component';
import { VideosListComponent } from './components/ayuda/videos-list/videos-list.component';
import { GroupsListComponent } from './components/groups/groups-list/groups-list.component';
import { GroupsFormComponent } from './components/groups/groups-form/groups-form.component';
import { ConfigFileTypesComponent } from './components/config-file-types/config-file-types.component';
import { RequestFormComponent } from './components/request/request-form/request-form.component';

import { NavComponent } from './components/sidebar/nav.component';
import { MobilenavComponent } from './components/mobilenav/mobilenav.component';
import { RequestOptionsListComponent } from './components/request/request-options-list/request-options-list.component';
import { NgxLoadingModule } from 'ngx-loading';
import { EditorModule } from '@tinymce/tinymce-angular';
import { SidebarModule } from 'ng-sidebar';
import { SideComponent } from './components/side/side.component';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { FormularioComponent } from './components/formularios/formulario/formulario.component';
import { ConstructorFormulariosComponent } from './components/formularios/constructor-formularios/constructor-formularios.component';
import { DateComponent } from './components/formularios/types/date.component';
import { FileComponent } from './components/formularios/types/file.component';
import { InputComponent } from './components/formularios/types/input.component';
import { SelectComponent } from './components/formularios/types/select.component';
import { WYSIWYGComponent } from './components/formularios/types/wysiwyg.component';
import { FloatComponent } from './components/formularios/types/floats.component';
import { IntegerComponent } from './components/formularios/types/int.component';
import { FormulariosService } from './components/formularios/formulario.service';
import { RequestListInProcessComponent } from './components/request/request-list-in-process/request-list-in-process.component';
import { NgxUiLoaderConfig, NgxUiLoaderHttpModule, NgxUiLoaderModule, NgxUiLoaderRouterModule } from 'ngx-ui-loader';
import { FormularioListComponent } from './components/formularios/formulario-list/formulario-list.component';

import localeEsGT from '@angular/common/locales/es-GT';
registerLocaleData(localeEsGT, 'es-GT');


import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { UnassignedRequestsListComponent } from './components/request/unassigned-requests-list/unassigned-requests-list.component';
import { WorkingComponent } from './components/working/working.component';
import { RequestDetailsComponent } from './components/request/request-details/request-details.component';
import { CommentFormComponent } from './components/request/comment-form/comment-form.component';
import { RequestListForAdminComponent } from './components/request/request-list-for-admin/request-list-for-admin.component';
import { FormularioShowComponent } from './components/formularios/formulario-show/formulario-show.component';

import { ReplaceNullWithTextPipe } from './pipe/replace-null-with-text.pipe';
import { RequestVersionsHistoryComponent } from './components/request/request-versions-history/request-versions-history.component';
import { RequestPublicComponent } from './components/public/request-public/request-public.component';
import { Error404Component } from './components/layouts/error404/error404.component';
import { HistorialSolicitanteComponent } from './components/request/historial-solicitante/historial-solicitante.component';
import { HistorialCoordinadorComponent } from './components/request/historial-coordinador/historial-coordinador.component';
import { NgxResizeWatcherDirective } from './ngx-resize-watcher.directive';
import { RequestFormForEngineerComponent } from './components/request/request-form-for-engineer/request-form-for-engineer.component';
import { HistorialIngenieroComponent } from './components/request/historial-ingeniero/historial-ingeniero.component';
import { ConfigRequestsComponent } from './components/config-requests/config-requests.component';
import { RequestDetailsModalComponent } from './components/request/request-details-modal/request-details-modal.component';
import { RequestFormEnginerFullComponent } from './components/request/request-form-enginer-full/request-form-enginer-full.component';
import { RequestCompleteComponent } from './components/request/request-complete/request-complete.component';
import { OrderQuestionsComponent } from './components/formularios/order-questions/order-questions.component';

defineLocale('es', esLocale);

const ngWizardConfig: NgWizardConfig = {
  theme: THEME.default
};

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  "bgsColor": "#FFC20E",
  "bgsOpacity": 0.5,
  "bgsPosition": "center-center",
  "bgsSize": 60,
  "bgsType": "ball-spin-clockwise",
  "blur": 5,
  "delay": 0,
  "fastFadeOut": true,
  "fgsColor": "#ffc20e",
  "fgsPosition": "center-center",
  "fgsSize": 60,
  "fgsType": "ball-spin-clockwise",
  "gap": 24,
  "logoPosition": "center-center",
  "logoSize": 120,
  "logoUrl": "",
  "masterLoaderId": "master",
  "overlayBorderRadius": "0",
  "overlayColor": "rgba(40, 40, 40, 0.8)",
  "pbColor": "red",
  "pbDirection": "ltr",
  "pbThickness": 3,
  "hasProgressBar": false,
  "text": "",
  "textColor": "#FFFFFF",
  "textPosition": "center-center",
  "maxTime": -1,
  "minTime": 300
};


@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    InicioComponent,
    ComponentesComponent,
    SiderbarheadComponent,
    MobilenavComponent,
    NavComponent,
    TabComponent,
    ButtonsComponent,
    InputsComponent,
    WizardExampleComponent,
    TextComponent,
    TablesComponent,
    WizardAlternativeComponent,
    ProfileComponent,
    DepartamentosInternosFormComponent,
    DepartamentosInternosListComponent,
    DepartamentosInternosOmListComponent,
    DepartamentosInternosOmFormComponent,
    LoginComponent,
    CoordinadoresListComponent,
    AyudaSolicitantesComponent,
    AyudaConfiguracionComponent,
    NuevoVideoAyudaComponent,
    ConfiguracionGlobalComponent,
    UsersListComponent,
    UserFormComponent,
    VideoComponent,
    VideosListComponent,
    GroupsListComponent,
    GroupsFormComponent,
    ConfigFileTypesComponent,
    RequestFormComponent,
    RequestOptionsListComponent,
    SideComponent,
    FormularioComponent,
    ConstructorFormulariosComponent,
    DateComponent,
    FileComponent,
    InputComponent,
    IntegerComponent,
    FloatComponent,
    SelectComponent,
    WYSIWYGComponent,
    RequestListInProcessComponent,
    FormularioListComponent,
    UnassignedRequestsListComponent,
    WorkingComponent,
    RequestDetailsComponent,
    CommentFormComponent,
    RequestListForAdminComponent,
    FormularioShowComponent,
    ReplaceNullWithTextPipe,
    RequestVersionsHistoryComponent,
    RequestPublicComponent,
    Error404Component,
    HistorialSolicitanteComponent,
    HistorialCoordinadorComponent,
    NgxResizeWatcherDirective,
    RequestFormForEngineerComponent,
    HistorialIngenieroComponent,
    ConfigRequestsComponent,
    RequestDetailsModalComponent,
    RequestFormEnginerFullComponent,
    RequestCompleteComponent,
    OrderQuestionsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    AppRoutingModule,
    LayoutModule,
    SweetAlert2Module.forRoot(),
    InfiniteScrollModule,
    NgxDatatableModule,
    NgWizardModule.forRoot(ngWizardConfig),
    BrowserAnimationsModule,
    RoundProgressModule,
    NgSelectModule,
    FormsModule,
    FontAwesomeModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({}),
    EditorModule,
    AngularEditorModule,
    SidebarModule.forRoot(),
    AccordionModule.forRoot(),
    NgScrollbarModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    TimepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    VgCoreModule, VgControlsModule, VgOverlayPlayModule, VgBufferingModule,
    NgxUiLoaderRouterModule,
    NgxUiLoaderHttpModule,
    SortableModule.forRoot(),
    PopoverModule.forRoot()
  ],
  providers: [
    DepartamentoInternoService, DepartamentoInternoOmService, authInterceptorProviders, AyudaService, ConfiguracionGlobalService, FormulariosService,
    {provide: LOCALE_ID, useValue: 'es-GT'}
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
