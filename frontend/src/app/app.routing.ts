import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';
import { PermissionGuardService } from './guards/permission-guard.service';

import { InicioComponent } from './pages/template/inicio/inicio.component';
import { ComponentesComponent } from './pages/template/componentes/componentes.component';
import { ProfileComponent } from './pages/template/profile/profile.component';

import { DepartamentosInternosFormComponent } from './departamentos/departamentos-internos/departamentos-internos-form/departamentos-internos-form.component';
import { DepartamentosInternosListComponent } from './departamentos/departamentos-internos/departamentos-internos-list/departamentos-internos-list.component';
import { DepartamentosInternosOmFormComponent } from './departamentos/departamentos-internos-om/departamentos-internos-om-form/departamentos-internos-om-form.component';
import { DepartamentosInternosOmListComponent } from './departamentos/departamentos-internos-om/departamentos-internos-om-list/departamentos-internos-om-list.component';
import { LoginComponent } from './login/login.component';
import { CoordinadoresListComponent } from './components/coordinadores/coordinadores-list/coordinadores-list.component';
import { AyudaSolicitantesComponent } from './components/ayuda/ayuda-solicitantes/ayuda-solicitantes.component';
import { AyudaConfiguracionComponent } from './components/ayuda/ayuda-configuracion/ayuda-configuracion.component';
import { NuevoVideoAyudaComponent } from './components/ayuda/nuevo-video-ayuda/nuevo-video-ayuda.component';
import { ConfiguracionGlobalComponent } from './components/configuracion-global/configuracion-global.component';
import { UsersListComponent } from './components/users/users-list/users-list.component';
import { UserFormComponent } from './components/users/user-form/user-form.component';
import { VideoComponent } from './components/ayuda/video/video.component';
import { GroupsListComponent } from './components/groups/groups-list/groups-list.component';
import { GroupsFormComponent } from './components/groups/groups-form/groups-form.component';
import { ConfigFileTypesComponent } from './components/config-file-types/config-file-types.component';
import { RequestFormComponent } from './components/request/request-form/request-form.component';
import { RequestOptionsListComponent } from './components/request/request-options-list/request-options-list.component';

import { FormularioComponent } from './components/formularios/formulario/formulario.component';
import { RequestListInProcessComponent } from './components/request/request-list-in-process/request-list-in-process.component';
import { FormularioListComponent } from './components/formularios/formulario-list/formulario-list.component';
import { LoginGuard } from './guards/login.guard';
import { UnassignedRequestsListComponent } from './components/request/unassigned-requests-list/unassigned-requests-list.component';
import { WorkingComponent } from './components/working/working.component';
import { RequestDetailsComponent } from './components/request/request-details/request-details.component';
import { RequestListForAdminComponent } from './components/request/request-list-for-admin/request-list-for-admin.component';
import { FormularioShowComponent } from './components/formularios/formulario-show/formulario-show.component';
import { RequestPublicComponent } from './components/public/request-public/request-public.component';
import { Error404Component } from './components/layouts/error404/error404.component';
import { HistorialSolicitanteComponent } from './components/request/historial-solicitante/historial-solicitante.component';
import { HistorialCoordinadorComponent } from './components/request/historial-coordinador/historial-coordinador.component';
import { RequestFormForEngineerComponent } from './components/request/request-form-for-engineer/request-form-for-engineer.component';
import { HistorialIngenieroComponent } from './components/request/historial-ingeniero/historial-ingeniero.component';
import { ConfigRequestsComponent } from './components/config-requests/config-requests.component';
import { RequestCompleteComponent } from './components/request/request-complete/request-complete.component';

const routes: Routes = [
  //Otros
  {path: 'login', component: LoginComponent},
  //{path: 'inicio', component: InicioComponent,  canActivate: [AuthGuard]},
  {path: '', redirectTo: '/inicio', pathMatch: 'full', canActivate: [AuthGuard]},
  //{path: '', redirectTo: '/inicio', pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'componentes', component: ComponentesComponent, canActivate: [AuthGuard]},
  {path: 'perfil', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'inicio', component: InicioComponent, canActivate: [AuthGuard]},
  {path: 'request/accessTo/:id', component: RequestPublicComponent},
  
  
  //DepartamentosInternos
  {path: 'departamento-interno/form', component: DepartamentosInternosFormComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService] },
  {path: 'departamento-interno/form/:id', component: DepartamentosInternosFormComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'departamentos-internos', component: DepartamentosInternosListComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  
  //DepartamentosInternosOEM
  {path: 'departamento-interno-om/form', component: DepartamentosInternosOmFormComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'departamento-interno-om/form/:id', component: DepartamentosInternosOmFormComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'departamentos-internos-om', component: DepartamentosInternosOmListComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  
  //Coordinadores
  {path: 'coordinadores', component: CoordinadoresListComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  
  //Ayuda
  {path: 'ayuda', component: AyudaSolicitantesComponent, data: { permission: ["ROLE_SOLICITANTE","ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'ayuda/configuracion', component: AyudaConfiguracionComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'ayuda/configuracion/nuevo-video', component: NuevoVideoAyudaComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'ayuda/configuracion/edit-video/:id', component: NuevoVideoAyudaComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'ayuda/video/:id', component: VideoComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL", "ROLE_SOLICITANTE"]}, canActivate: [PermissionGuardService]},
  
  //ConfiguracionGlobal
  {path: 'configuracion-global', component: ConfiguracionGlobalComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'configuracion-grupos', component: GroupsListComponent, data: { permission: ["ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'configuracion-grupos/form/:id', component: GroupsFormComponent, data: { permission: ["ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'config-files', component: ConfigFileTypesComponent, data: { permission: ["ROLE_ADMINFUNCIONAL","ROLE_ADMIN"]}, canActivate: [PermissionGuardService]},
  {path: 'config-requests', component: ConfigRequestsComponent, data: { permission: ["ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},

  //Users
  {path: 'users', component: UsersListComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'users/form/:id', component: UserFormComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService] },

  //Solicitudes
  {path: 'solicitud/nueva', component: RequestOptionsListComponent, data: { permission: ["ROLE_SOLICITANTE"]}, canActivate: [PermissionGuardService]},
  {path: 'solicitud/form/:id', component: RequestFormComponent, data: { permission: ["ROLE_SOLICITANTE"]}, canActivate: [PermissionGuardService]},
  {path: 'solicitud/form/engineer/:id', component: RequestFormForEngineerComponent, data: { permission: ["ROLE_INGENIERO"]}, canActivate: [PermissionGuardService]},
  {path: 'solicitud/form', component: RequestFormComponent, data: { permission: ["ROLE_SOLICITANTE"]}, canActivate: [PermissionGuardService]},
  {path: 'mis-solicitudes/:status', component: RequestListInProcessComponent, data: { permission: ["ROLE_SOLICITANTE"]}, canActivate: [PermissionGuardService]},
  {path: 'solicitudes/para-asignacion', component: UnassignedRequestsListComponent, data: { permission: ["ROLE_COORDINADOR"]}, canActivate: [PermissionGuardService]},
  {path: 'solicitud/:id', component: RequestDetailsComponent, data: { permission: ["ROLE_SOLICITANTE", "ROLE_COORDINADOR", "ROLE_INGENIERO", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  //Solicitudes-Historial Solicitante
  {path: 'historial-solicitudes', component: HistorialSolicitanteComponent, data: { permission: ["ROLE_SOLICITANTE"]}, canActivate: [PermissionGuardService]},
  //Solicitudes-Historial Coordinador
  {path: 'historial-all-solicitudes', component: HistorialCoordinadorComponent, data: { permission: ["ROLE_COORDINADOR"]}, canActivate: [PermissionGuardService]},
  //Solicitudes-Historial Ingeniero
  {path: 'historial-solicitudes-ing', component: HistorialIngenieroComponent, data: { permission: ["ROLE_INGENIERO"]}, canActivate: [PermissionGuardService]},
  {path: 'historial-solicitudes-ing-complete', component: RequestCompleteComponent, data: { permission: ["ROLE_INGENIERO"]}, canActivate: [PermissionGuardService]},
  //Solicitudes-Admin
  {path: 'solicitudes', component: RequestListForAdminComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  


  //Formularios
  {path: 'formularios', component: FormularioListComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'formulario/form', component: FormularioComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'formulario/edit-form/:id', component: FormularioComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'formulario/file/:id', component: FormularioComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  {path: 'formulario/show/:id', component: FormularioShowComponent, data: { permission: ["ROLE_ADMIN", "ROLE_ADMINFUNCIONAL"]}, canActivate: [PermissionGuardService]},
  

  //working
  {path: 'we-are-working', component: WorkingComponent},





  //----------------------404---------------------------------->
  //404
  {path: '404', component: Error404Component},
  {path: '**', redirectTo: '404'},
  
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
