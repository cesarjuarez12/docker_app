# ConstruirContenedorFrontend

Despues de clonar posicionarse en el Path donde esta el Archivo Dockerfile
Ejecutar en consola `docker build -t NombreContenedor .`

## EjecutarContenedor

Sin moverse del Path Anterior
Ejecutar en consola `docker run -d -it -p 80:80 NombreContenedor`

## ReplicarContainer -Ejemplo Puerto 8000

Sin moverse del Path Anterior
Ejecutar en consola `docker run -d -it -p 8000:80 NombreContenedor`